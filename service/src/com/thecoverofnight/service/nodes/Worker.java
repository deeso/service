/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.nodes;

import java.io.IOException;
import java.net.Socket;

import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.xml.DOMConfigurator;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.comms.BasicMessage;
import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.RunningTask;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.NodeXMLReaderWriter;

class Dummy {

	Boolean done;

	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

}

public class Worker extends BaseNode {

	public void Node() {
		myServer = null;
	}

	public static void main(String[] args) throws IOException {
		DOMConfigurator.configure("example/log4j.xml");
		Worker x = new Worker();
		LOGGER.debug("Initializing the server.");
		try {
			NodeXMLReaderWriter.parseFromFile(x, "example/workernode.xml");
		} catch (ParserConfigurationException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		} catch (SAXException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		LOGGER.debug("Starting the server.");
		x.startServer();

		final Dummy neverEndingStory = new Dummy();
		neverEndingStory.setDone(true);
		Timer t = new Timer();
		TimerTask tt = new TimerTask() {
			@Override
			public void run() {
				neverEndingStory.setDone(false);
			}
		};
		t.schedule(tt, 60 * 1000);

		while (neverEndingStory.getDone()) {
			try {
				Thread.sleep(60 * 1000 + 500);
			} catch (InterruptedException e) {
				LOGGER.debug(ExceptionToString.toString(e));

			}

		}
		t = null;

		LOGGER.debug("Stopping the server.");
		// x.stopServer();
	}

	@Override
	public void connectionOpened(Socket connection) {
		// TODO Auto-generated method stub
		LOGGER.debug("Client connection received, adding the peer.\n");
		try {
			addPeerNode(connection);
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
	}

	@Override
	public void connectionClosed(Socket connection) {
		// TODO Auto-generated method stub
		LOGGER.debug("Client Connection closed.\n");

	}

	@Override
	public void dataRecieved(String host, String data) {
		LOGGER.debug("Completed recieving message.\n");
		LOGGER.debug("msg:\n" + data + "\n");
		sendData(host, data);
	}

	@Override
	public void dataSent() {
		// TODO Auto-generated method stub
		LOGGER.debug("Data sent to the other end.\n");
	}

	public void jobCompleted(String taskId, Command cmd) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commandCompleted(RunningTask rt) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleMessage(String peerNodeName, BasicMessage data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataRecieved(Socket connection, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionClosed(String host, int port) {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

}