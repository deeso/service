/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.nodes;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;


import com.thecoverofnight.service.comms.BasicMessage;
import com.thecoverofnight.service.comms.IncompleteBasicMessage;
import com.thecoverofnight.service.comms.NodeClientSocket;
import com.thecoverofnight.service.comms.NodeEvents;
import com.thecoverofnight.service.comms.NodeServerSocket;
import com.thecoverofnight.service.comms.PlainTextNodeSocket;
import com.thecoverofnight.service.job.RunningTask;
import com.thecoverofnight.service.util.ExceptionToString;

@SuppressWarnings("static-access")
public abstract class BaseNode implements NodeEvents {

	String myName;
	protected Long window;
	protected HashMap<String, NodeClientSocket> myPeerNodes = new HashMap<String, NodeClientSocket>();
	protected HashMap<String, IncompleteBasicMessage> pendingMessages = new HashMap<String, IncompleteBasicMessage>();
	HashMap<String, String> myPeerAliases = new HashMap<String, String>();

	protected static Logger LOGGER = Logger.getLogger(BaseNode.class);

	protected static Options options = null; // Command line options

	protected final String HEARTBEAT = "HEARTBEAT";
	protected final String SHUTDOWN = "SHUTDOWN";
	protected static final String CONFIGURATION_FILE = "C";
	// private static final String NETWORK_ADDRESS = "n";
	protected static final String CONFIGURATION_PORT = "p";
	protected static final String CONFIGURATION_SERVER_PORT = "s";
	protected static final String CONFIGURATION_TIMEOUT = "t";
	protected static final String CONFIGURATION_HOST = "i";
	protected static final String CONFIGURATION_BACKLOG = "b";
	protected static final String CONFIGURATION_LOGGER = "L";
	protected static final String CONFIGURATION_HELP = "h";

	protected CommandLine cmd = null; // Command Line arguments

	static {
		options = new Options();
		Option oport = OptionBuilder.withArgName("PORT").hasArg()
				.withDescription("port to listen on").withType(Integer.class)
				.create(CONFIGURATION_PORT), oserver_port = OptionBuilder
				.withArgName("PORT")
				.hasArg()
				.withDescription(
						"command handler port to listen on (manager only)")
				.withType(Integer.class).create(CONFIGURATION_SERVER_PORT), ohost = OptionBuilder
				.withArgName("HOST").hasArg()
				.withDescription("hostname to listen on (0.0.0.0 by default)")
				.withType(String.class).create(CONFIGURATION_HOST), obacklog = OptionBuilder
				.withArgName("BACKLOG_CNT").hasArg()
				.withDescription("service backlog permitted")
				.withType(Integer.class).create(CONFIGURATION_BACKLOG), otimeout = OptionBuilder
				.withArgName("TIMEOUT").hasArg()
				.withDescription("timeout window of connections")
				.withType(Integer.class).create(CONFIGURATION_TIMEOUT), ologger = OptionBuilder
				.withArgName("FILE")
				.hasArg()
				.withDescription(
						"log4j.xml configuration file (example/log4j.xml by default)")
				.withType(String.class).create(CONFIGURATION_LOGGER);

		Option help = new Option(CONFIGURATION_HELP, "print this message");
		// options.addOption(CONFIGURATION_FILE, true,
		// "configuration file location");
		options.addOption(oport);
		options.addOption(oserver_port);
		options.addOption(ohost);
		options.addOption(obacklog);
		options.addOption(otimeout);
		options.addOption(ologger);
		options.addOption(help);

	}

	public String getName() {
		return myName;
	}

	public void setName(String myName) {
		this.myName = myName;
	}

	protected NodeServerSocket myServer;

	public void initServerParameters(String interface_, int port, int timeout,
			int backlog) {
		myServer = new NodeServerSocket(this, interface_, port, timeout);
	}

	public void startServer() {
		LOGGER.debug("Server start begins.");
		myServer.start();
		LOGGER.debug("Server start ends.");
	}

	private void stopServer() {
		try {
			LOGGER.debug("Server stop begins.\n");
			myServer.stop();
			LOGGER.debug("Server stop ends.\n");
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
			e.printStackTrace();
		}
	}

	public void cleanupPeerNodes() {
		LOGGER.debug("cleanupPeerNodes begins.\n");
		for (String host : new HashSet<String>(myPeerNodes.keySet())) {
			disconnectFromNode(host);
		}
		LOGGER.debug("cleanupPeerNodes ends.\n");
	}

	public void addAlias(String host, String alias) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			LOGGER.debug("addAlias " + host + " -> " + alias);
			myPeerAliases.put(host, alias);
			myPeerAliases.put(alias, host);
		}
	}

	public String getAlias(String host) {
		if (myPeerAliases.containsKey(host)) {
			return myPeerAliases.get(host);
		}
		return null;
	}

	public boolean disconnectFromNode(String host) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			myPeerNodes.get(host).disconnect();
			myPeerNodes.remove(host);
			return true;
		}
		return false;
	}

	public boolean addPeerNode(String host, int port, int timeout) {

		if (myPeerNodes.containsKey(host + ":" + port)) {
			return true;
		}

		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, host, port,
				timeout);
		myPeerNodes.put(host + ":" + pt.getPort(), pt);
		pt.startPolling();
		return true;
	}

	public boolean addPeerNode(Socket connection) throws IOException {
		String peerNode = connection.getInetAddress().getHostAddress() + ":"
				+ connection.getPort();

		if (myPeerNodes.containsKey(peerNode)) {
			return true;
		}

		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, connection,
				myServer.getTimeout());
		pt.setPollTime(myServer.getTimeout());
		pt.setTimeout(myServer.getTimeout());
		pt.startPolling();
		myPeerNodes.put(pt.getHost() + ":" + pt.getPort(), pt);
		return true;
	}

	public NodeClientSocket getPeerNode(Socket connection) throws IOException {
		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, connection,
				myServer.getTimeout());
		String peerNodeName = pt.getHost() + ":" + pt.getPort();
		if (myPeerNodes.containsKey(peerNodeName))
			return myPeerNodes.get(peerNodeName);
		return null;
	}

	public NodeClientSocket getPeerNode(String peerNodeName) throws IOException {
		if (myPeerNodes.containsKey(peerNodeName))
			return myPeerNodes.get(peerNodeName);
		return null;
	}

	public boolean connectToPeer(String host, int port) {
		String peerNode = host + ":" + port;
		return connectToPeer(peerNode);
	}

	public boolean connectToPeer(String host) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			LOGGER.debug("connectToPeer: " + host);
			return myPeerNodes.get(host).connect();
		} else if (myPeerAliases.containsKey(host)
				&& myPeerAliases.get(host) != null) {
			String alias = myPeerAliases.get(host);
			if (myPeerNodes.containsKey(alias)
					&& myPeerNodes.get(alias) != null) {
				LOGGER.debug("connectToPeer: " + host + " via " + alias);
				return myPeerNodes.get(host).connect();
			}
			LOGGER.debug("FAILED: connectToPeer: " + host + " via " + alias);
			return false;
		}
		LOGGER.debug("FAILED: connectToPeer: " + host);
		return false;
	}

	public boolean connectToAllPeers() {
		boolean result = true;
		LOGGER.debug("connectToAllPeers started.");
		for (String host : myPeerNodes.keySet()) {
			if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
				result &= myPeerNodes.get(host).connect();
				myPeerNodes.get(host).startPolling();
			}
		}
		LOGGER.debug("connectToAllPeers ended.");
		return result;
	}

	public void sendBasicMessage(String host, BasicMessage bn) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			NodeClientSocket ncs = myPeerNodes.get(host);
			ncs.send(bn.toString());
		}
	}

	public void sendData(String host, String data) {
		sendBasicMessage(host, new BasicMessage(data));
	}

	@Override
	public void dataRecieved(String host, int port, BufferedReader inStream) {

		String peerNodeName = host + ":" + port;
		Long time = new Long((new Date()).getTime() / 1000);
		IncompleteBasicMessage ibm = null;

		if (pendingMessages.containsKey(peerNodeName)
				&& pendingMessages.get(peerNodeName).isMessageStale(time,
						window)) {
			pendingMessages.remove(peerNodeName);
		}
		int numBytesToRead = 4; // size of the length
		char[] bytesToRead = new char[numBytesToRead];
		if (pendingMessages.containsKey(peerNodeName)) {
			ibm = pendingMessages.get(peerNodeName);
			numBytesToRead = ibm.getLength() - ibm.getData().length();
			// ibm.append(data);
			try {
				bytesToRead = new char[numBytesToRead];
			} catch (OutOfMemoryError e) {
				LOGGER.debug("Number of bytes ");
				LOGGER.debug(ExceptionToString.toString(e));
			}
		}

		int bytesRead = 0;
		try {
			bytesRead = inStream.read(bytesToRead);
		} catch (IOException e1) {
			LOGGER.debug("Error reading the input buffer.");
			LOGGER.debug(e1);
		}

		if (ibm != null) {
			ibm.append(new String(bytesToRead));
		} else {
			try {
				if (bytesRead < 4) {
					LOGGER.debug("Error reading the bytes for the message length.");
				}
				char[] msgId = new char[numBytesToRead];
				bytesRead = inStream.read(msgId);
				if (bytesRead < 4) {
					LOGGER.debug("Error reading the bytes for the message ID.");
				}
				ibm = new IncompleteBasicMessage(bytesToRead, msgId);
				pendingMessages.put(peerNodeName, ibm);
			} catch (IOException e) {
				LOGGER.debug("Error handling recieved data.");
				LOGGER.debug(e.getMessage());
			}
		}

		if (ibm != null && ibm.isMessageComplete()) {
			LOGGER.debug("Handling the completed basic message");
			pendingMessages.remove(peerNodeName);
			BasicMessage bm = new BasicMessage(ibm);
			if (bm.getData().equalsIgnoreCase(HEARTBEAT)) {
				return;
			}
			handleMessage(peerNodeName, bm);
		}
	}

	abstract public void commandCompleted(RunningTask rt);

	abstract public void handleMessage(String peerNodeName, BasicMessage data);

	@Override
	public void dataRecieved(Socket connection, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataRecieved(String host, String data) {
		// TODO Auto-generated method stub

	}

}
