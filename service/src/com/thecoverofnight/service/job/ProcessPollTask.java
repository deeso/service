/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.util.TimerTask;

import org.apache.log4j.Logger;

class ProcessPollTask extends TimerTask {

	private static Logger LOGGER = Logger.getLogger(ProcessPollTask.class);
	RunningTask myTask;

	public ProcessPollTask(RunningTask task) {

		myTask = task;

	}

	@Override
	public void run() {
		LOGGER.debug("ProcessPollTask.run: executing socket check.");

		if (myTask.checkStderrStream()) {
			// TODO get the error string, log it, and fwd it to the JobManager
			LOGGER.debug("ProcessPollTask.run: stderr has something for us: .");
			return;
		}

		if (myTask.checkStderrStream()) {
			// TODO get the stdout string, log it, and fwd it to the JobManager
			LOGGER.debug("ProcessPollTask.run: stdout has something for us: .");

		}

		// TODO if process is still running and we need to poll, reschedule this

		// TODO otherwise cancel ourselves
		LOGGER.debug("ProcessPollTask.run: recv'ing data.");

	}
}
