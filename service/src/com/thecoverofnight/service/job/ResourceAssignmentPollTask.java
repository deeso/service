/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.implementation.ResourceManager2;
import com.thecoverofnight.service.util.ExceptionToString;

public class ResourceAssignmentPollTask {
	final Object lock = new Object();

	ResourceManager2 myResourceManager;
	Timer timer = null;

	int pollTime = 0;
	private static Logger LOGGER = Logger
			.getLogger(ResourceAssignmentPollTask.class);

	int state = NOT_POLLING;
	static final int NOT_POLLING = 0;
	static final int POLLING = 1;

	Boolean CMD_BEING_ASSIGNED = new Boolean(false);
	static final int NOT_READING = 0;
	static final int READING = 1;

	public ResourceAssignmentPollTask(ResourceManager2 rm, int timing) {
		myResourceManager = rm;
		pollTime = timing;
		state = NOT_POLLING;
	}

	public void start() {

		if (state == POLLING)
			return;

		synchronized (lock) {

			if (state == NOT_POLLING) {
				state = POLLING;
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						synchronized (lock) {
							if (!isPolling()) {
								LOGGER.debug("Looks like state == NOT_POLLING");

							} else if (myResourceManager
									.checkAvailableJobsAndWorkers()
									&& !CMD_BEING_ASSIGNED) {
								CMD_BEING_ASSIGNED = true;
								LOGGER.debug("assigning commands.");
								myResourceManager.assignWork();
							}
						} // end of synchronized block s

						if (myResourceManager.isShuttingDown()) {
							cancel();
							LOGGER.debug("cancelling polling for resources and commands to be delegated.");
						}

					}
				}, pollTime, pollTime);
			}
		}
	}

	public void doneAssigning() {
		synchronized (lock) {
			CMD_BEING_ASSIGNED = false;
		}
	}

	public void reset() {
		cancel();
	}

	public void setPollTime(int time) {
		pollTime = time;
	}

	public boolean isPolling() {
		return state == POLLING;
	}

	public void cancel() {
		synchronized (lock) {
			state = NOT_POLLING;
			CMD_BEING_ASSIGNED = false;

			if (timer == null) {
				return;
			}

			timer.cancel();

		}

	}

}
