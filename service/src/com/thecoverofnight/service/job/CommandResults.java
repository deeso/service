/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */
package com.thecoverofnight.service.job;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;


import com.thecoverofnight.service.util.Component;
import com.thecoverofnight.service.xml.CommandResultsXMLReaderWriter;
import com.thecoverofnight.service.xml.ComponentSaxHandler;

public class CommandResults implements Component {

	final CommandResultsXMLReaderWriter CMDRESS_XML_ONLY = CommandResultsXMLReaderWriter.CMDRESS_XML_ONLY;
	ArrayList<CommandResult> commandResults = new ArrayList<CommandResult>();
	String commandId;

	public String getCommandId() {
		return commandId;
	}

	public void setCommandId(String id) {
		commandId = id;
	}

	public CommandResults(String id) {
		commandId = id;
	}

	public CommandResults() {
		commandId = "";
	}

	public CommandResults(ArrayList<CommandResult> commandResults) {
		for (CommandResult cmdRes : commandResults) {
			this.commandResults.add(cmdRes);
		}
	}

	public ArrayList<CommandResult> getCommandResults() {
		return commandResults;
	}

	public void addCommandResult(CommandResult cmdResult) {
		commandResults.add(cmdResult);
	}

	public void addCommandResults(ArrayList<CommandResult> cmdResults) {
		for (CommandResult cmdResult : cmdResults)
			commandResults.add(cmdResult);
	}

	@Override
	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException {
		if (handler.checkTag(CMDRESS_XML_ONLY.XML_COMMAND_RESULTS_ID)) {
			commandId = new String(ch, start, length);
			handler.endTag(CMDRESS_XML_ONLY.XML_COMMAND_RESULTS_ID);
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException {
		// Nothing

	}

	@Override
	public void fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		CMDRESS_XML_ONLY.fromTransport(this, n);

	}

	@Override
	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		CMDRESS_XML_ONLY.fromTransport(this, xml);

	}

	@Override
	public ComponentSaxHandler getHandler() {
		return CMDRESS_XML_ONLY.createDefaultHandler();
	}

	public ArrayList<CommandResult> getResults() {
		return commandResults;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException {

	}

	@Override
	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMDRESS_XML_ONLY.toTransport(this);
	}

	@Override
	public Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		return CMDRESS_XML_ONLY.toTransport(this, doc);
	}

	@Override
	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMDRESS_XML_ONLY.toTransportString(this);
	}

}
