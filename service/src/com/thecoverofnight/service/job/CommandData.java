/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.io.IOException;
import java.util.HashMap;

import com.thecoverofnight.service.util.Component;

public interface CommandData extends Component {

	public HashMap<String, String> getAttributes();

	public void setAttribute(String key, String val);

	public void getAttribute(String key);

	public void prepareData();

	public void cleanupData();

	public String readDataToString();

	public void writeDataFromString(String data);

	void appendDataFromString(String data) throws IOException;

	void writeDataFromBase64(String data);

	void appendDataFromString64(String data) throws IOException;

}
