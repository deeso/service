/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.util.Component;
import com.thecoverofnight.service.xml.CommandResultXMLReaderWriter;
import com.thecoverofnight.service.xml.ComponentSaxHandler;
import com.thecoverofnight.service.xml.FileContainerXMLReaderWriter;

@SuppressWarnings("static-access")
public class CommandResult implements Component {

	final CommandResultXMLReaderWriter CMDRES_XML_ONLY = CommandResultXMLReaderWriter.CMDRES_XML_ONLY;
	FileContainerXMLReaderWriter FC_XML = FileContainerXMLReaderWriter.FC_XML_ONLY;
	ArrayList<FileContainer> myCommandResultFiles = new ArrayList<FileContainer>();
	String myCommandResultId;
	ArrayList<String> myCommandResultMessages = new ArrayList<String>();
	String myCommandResultName;
	int myCommandResultStatusCode;

	public CommandResult() {

		myCommandResultStatusCode = 0;
	}

	public CommandResult(String msg, int code) {
		myCommandResultMessages.add(msg);
		myCommandResultStatusCode = code;
	}

	public CommandResult(String msg, int code, ArrayList<FileContainer> fcs) {
		myCommandResultMessages.add(msg);
		myCommandResultStatusCode = code;
		for (FileContainer fc : fcs) {
			myCommandResultFiles.add(fc);
		}
	}

	public CommandResult(String msg, int code, FileContainer fc) {
		myCommandResultMessages.add(msg);
		myCommandResultStatusCode = code;
		myCommandResultFiles.add(fc);
	}

	public void addFileContainer(FileContainer fc) {
		myCommandResultFiles.add(fc);
	}

	public void addMessage(String msg) {
		myCommandResultMessages.add(msg);
	}

	public void addMessages(ArrayList<String> msgs) {
		for (String msg : msgs) {
			myCommandResultMessages.add(msg);
		}
	}

	public void addMessages(String[] msgs) {
		for (String msg : msgs) {
			myCommandResultMessages.add(msg);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException {
		/*
		 * All the elements have a "value" attribute that contain the value. The
		 * FileContainer will parse itself
		 */

	}

	@Override
	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException {

		if (qName.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_FILES)) {
			handler.endTag(CMDRES_XML_ONLY.XML_COMMAND_RESULT_FILES);
			handler.resetTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
		} else if (qName
				.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGES)) {
			handler.endTag(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGE);
			handler.endTag(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGES);
		}

	}

	@Override
	public void fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		CMDRES_XML_ONLY.fromTransport(this, n);

	}

	@Override
	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		CMDRES_XML_ONLY.fromTransport(this, xml);

	}

	public ArrayList<FileContainer> getCommandResultFiles() {
		return myCommandResultFiles;
	}

	public String getCommandResultId() {
		return myCommandResultId;
	}

	public ArrayList<String> getCommandResultMessages() {
		return myCommandResultMessages;
	}

	public String getCommandResultName() {
		return myCommandResultName;
	}

	public int getCommandResultStatusCode() {
		return myCommandResultStatusCode;
	}

	@Override
	public ComponentSaxHandler getHandler() {
		return CMDRES_XML_ONLY.createDefaultHandler();
	}

	public void setCommandResultFiles(
			ArrayList<FileContainer> myCommandResultFiles) {
		this.myCommandResultFiles = myCommandResultFiles;
	}

	public void setCommandResultId(String myCommandResultId) {
		this.myCommandResultId = myCommandResultId;
	}

	public void setCommandResultMessages(
			ArrayList<String> myCommandResultMessages) {
		this.myCommandResultMessages = myCommandResultMessages;
	}

	public void setCommandResultName(String myCommandResultName) {
		this.myCommandResultName = myCommandResultName;
	}

	public void setCommandResultStatusCode(int myCommandResultStatusCode) {
		this.myCommandResultStatusCode = myCommandResultStatusCode;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException {
		// do I want to create logic here that will clear the ComandResult's
		// messages?
		if (qName.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_ID)) {
			myCommandResultId = attributes
					.getValue(CMDRES_XML_ONLY.XML_COMMAND_RESULT_VALUE_ATTR);
			handler.endTag(qName);
		} else if (qName
				.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_NAME)) {
			myCommandResultName = attributes
					.getValue(CMDRES_XML_ONLY.XML_COMMAND_RESULT_VALUE_ATTR);
			handler.endTag(qName);
		} else if (qName
				.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_STATUS_CODE)) {
			try {
				myCommandResultStatusCode = Integer
						.parseInt(attributes
								.getValue(CMDRES_XML_ONLY.XML_COMMAND_RESULT_VALUE_ATTR));
			} catch (NullPointerException e) {
				myCommandResultStatusCode = -1;
			} catch (NumberFormatException e) {
				myCommandResultStatusCode = -1;
			}
			handler.endTag(qName);
		} else if (qName
				.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGES)) {
			handler.startTag(qName);
		} else if (handler
				.checkTag(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGES)
				&& qName.equalsIgnoreCase(CMDRES_XML_ONLY.XML_COMMAND_RESULT_MESSAGE)) {
			myCommandResultMessages.add(attributes
					.getValue(CMDRES_XML_ONLY.XML_COMMAND_RESULT_VALUE_ATTR));
			handler.endTag(qName);
		}
	}

	@Override
	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMDRES_XML_ONLY.toTransport(this);
	}

	@Override
	public Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		return CMDRES_XML_ONLY.toTransport(this, doc);
	}

	@Override
	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMDRES_XML_ONLY.toTransportString(this);
	}

}
