/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.PriorityBlockingQueue;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.job.CommandState.CMD_STATE;
import com.thecoverofnight.service.nodes.BaseNode;

public class JobManager implements Manager, JobEvents {

	String myJobId;
	String myJobDesc;

	Integer jobIds = new Integer(0);
	PriorityBlockingQueue<Command> pendingCmdQueue = new PriorityBlockingQueue<Command>();
	HashMap<String, RunningTask> runningCommands = new HashMap<String, RunningTask>();

	HashSet<RunningTask> parentCommands = new HashSet<RunningTask>();
	HashMap<RunningTask, String> runningTaskToPeer = new HashMap<RunningTask, String>();
	HashMap<RunningTask, RunningTask> childTaskToParentTask = new HashMap<RunningTask, RunningTask>();

	HashMap<String, ArrayList<String>> commandM;

	static Logger LOGGER = Logger.getLogger(JobManager.class);

	BaseNode myBaseNode = null;

	public JobManager(BaseNode bn) {
		myBaseNode = bn;

	}

	void startCommand(Command cmd) {
		synchronized (runningCommands) {

			RunningTask r = new RunningTask(this, cmd, getNewTaskID());
			r.start();
			runningCommands.put(r.getTaskId(), r);
			parentCommands.add(r);
		}
	}

	void startNextCmd() throws InterruptedException {

		// FIXME this runs commnads serially, but what do i need to do in order
		// to make it multi-threaded
		synchronized (runningCommands) {

			Command cmd = pendingCmdQueue.poll();
			if (cmd != null) {
				RunningTask r = new RunningTask(this, cmd, getNewTaskID());
				r.start();
				runningCommands.put(r.getTaskId(), r);
			}
		}

	}

	public HashMap<String, RunningTask> getRunningCommands() {
		return runningCommands;
	}

	void addCommandToExecute(Command cmd) {
		// TODO signal the queue to wake up
		pendingCmdQueue.add(cmd);

	}

	@Override
	public void jobCompleted(String jobId, int statusCode, String message) {
		LOGGER.debug("Completed the following job with state: " + jobId + " "
				+ statusCode);
		LOGGER.debug("Exit message: " + message);
		Command cmd = null;

		synchronized (runningCommands) {

			// Add a command result to the runnning task
			if (runningCommands.containsKey(jobId)) {

				ArrayList<String> messages = runningCommands.get(jobId)
						.getMessages();
				cmd = runningCommands.get(jobId).getCommand();
				// myBaseNode.jobCompleted(jobId, cmd);
				messages.add(message);
				CommandResult cresult = createCommandResult(statusCode,
						messages, cmd);
				runningCommands.get(jobId).addCommandResult(cresult);
			}
		}
		if (cmd == null) {
			// FIXME raise exceptions here
		}

	}

	@Override
	public void yielding(String jobId) {
		LOGGER.debug("Job failed: " + jobId);
	}

	@Override
	public void jobFailure(String jobId, int statusCode, String message) {
		LOGGER.debug("Completed the following job with state: " + jobId + " "
				+ statusCode);
		LOGGER.debug("Exit message: " + message);
		Command cmd = null;
		synchronized (runningCommands) {
			// Add a command result to the runnning task
			if (runningCommands.containsKey(jobId)) {

				ArrayList<String> messages = runningCommands.get(jobId)
						.getMessages();
				cmd = runningCommands.get(jobId).getCommand();
				// myBaseNode.jobCompleted(jobId, cmd);
				messages.add(message);
				CommandResult cresult = createCommandResult(statusCode,
						messages, cmd);
				runningCommands.get(jobId).addCommandResult(cresult);
			}
		}

	}

	@Override
	public void queueBlockingCommand(Command command) {
		// TODO Auto-generated method stub

	}

	@Override
	public RunningTask nonBlockingQueueSupportingCommand(Command cmd,
			RunningTask rt) {

		// TODO this may be a little dangerous, because we can not adhere to
		// the serial nature of a command (e.g. x happens before y)
		RunningTask supportTask = new RunningTask(this, cmd, getNewTaskID());
		childTaskToParentTask.put(supportTask, rt);
		synchronized (runningCommands) {
			runningCommands.put(supportTask.getTaskId(), supportTask);
		}
		supportTask.start();
		return supportTask;
	}

	@Override
	public CMD_STATE blockingQueueSupportingCommand(Command cmd, RunningTask rt) {
		RunningTask supportTask = new RunningTask(this, cmd, getNewTaskID());

		childTaskToParentTask.put(supportTask, rt);
		synchronized (runningCommands) {
			runningCommands.put(supportTask.getTaskId(), supportTask);
		}
		supportTask.start();
		Thread supportTaskThread = supportTask.getMyMainThread();
		try {
			// support task Thread is the RunningTask's main thread and it will
			// run until it completes all the commands
			supportTaskThread.join();

		} catch (InterruptedException e) {
			// FIXME command failed, really? Lazy way to handle the exception
			return CMD_STATE.FAILED;
		}
		// FIXME is this the appropriate way to wait for the
		// command result to be completed.
		return supportTask.getMyLastCommandState();
	}

	private String getNewTaskID() {
		return (jobIds++).toString();
	}

	public void addNewCommand(Command cmd) throws InterruptedException {
		// TODO this priority queue is being mis-used
		// a command should be added then a thread polls the number of
		// executing commands, and if we ever drop below that number,
		// then we start the next command.

		pendingCmdQueue.add(cmd);
		startNextCmd();
	}

	public CommandResult createCommandResult(int statusCode,
			ArrayList<String> messages, Command cmd) {

		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultStatusCode(statusCode);
		cmdResult.setCommandResultMessages(messages);
		cmdResult.setCommandResultId(cmd.getCommandId());
		cmdResult.setCommandResultName(cmd.getCommandName());
		// FIXME make sure the file names are fully qualified
		ArrayList<FileContainer> fcs = new ArrayList<FileContainer>();
		for (String fname : cmd.getResultFiles()) {
			FileContainer fc = new FileContainer();
			// this will read the file automatically
			fc.setFromFullPath(fname);
			fcs.add(fc);
		}
		cmdResult.setCommandResultFiles(fcs);
		return cmdResult;
	}

	@Override
	public void commandCompleted(Command cmd, String taskId, RunningTask rt) {

		if (childTaskToParentTask.containsKey(rt)
				&& childTaskToParentTask.get(rt) != null) {
			RunningTask parent = childTaskToParentTask.get(rt);
			synchronized (parent) {
				while (!rt.isWholeCommandComplete()) {
				}
				parent.addCommandResults(rt.getCommandResults()
						.getCommandResults());

				synchronized (childTaskToParentTask) {
					childTaskToParentTask.remove(rt);
				}
			}
		}

		myBaseNode.commandCompleted(rt);
		synchronized (runningCommands) {
			runningCommands.remove(rt);
		}

	}

}
