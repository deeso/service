/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.Component;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.ComponentSaxHandler;
import com.thecoverofnight.service.xml.FileContainerXMLReaderWriter;

public class FileContainer implements CommandData, Component {
	private static Logger LOGGER = Logger.getLogger(FileContainer.class);
	static final FileContainerXMLReaderWriter FC_XML = FileContainerXMLReaderWriter.FC_XML_ONLY;
	boolean deleteDestDirectory = false;
	boolean fileDataHasBeenWritten = false;

	String myFilename = new String(""), myFiledata = new String(""),
			myOriginalBaseDirectory = new String(""),
			myDestinationBaseDirectory = new String(""),
			myPermissions = new String("");

	public FileContainer() {
	}

	public FileContainer(String filename, String baseDirectory) {
		setFilename(filename);
		setOriginalBaseDirectory(baseDirectory);
		setDestinationBaseDirectory(baseDirectory);
		readDataFromFile();
	}

	public FileContainer(String filename, String baseDirectory,
			String envBaseDirectory) {
		setFilename(filename);
		setOriginalBaseDirectory(baseDirectory);
		setDestinationBaseDirectory(envBaseDirectory);
		readDataFromFile();
	}

	public FileContainer(String filename, String baseDirectory,
			String envBaseDirectory, String fileData) {
		setFilename(filename);
		setOriginalBaseDirectory(baseDirectory);
		setDestinationBaseDirectory(envBaseDirectory);
		myFiledata = fileData;
	}

	public FileContainer(String filename, String baseDirectory,
			String envBaseDirectory, String fileData, String permisions) {
		setFilename(filename);
		setOriginalBaseDirectory(baseDirectory);
		setDestinationBaseDirectory(envBaseDirectory);
		myFiledata = fileData;
		myPermissions = permisions;
	}

	@Override
	public void appendDataFromString(String data) throws IOException {
		String result = new String(Base64.decode(getFiledata())) + data;
		setFiledata(Base64.encodeBytes(result.getBytes()));
		appendDataToFile(data);
	}

	@Override
	public void appendDataFromString64(String data) throws IOException {
		appendDataFromString(new String(Base64.decode(data)));
	}

	public boolean appendDataToFile(String strData) {

		if (!fileDataHasBeenWritten)
			return writeDataToFile();
		// fileDataHasBeenWritten will be false if the Data has not been written
		// to file yet or it failed to be writtent to file.

		File file = new File(getJoinedDestFilename());
		try {
			byte[] data = strData.getBytes();
			DataOutputStream dos;
			dos = new DataOutputStream(new BufferedOutputStream(
					new FileOutputStream(file, true)));
			dos.write(data);
			dos.flush();
			dos.close();
			return true;
		} catch (FileNotFoundException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		return false;
	}

	@Override
	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException {

		if (handler.checkTag(FC_XML.XML_FILE_DATA)) {
			handler.endTag(FC_XML.XML_FILE_DATA);
			setFiledata(new String(ch, start, length));
		}
		if (handler.checkTag(FC_XML.XML_FILE_NAME)) {
			handler.endTag(FC_XML.XML_FILE_NAME);
			setFilename(new String(ch, start, length));
		}
		if (handler.checkTag(FC_XML.XML_FILE_ORIGINAL_BASE)) {
			handler.endTag(FC_XML.XML_FILE_ORIGINAL_BASE);
			setOriginalBaseDirectory(new String(ch, start, length));
		}
		if (handler.checkTag(FC_XML.XML_FILE_DESTINATION_BASE)) {
			handler.endTag(FC_XML.XML_FILE_DESTINATION_BASE);
			setDestinationBaseDirectory(new String(ch, start, length));
		}
		if (handler.checkTag(FC_XML.XML_FILE_PERMISSIONS)) {
			handler.endTag(FC_XML.XML_FILE_PERMISSIONS);
			setPermissions(new String(ch, start, length));
		}

	}

	@Override
	public void cleanupData() {
		// FIXME add real exception handling code
		try {
			cleanupFile();
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}

	}

	public boolean cleanupFile() throws IOException {

		File file = new File(getDestinationBaseDirectory());
		if (isDeleteDestDirectory() && file.exists() && file.canWrite()
				&& file.isDirectory() && file.list().length < 2) {
			// delete the file, then delete the diretory
			FileUtils.deleteDirectory(file);
			return !file.exists();
		}
		file = new File(getJoinedDestFilename());
		if (file.exists() && file.canWrite()) {
			return file.delete();
		}

		throw new IOException();
	}

	private boolean createDestinationDirectory() {
		File dir = new File(getDestinationBaseDirectory());
		if (!dir.exists()) {
			return dir.mkdirs();
		}
		return dir.exists();
	}

	@Override
	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException {
		if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)) {
			handler.endParsingComponent(qName);
		}

	}

	@Override
	public void fromTransport(Node doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		FC_XML.fromTransport(this, doc);
	}

	@Override
	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		FC_XML.fromTransport(this, xml);

	}

	@Override
	public void getAttribute(String key) {
		// TODO Auto-generated method stub
	}

	@Override
	public HashMap<String, String> getAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getDestinationBaseDirectory() {
		return myDestinationBaseDirectory;
	}

	public String getFiledata() {
		return myFiledata;
	}

	public String getFilename() {
		return myFilename;
	}

	@Override
	public ComponentSaxHandler getHandler() {
		return FC_XML.createDefaultHandler();
	}

	public String getJoinedDestFilename() {
		String sepaString = System.getProperty(new String("file.separator"));
		return new String(myDestinationBaseDirectory + sepaString + myFilename);
	}

	public String getJoinedOrigFilename() {
		String sepaString = System.getProperty(new String("file.separator"));
		return new String(myOriginalBaseDirectory + sepaString + myFilename);
	}

	public String getOriginalBaseDirectory() {
		return myOriginalBaseDirectory;
	}

	public String getPermissions() {
		return myPermissions;
	}

	public boolean isDeleteDestDirectory() {
		return deleteDestDirectory;
	}

	@Override
	public void prepareData() {
		// FIXME add real exception handling code
		writeDataToFile();
	}

	public boolean readDataFromFile() {

		File file = new File(getJoinedOrigFilename());
		DataInputStream dis;
		try {
			dis = new DataInputStream(new BufferedInputStream(
					new FileInputStream(file)));
			byte[] data = new byte[(int) file.length()];
			dis.read(data);
			myFiledata = Base64.encodeBytes(data);
			dis.close();
			return true;
		} catch (FileNotFoundException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		return false;
	}

	public String readDataToBase64() {
		return getFiledata();
	}

	@Override
	public String readDataToString() {
		try {
			return new String(Base64.decode(getFiledata()));
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		return null;
	}

	@Override
	public void setAttribute(String key, String val) {
		// TODO Auto-generated method stublkasdlk
	}

	public void setDeleteDestDirectory(boolean deleteDestDirectory) {
		this.deleteDestDirectory = deleteDestDirectory;
	}

	public void setDestinationBaseDirectory(String myDestinationBaseDirectory) {
		this.myDestinationBaseDirectory = trimFileSystemSeparator(myDestinationBaseDirectory
				.trim());
	}

	public void setFiledata(String myFiledata) {
		this.myFiledata = myFiledata;
	}

	public void setFilename(String myFilename) {
		this.myFilename = trimFileSystemSeparator(myFilename.trim());
	}

	public void setFromFullPath(String path) {
		File initialFile = new File(path);
		setFilename(initialFile.getName());
		setOriginalBaseDirectory(initialFile.getParent());
		setDestinationBaseDirectory("");
		readDataFromFile();
	}

	public void setFromFullPathAndDest(String path, String destPath) {
		setFromFullPath(path);
		setDestinationBaseDirectory(destPath);
	}

	public void setOriginalBaseDirectory(String myOriginalBaseDirectory) {
		this.myOriginalBaseDirectory = trimFileSystemSeparator(myOriginalBaseDirectory
				.trim());
	}

	public void setPermissions(String myPermissions) {
		this.myPermissions = trimFileSystemSeparator(myPermissions);
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException {

	}

	@Override
	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return FC_XML.toTransport(this);
	}

	@Override
	public Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		return FC_XML.toTransport(this, doc);
	}

	@Override
	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return FC_XML.toTransportString(this);
	}

	public String trimFileSystemSeparator(String str) {
		if (str == null)
			return "";
		String sepaString = System.getProperty(new String("file.separator"));
		return StringUtils.chomp(str.trim(), sepaString);
	}

	@Override
	public void writeDataFromBase64(String data) {
		setFiledata(data);
		writeDataToFile();
	}

	@Override
	public void writeDataFromString(String data) {
		writeDataFromBase64(Base64.encodeBytes(data.getBytes()));
	}

	public boolean writeDataToFile() {
		fileDataHasBeenWritten = false;
		if (!createDestinationDirectory()) {
			// this failed to create the right directory structure
			return fileDataHasBeenWritten;
		}
		File file = new File(getJoinedDestFilename());
		try {
			byte[] data = Base64.decode(myFiledata);
			DataOutputStream dos;
			dos = new DataOutputStream(new BufferedOutputStream(
					new FileOutputStream(file)));
			dos.write(data);
			dos.flush();
			dos.close();
			fileDataHasBeenWritten = true;
			return fileDataHasBeenWritten;
		} catch (FileNotFoundException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}

		return fileDataHasBeenWritten;
	}

}