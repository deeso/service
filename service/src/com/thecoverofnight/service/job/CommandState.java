/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

public class CommandState {
	public static enum CMD_STATE {
		PENDING, RUNNING, IDLE, FAILED, SUCCEEDED
	}

	public String taskId;
	public CMD_STATE cmdState;
	public Command cmd;

	public CommandState(String taskId, CMD_STATE cmdState, Command cmd) {
		this.taskId = taskId;
		this.cmdState = cmdState;
		this.cmd = cmd;
	}

	public CMD_STATE getCmdState() {
		return cmdState;
	}

	public void setCmdState(CMD_STATE cmdState) {
		this.cmdState = cmdState;
	}

	public String getTaskId() {
		return taskId;
	}

	public Command getCmd() {
		return cmd;
	}
}