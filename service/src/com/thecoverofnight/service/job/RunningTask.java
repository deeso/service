/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.job.CommandState.CMD_STATE;
import com.thecoverofnight.service.util.ExceptionToString;

public class RunningTask {
	private static Logger LOGGER = Logger.getLogger(RunningTask.class);
	Process myProcess;
	String myTaskId;
	Thread myMainThread = null;
	boolean jobCompleted = false;

	private CommandResults cResults;
	ArrayList<String> messages = new ArrayList<String>();

	void setWholeCommandComplete() {
		jobCompleted = true;
	}

	boolean isWholeCommandComplete() {
		return jobCompleted;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	private void readProcessOutput(Process process) throws IOException {
		LOGGER.debug("Reading the process stdout/stderr");
		InputStream stdout = process.getInputStream();
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(stdout));
		String line = reader.readLine();
		while (line != null) {
			messages.add(line);
			line = reader.readLine();
		}
		LOGGER.debug("Done reading the process stdout/stderr");
	}

	public void addCommandResults(ArrayList<CommandResult> cmdResults) {
		synchronized (this.cResults) {
			cResults.addCommandResults(cmdResults);
		}

	}

	public void addCommandResult(CommandResult cmdResult) {
		synchronized (this.cResults) {
			cResults.addCommandResult(cmdResult);
		}
	}

	// public ArrayList<CommandResult> getCommandResultArrayList() {
	// return cResults.getCommandResults();
	// }
	public CommandResults getCommandResults() {
		return cResults;
	}

	public Thread getMyMainThread() {
		return myMainThread;
	}

	public static final String COMMAND_PREP = "CommandPrep"; // followed by
	// commands
	public static final String COMMAND_PREPROCESS = "CommandPreprocess"; // followed
	// by
	// commands

	public static final String COMMAND_EXECUTABLE = "CommandExecutable"; // file
	// containers

	public static final String COMMAND_POST_PROCESS = "CommandPostProcess"; // followed
	// by
	// commands
	public static final String COMMAND_CLEANUP = "CommandCleanUp"; // //

	// followed
	// by
	// commands

	public static enum STATE {
		RUNNING_PREP_COMMANDS, RUNNING_PREPROCESS_COMMANDS, RUNNING_COMMAND, RUNNING_POSTPROCESS_COMMANDS, RUNNING_CLEANUP_COMMANDS,

		FAILED_RUNNING_PREP_COMMANDS, FAILED_RUNNING_PREPROCESS_COMMANDS, FAILED_RUNNING_COMMAND, FAILED_RUNNING_POSTPROCESS_COMMANDS, FAILED_RUNNING_CLEANUP_COMMANDS,

		TASK_NOT_STARTED, TASK_COMPLETED, TASK_FAILED, TASK_FAILURE
	};

	STATE[] myFailedStates = { STATE.FAILED_RUNNING_PREP_COMMANDS,
			STATE.FAILED_RUNNING_PREPROCESS_COMMANDS,
			STATE.FAILED_RUNNING_COMMAND,
			STATE.FAILED_RUNNING_POSTPROCESS_COMMANDS,
			STATE.FAILED_RUNNING_CLEANUP_COMMANDS, };
	String[] myCommands = { COMMAND_PREP, COMMAND_PREPROCESS,
			COMMAND_EXECUTABLE, COMMAND_POST_PROCESS, COMMAND_CLEANUP };

	private void initMyFailedCommandMapping() {
		for (int i = 0; i < myCommands.length; i++) {
			myFailedCommandMapping.put(myCommands[i], myFailedStates[i]);
		}
	}

	HashMap<String, RunningTask.STATE> myFailedCommandMapping = new HashMap<String, RunningTask.STATE>();

	boolean commandFailed = false;

	public static enum STATE_RESULTS {
		// negative state
		PREP_COMMANDS_FAILED, PREPROCESS_FAILED, COMMAND_FAILED, POSTPROCESS_COMMANDS_FAILED, CLEANUP_COMMANDS_FAILED,

		// positive state
		PREP_COMMANDS_SUCCEEDED, PREPROCESS_COMMANDS_SUCCEEDED, COMMAND_SUCCEEDED, POSTPROCESS_COMMANDS_SUCCEEDED, CLEANUP_COMMANDS_SUCCEEDED;
	};

	CMD_STATE myLastCommandState;

	public CMD_STATE getMyLastCommandState() {
		return myLastCommandState;
	}

	STATE myCurrentState;
	String currentCommandId;

	HashMap<String, ArrayList<CommandState>> myCommandsStates = new HashMap<String, ArrayList<CommandState>>();

	BufferedReader myProcessStdOutStream;
	BufferedReader myProcessStdErrStream;

	Command myCommand;
	Manager myManager;
	RunningTask me;

	Thread currentTask = null;
	String state = null;

	private boolean keepRunning = false, executePrepCommands = false,
			executePreProcessCommands = false,
			executePostProcessCommands = false, executeCleanupCommands = false;

	protected void stateMachineTransition() throws RuntimeException {

		if (commandFailed) {
			// FIXME possibly need a more appropriate exception here
			myMainThread.interrupt();
			throw new RuntimeException(
					"Command failed to execute appropriately, intervention needed");
		}

		switch (myCurrentState) {
		case TASK_NOT_STARTED:
			myCurrentState = STATE.RUNNING_PREP_COMMANDS;
			break;
		case RUNNING_PREP_COMMANDS:
			myCurrentState = STATE.RUNNING_PREPROCESS_COMMANDS;
			break;
		case RUNNING_PREPROCESS_COMMANDS:
			myCurrentState = STATE.RUNNING_COMMAND;
			break;

		case RUNNING_COMMAND:
			myCurrentState = STATE.RUNNING_POSTPROCESS_COMMANDS;
			break;
		case RUNNING_POSTPROCESS_COMMANDS:
			myCurrentState = STATE.RUNNING_CLEANUP_COMMANDS;
			break;
		case RUNNING_CLEANUP_COMMANDS:
			myCurrentState = STATE.TASK_COMPLETED;
			myMainThread.interrupt();
			break;
		case FAILED_RUNNING_PREP_COMMANDS:
		case FAILED_RUNNING_PREPROCESS_COMMANDS:
		case FAILED_RUNNING_COMMAND:
		case FAILED_RUNNING_POSTPROCESS_COMMANDS:
		case FAILED_RUNNING_CLEANUP_COMMANDS:
		case TASK_FAILED:
			myCurrentState = STATE.TASK_FAILURE;
			myMainThread.interrupt();
			break;

		default:
			myMainThread.interrupt();
			throw new RuntimeException("Running Task State is invalid.");
		}

		executeNextState();
	}

	private void executeNextState() {

		switch (myCurrentState) {
		case TASK_NOT_STARTED:
			// fall through
		case RUNNING_PREP_COMMANDS:
			if (executePrepCommands) {
				performPrepCommands();
				break;
			}
			stateMachineTransition();
			break;
		case RUNNING_PREPROCESS_COMMANDS:
			if (executePreProcessCommands) {
				performPreProcessCommands();
				break;
			}
			stateMachineTransition();
			break;
		case RUNNING_COMMAND:
			executeCommand();
			break;
		case RUNNING_POSTPROCESS_COMMANDS:
			if (executePostProcessCommands) {
				performPostProcessCommands();
				break;
			}
			stateMachineTransition();
			break;
		case RUNNING_CLEANUP_COMMANDS:
			if (executeCleanupCommands) {
				performCleanupCommands();
				break;
			}
			stateMachineTransition();
			break;
		case TASK_FAILURE:
		case TASK_COMPLETED:
			jobCompleted = true;
			myManager.commandCompleted(myCommand, myTaskId, this);

		default:
			break;

		}

	}

	private void performCommands(final String CommandType) {

		currentTask = new Thread(new Runnable() {
			@Override
			public void run() {
				for (CommandState cmdState : myCommandsStates.get(CommandType)) {
					CMD_STATE rState = myManager
							.blockingQueueSupportingCommand(cmdState.getCmd(),
									me);
					cmdState.setCmdState(rState);
					myLastCommandState = rState;
					if (rState == CMD_STATE.FAILED) {
						myCurrentState = getFailedState(CommandType);
						commandFailed = true;
						break;
					}
				}

				stateMachineTransition();
			}

		});
		currentTask.start();

	}

	private STATE getFailedState(String commandType) throws RuntimeException {

		if (!myFailedCommandMapping.containsKey(commandType)) {
			throw new RuntimeException(
					"Invalied command type passed into the performCommands: "
							+ commandType);
		}
		return myFailedCommandMapping.get(commandType);
	}

	private void performCleanupCommands() {
		myCurrentState = STATE.RUNNING_CLEANUP_COMMANDS;
		performCommands(COMMAND_CLEANUP);
	}

	private void executeCommand() {
		myCurrentState = STATE.RUNNING_COMMAND;
		CommandState cmdState = myCommandsStates.get(COMMAND_EXECUTABLE).get(0);
		performCommand(cmdState);
	}

	private void performPostProcessCommands() {
		myCurrentState = STATE.RUNNING_POSTPROCESS_COMMANDS;
		performCommands(COMMAND_POST_PROCESS);
	}

	private void performPreProcessCommands() {
		myCurrentState = STATE.RUNNING_PREPROCESS_COMMANDS;
		performCommands(COMMAND_PREPROCESS);

	}

	private void performPrepCommands() {
		myCurrentState = STATE.RUNNING_PREP_COMMANDS;
		performCommands(COMMAND_PREP);
	}

	public Process getProcess() {
		return myProcess;
	}

	public void setProcess(Process myProcess) {
		this.myProcess = myProcess;
	}

	public String getTaskId() {
		return myTaskId;
	}

	public void setTaskId(String myTaskId) {
		this.myTaskId = myTaskId;
	}

	public boolean isKeepRunning() {
		return keepRunning;
	}

	public void setKeepRunning(boolean keepRunning) {
		this.keepRunning = keepRunning;
	}

	public RunningTask(Manager jbManager, Command cmd, String taskId) {
		myCommand = cmd;
		cResults = new CommandResults(cmd.getCommandId());
		myManager = jbManager;
		myTaskId = taskId;
		me = this;
		myCurrentState = STATE.TASK_NOT_STARTED;
		initMyFailedCommandMapping();
		// add the states to the container
		addCommandState();
		addPrepCommandStates();
		addPreProcessCommandStates();
		addPostProcessCommandStates();
		addCleanupCommandStates();

	}

	private void addCommandState() {
		ArrayList<Command> cmd = new ArrayList<Command>();
		cmd.add(myCommand);
		addCommandStates(COMMAND_EXECUTABLE, cmd);
	}

	private void addPrepCommandStates() {
		executePrepCommands = true;
		addCommandStates(COMMAND_PREP, myCommand.getPrepCommands());
	}

	private void addPreProcessCommandStates() {
		executePreProcessCommands = true;
		addCommandStates(COMMAND_PREPROCESS, myCommand.getPreProcessCommands());
	}

	private void addPostProcessCommandStates() {
		executePostProcessCommands = true;
		addCommandStates(COMMAND_POST_PROCESS,
				myCommand.getPostProcessCommands());
	}

	private void addCleanupCommandStates() {
		executeCleanupCommands = true;
		addCommandStates(COMMAND_CLEANUP, myCommand.getCleanUpCommands());
	}

	private void addCommandStates(String commandType,
			ArrayList<Command> commands) {
		myCommandsStates.put(commandType, createCommandStates(commands));
	}

	private ArrayList<CommandState> createCommandStates(
			ArrayList<Command> commands) {

		ArrayList<CommandState> cmdStates = new ArrayList<CommandState>();
		for (Command command : commands) {
			cmdStates
					.add(new CommandState(myTaskId, CMD_STATE.PENDING, command));
		}
		return cmdStates;
	}

	public void setExecutePrepCommands(boolean executePrepCommands) {
		this.executePrepCommands = executePrepCommands;
	}

	public boolean isExecutePrepCommands() {
		return executePrepCommands;
	}

	public boolean isExecutePreProcessCommands() {
		return executePreProcessCommands;
	}

	public void setExecutePreProcessCommands(boolean executePreProcessCommands) {
		this.executePreProcessCommands = executePreProcessCommands;
	}

	public boolean isExecutePostProcessCommands() {
		return executePostProcessCommands;
	}

	public void setExecutePostProcessCommands(boolean executePostProcessCommands) {
		this.executePostProcessCommands = executePostProcessCommands;
	}

	public boolean isExecuteCleanupCommands() {
		return executeCleanupCommands;
	}

	public void setExecuteCleanupCommands(boolean executeCleanupCommands) {
		this.executeCleanupCommands = executeCleanupCommands;
	}

	boolean checkStderrStream() {
		try {
			return myProcessStdErrStream.ready();
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		keepRunning = false;
		return false;
	}

	boolean checkStdoutStream() {
		try {
			return myProcessStdOutStream.ready();
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));

		}
		keepRunning = false;
		return false;
	}

	void taskFailed(int statusCode, String message) {
		myManager.jobFailure(myTaskId, statusCode, message);
	}

	void performCommand(CommandState cmdState) {
		// build process

		currentTask = new Thread(new Runnable() {

			@Override
			public void run() {
				LOGGER.debug("starting the RunningTask: "+myCommand.getCommandId()+" "+myCommand.getCommandLine());
				ProcessBuilder pb = null;
				String cls = myCommand.getCommandLine();
				if (isShellCommand(cls) && cls.contains(" -c ")){
					String cmd = getShellCommand(cls),
					       args = getShellArg(cls);
					
					pb = new ProcessBuilder(cmd, "-c", args);
				}else{
					pb = new ProcessBuilder(tokenize(myCommand
							.getCommandLine()));
					 
				}
				//pb = new ProcessBuilder(myCommand
				//		.getCommandLine());
				 
				pb.redirectErrorStream(true);
				Map<String, String> env = pb.environment();
				HashMap<String, String> cmdEnv = myCommand
						.getCommandEnvironment();
				for (String varName : cmdEnv.keySet()) {
					env.put(varName, cmdEnv.get(varName));
				}

				if (myCommand != null && myCommand.getCommandCWD() != null
						&& myCommand.getCommandCWD().length() > 0)
					pb.directory(new File(myCommand.getCommandCWD()));
				try {
					myProcess = pb.start();
					// FIXME is this the way we want to handle processes?
					try {
						myProcess.waitFor();
						readProcessOutput(myProcess);
					} catch (InterruptedException e) {
						readProcessOutput(myProcess);
						myProcess = null;
						LOGGER.debug(ExceptionToString.toString(e));
						myCurrentState = STATE.FAILED_RUNNING_COMMAND;
						taskFailed(-1, ExceptionToString.toString(e));

					}

				} catch (IOException e) {

					messages.add("IOException when trying to start process for the RunningTask");
					LOGGER.debug("IOException when trying to start process for the RunningTask");
					LOGGER.debug(ExceptionToString.toString(e));

					int statusCode = -1;

					if (myManager != null && myProcess != null) {
						statusCode = myProcess.exitValue();
					} else if (myManager != null && myProcess == null) {
						myManager.jobFailure(myTaskId, -1, e.toString());
					}
					myCurrentState = STATE.FAILED_RUNNING_COMMAND;
					taskFailed(statusCode, ExceptionToString.toString(e));
					stateMachineTransition();
					return;
				}

				myManager.jobCompleted(myTaskId, myProcess.exitValue(),
						"yay done!");
				// TODO trigger job success clean up
				stateMachineTransition();
			}
		});
		currentTask.start();
	}

	String[] tokenize(String str) {
		ArrayList<String> results = new ArrayList<String>();
		String[] initResults = str.split(" ");

		for (int idx = 0; idx < initResults.length; idx++) {
			// combine backslash space delimited chars
			if (initResults[idx].endsWith("\\") && idx < initResults.length - 1) {
				results.add(initResults[idx] + " " + initResults[++idx]);
				continue;
			}
			results.add(initResults[idx]);
		}
		return results.toArray(new String[0]);
	}
	
	boolean isShellCommand(String str) {
		
		if ((str.contains("/bin/bash") && str.indexOf("/bin/bash") == 0) || 
				(str.contains("bash") && str.indexOf("bash") == 0) ||
				(str.contains("/bin/sh") && str.indexOf("/bin/sh") == 0) ||
				(str.contains("sh") && str.indexOf("sh") == 0)){
			// check for the -c flag.
			return str.contains(" -c ") && str.indexOf(" -c ") > 0;
		}
		return false;
	}
	
	String getShellCommand(String str) {
		
		if ((str.contains("/bin/bash") && str.indexOf("/bin/bash") == 0)){
			return new String("/bin/bash");
		} 

		else if (str.contains("bash") && str.indexOf("bash") == 0){
			return new String("bash");
		}
		else if (str.contains("/bin/sh") && str.indexOf("/bin/sh") == 0) {
			return new String("/bin/sh");
		}
		
		else if	(str.contains("sh") && str.indexOf("sh") == 0){
			return new String("sh");
		}
		return new String("/bin/sh");
	}
	
	String getShellArg(String str) {
		
		if ((str.contains(" -c ") && str.indexOf(" -c ") > 0)){
			String[] args = str.split(" -c ");
			if(args.length < 2){
				return new String("");
			}
			String new_str = "";
			// FIXME this parsing routing will not parse out all of the arguments, and that is a little frustrating
			// maybe using something like options parser will be more effective. 
			
			for (int i = 1; i < args.length; i++){
				new_str += args[i];
				if (i == 1 && new_str.charAt(0) == '"'){
					new_str = new_str.substring(2,new_str.length());
				}
				if (i == args.length-1 && new_str.charAt(new_str.length()-1) == '"'){
					new_str = new_str.substring(1, new_str.length()-1);
				}
				if (i != args.length-1){
					new_str += "-c";
				}
				if (i != args.length-1){
					new_str += "-c";
				}
			}
			return new_str;
		} 
		return new String("");
	}

	public void yield() {
		// TODO this may not be important in the grand scheme of things.
	}

	@SuppressWarnings("deprecation")
	public void poisonPill() {

		if (myProcess != null)
			myProcess.destroy();

		if (currentTask != null && currentTask.isAlive())
			currentTask.stop();

		if (myMainThread != null && myMainThread.isAlive())
			myMainThread.stop();

	}

	public void start() {

		// TODO kickstart the RunningTask, and continually monitor progress of
		// the
		// the children tasks, reap and handle accordingly
		if (myMainThread != null && myMainThread.isAlive()) {
			return;
		}

		myMainThread = new Thread(new Runnable() {

			@Override
			public void run() {
				stateMachineTransition();
				while (keepRunning && myCurrentState != STATE.TASK_COMPLETED
						&& !commandFailed) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// something happened here, need to handle the
						// interruption
						// but how?
						// TODO Not sure how to handle interruption
						LOGGER.debug(ExceptionToString.toString(e));
					}

				}

			}
		});
		myMainThread.start();

	}

	public Command getCommand() {
		return myCommand;
	}
}
