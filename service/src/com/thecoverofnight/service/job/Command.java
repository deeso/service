/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.util.Component;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.CommandXMLReaderWriter;
import com.thecoverofnight.service.xml.ComponentSaxHandler;

@SuppressWarnings("static-access")
public class Command implements Component, Comparable<Command> {
	private static Logger LOGGER = Logger.getLogger(Command.class);

	public static void main(String[] args) throws IOException {
		// check for legal argument list
		DOMConfigurator.configure("example/log4j.xml");
		// Command x = new Command();
		// FileContainer exe = new FileContainer();

	}

	CommandXMLReaderWriter CMD_XML = CommandXMLReaderWriter.CMD_XML_ONLY;
	ArrayList<Command> myCleanUpCommands = new ArrayList<Command>();

	// Preprocess and environmental settings before executing the command
	String myCommandCWD = "";

	private ArrayList<CommandData> myCommandData = new ArrayList<CommandData>();

	HashMap<String, String> myCommandEnvironment = new HashMap<String, String>();

	// Stuff needed to execute the command, e.g. exe, data, and Data
	FileContainer myCommandExecutable = new FileContainer();
	String myCommandId = "";

	String myCommandLine = "";

	String myCommandName = "";

	ArrayList<String> myCommandResultMessages = new ArrayList<String>();

	Manager myManager = null;

	ArrayList<Command> myPostProcessCommands = new ArrayList<Command>();

	ArrayList<Command> myPrepCommands = new ArrayList<Command>();

	ArrayList<Command> myPreProcessCommands = new ArrayList<Command>();

	ArrayList<String> myResults = new ArrayList<String>();

	public Command() {
	}

	public Command(String cmdXml) {
		String result = "";
		try {
			this.fromTransportString(cmdXml);
		} catch (IOException e) {
			result = "Experienced exceptions attempting to construct a new command from xml";
			result += ExceptionToString.toString(e);
			LOGGER.debug(result);
		} catch (ParserConfigurationException e) {
			result = "Experienced exceptions attempting to construct a new command from xml";
			result += ExceptionToString.toString(e);
			LOGGER.debug(result);
		} catch (SAXException e) {
			result = "Experienced exceptions attempting to construct a new command from xml";
			result += ExceptionToString.toString(e);
			LOGGER.debug(result);
		}
	}

	public void addCleanUpCommand(Command e) {
		myCleanUpCommands.add(e);
	}

	public boolean addCommandData(CommandData data) {
		return myCommandData.add(data);
	}

	public void addCommandMessage(String msg) {
		myCommandResultMessages.add(msg);
	}

	public void addEnvironmentVar(String key, String val) {
		myCommandEnvironment.put(key, val);
	}

	public void addPostProcessCommand(Command e) {
		myPostProcessCommands.add(e);
	}

	public void addPrepCommand(Command e) {
		myPrepCommands.add(e);
	}

	public void addPreProcessCommand(Command e) {
		myPostProcessCommands.add(e);
	}

	@Override
	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException {

		LOGGER.debug("Command.characters called \""
				+ new String(ch, start, length) + "\"");
		if (handler.checkTag(CMD_XML.XML_COMMAND_CWD)) {
			setCommandCWD(new String(ch, start, length));
			handler.endTag(CMD_XML.XML_COMMAND_CWD);
		} else if (handler.checkTag(CMD_XML.XML_COMMAND_ID)) {
			setCommandId(new String(ch, start, length));
			handler.endTag(CMD_XML.XML_COMMAND_ID);
		} else if (handler.checkTag(CMD_XML.XML_COMMAND_NAME)) {
			setCommandName(new String(ch, start, length));
			handler.endTag(CMD_XML.XML_COMMAND_NAME);
		}else if (handler.checkTag(CMD_XML.XML_COMMAND_RESULT)) {
			myResults.add(new String(ch, start, length));
			handler.endTag(CMD_XML.XML_COMMAND_RESULT);
		} 
		/*else if (handler.checkTag(CMD_XML.XML_COMMAND_LINE)) {
			setCommandLine(new String(ch, start, length));
			handler.endTag(CMD_XML.XML_COMMAND_LINE);
		} */
		

	}

	@Override
	public int compareTo(Command o) {
		// FIXME create a more elaborate comparison technique

		String oId = o.getCommandId(), tId = getCommandId();

		// try to compare two ints
		try {
			Integer oIdint = Integer.parseInt(oId), tIdint = Integer
					.parseInt(tId);

			return tIdint.compareTo(oIdint);
		} catch (NumberFormatException e) {
		}
		// try to compare two strings
		return tId.compareTo(oId);
	}

	@Override
	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException {

		if (handler.quickTagCheck(CMD_XML.XML_COMMAND_ENV_VARS, qName)) {
			handler.endTag(CMD_XML.XML_COMMAND_ENV_VARS);
		} else if (handler.quickTagCheck(CMD_XML.XML_COMMAND_EXECUTABLE, qName)) {

			handler.endTag(CMD_XML.XML_COMMAND_ENV_VARS);
		}

	}

	@Override
	public void fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		CMD_XML.fromTransport(this, n);

	}

	@Override
	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException {
		CMD_XML.fromTransport(this, xml);

	}

	public ArrayList<Command> getCleanUpCommands() {
		return myCleanUpCommands;
	}

	public String getCommandCWD() {
		return myCommandCWD;
	}

	public ArrayList<CommandData> getCommandData() {
		return myCommandData;
	}

	public CommandData getCommandDataByIndex(int index) {
		if (index < myCommandData.size())
			return myCommandData.get(index);
		return null;
	}

	public HashMap<String, String> getCommandEnvironment() {
		return myCommandEnvironment;
	}

	public FileContainer getCommandExecutable() {
		return myCommandExecutable;
	}

	public String getCommandId() {
		return myCommandId;
	}

	public String getCommandLine() {
		return myCommandLine;
	}

	public String getCommandName() {
		return myCommandName;
	}

	public String[] getCommandResultMessages() {
		return myCommandResultMessages.toArray(new String[0]);
	}

	public ArrayList<CommandData> getData() {
		return getCommandData();
	}

	public CommandData getFirstCommandData() {
		if (0 < myCommandData.size())
			return myCommandData.get(0);
		return null;
	}

	@Override
	public ComponentSaxHandler getHandler() {
		return CMD_XML.createDefaultHandler();
	}

	public CommandData getLastCommandData() {
		if (0 < myCommandData.size())
			return myCommandData.get(myCommandData.size() - 1);
		return null;
	}

	public Manager getManager() {
		return myManager;
	}

	public Manager getParent() {
		return myManager;
	}

	public ArrayList<Command> getPostProcessCommands() {
		return myPostProcessCommands;
	}

	public ArrayList<Command> getPrepCommands() {
		return myPrepCommands;
	}

	public ArrayList<Command> getPreprocessCommands() {
		return myPreProcessCommands;
	}

	public ArrayList<Command> getPreProcessCommands() {
		return myPreProcessCommands;
	}

	public String[] getResultFiles() {
		return myResults.toArray(new String[0]);
	}
	public void setResultFiles( ArrayList<String> resultFiles) {
		myResults.clear();
		addResultFiles(resultFiles);
	}
	public void setResultFiles( String[] resultFiles) {
		myResults.clear();
		addResultFiles(resultFiles);
	}
	public void addResultFile( String resultFile) {
		myResults.add(resultFile);
	}
	public void addResultFiles( String[] resultFiles) {
		for(String resultFile: resultFiles)
			myResults.add(resultFile);
	}
	public void addResultFiles( ArrayList<String> resultFiles) {
		for(String resultFile: resultFiles)
			myResults.add(resultFile);
	}

	public void setCleanUpCommands(ArrayList<Command> myCleanUpCommands) {
		this.myCleanUpCommands = myCleanUpCommands;
	}

	public void setCommandCWD(String myCommandCWD) {
		this.myCommandCWD = myCommandCWD;
	}

	// data or file that we will operate on

	public void setCommandData(ArrayList<CommandData> myCommandData) {
		this.myCommandData = myCommandData;
	}

	public void setCommandEnvironment(
			HashMap<String, String> myCommandEnvironment) {
		this.myCommandEnvironment = myCommandEnvironment;
	}

	public void setCommandExecutable(FileContainer myCommandExecutable) {
		this.myCommandExecutable = myCommandExecutable;
	}

	public void setCommandId(String myCommandId) {
		this.myCommandId = myCommandId;
	}

	public void setCommandLine(String myCommandLine) {
		this.myCommandLine = myCommandLine;
	}

	public void setCommandName(String myCommandName) {
		this.myCommandName = myCommandName;
	}

	public void setData(ArrayList<CommandData> myData) {
		this.setCommandData(myData);
	}

	public void setManager(Manager myManager) {
		this.myManager = myManager;
	}

	public void setParent(Manager parent) {
		this.myManager = parent;
	}

	public void setPostProcessCommands(ArrayList<Command> myPostProcessCommands) {
		this.myPostProcessCommands = myPostProcessCommands;
	}

	public void setPrepCommands(ArrayList<Command> myPrepCommands) {
		this.myPrepCommands = myPrepCommands;
	}

	public void setPreprocessCommands(ArrayList<Command> myPreprocessCommands) {
		this.myPreProcessCommands = myPreprocessCommands;
	}

	public void setPreProcessCommands(ArrayList<Command> myPreProcessCommands) {
		this.myPreProcessCommands = myPreProcessCommands;
	}

	public void setWorkingDirectory(String myWorkingDirectory) {
		this.myCommandCWD = myWorkingDirectory;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException {

		if (handler.checkTag(CMD_XML.XML_COMMAND_ENV_VARS)
				&& handler.checkTag(CMD_XML.XML_COMMAND_ENV_VAR)) {
			String val = null, key = null;
			key = attributes.getValue(CMD_XML.XML_COMMAND_ENV_VAR_KEY);
			val = attributes.getValue(CMD_XML.XML_COMMAND_ENV_VAR_VAL);
			if (key != null && val != null) {
				addEnvironmentVar(key, val);
			}
			handler.endTag(CMD_XML.XML_COMMAND_ENV_VAR);
		}else if (handler.checkTag(CMD_XML.XML_COMMAND_LINE)) {
			setCommandLine(StringEscapeUtils.unescapeXml(
					attributes.getValue(CMD_XML.XML_COMMAND_CMD_LINE_VALUE)));
			handler.endTag(CMD_XML.XML_COMMAND_LINE);
		}

	}

	@Override
	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMD_XML.toTransport(this);
	}

	@Override
	public Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		return CMD_XML.toTransport(this, doc);
	}

	@Override
	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return CMD_XML.toTransportString(this);
	}

}
