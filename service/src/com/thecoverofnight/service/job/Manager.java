/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.job;

import com.thecoverofnight.service.job.CommandState.CMD_STATE;

public interface Manager {

	void jobFailure(String myTaskId, int i, String string);

	public void jobCompleted(String jobId, int statusCode, String message);

	public void yielding(String jobId);

	void queueBlockingCommand(Command command);

	RunningTask nonBlockingQueueSupportingCommand(Command cmd, RunningTask rt);

	CMD_STATE blockingQueueSupportingCommand(Command cmd, RunningTask rt);

	public void commandCompleted(Command cmd, String taskId, RunningTask rt);
}
