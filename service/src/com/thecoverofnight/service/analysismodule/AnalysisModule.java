/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.analysismodule;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.Component;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.AnalysisModuleXMLReaderWriter;
import com.thecoverofnight.service.xml.ComponentSaxHandler;

public class AnalysisModule implements Component {

	String myName;
	String myCode;
	String myModuleType;
	String myAvailability;
	private static Logger LOGGER = Logger.getLogger(AnalysisModule.class);
	ArrayList<String> myDependencies = new ArrayList<String>();

	public void setName(String myName) {
		this.myName = myName;
	}

	public void setCode(String myCode) {
		this.myCode = myCode;
	}

	public void setModuleType(String myModuleType) {
		this.myModuleType = myModuleType;
	}

	public void setDependencies(ArrayList<String> myDependencies) {
		this.myDependencies = myDependencies;
	}

	public String getAvailability() {
		return myAvailability;
	}

	public void setAvailability(String myAvailability) {
		this.myAvailability = myAvailability;
	}

	public String[] getDependencies() {
		return myDependencies.toArray(new String[0]);
	}

	public void addDependencies(String dependency) {
		this.myDependencies.add(dependency);
	}

	public String getName() {
		return myName;
	}

	public String getCode() {
		return myCode;
	}

	public String getModuleType() {
		return myModuleType;
	}

	public AnalysisModule(String name, String type, String availabitlity) {
		myName = name;
		myModuleType = type;
		myAvailability = availabitlity;
	}

	public AnalysisModule() {
		myName = "";
		myModuleType = "";
		myAvailability = "";

	}

	public boolean readCode(String filename) {
		File file = new File(filename);
		DataInputStream dis;
		try {
			dis = new DataInputStream(new BufferedInputStream(
					new FileInputStream(file)));
			byte[] data = new byte[(int) file.length()];
			dis.read(data);
			myCode = new String(data);
			return true;
		} catch (FileNotFoundException e) {
			LOGGER.debug(ExceptionToString.toString(e));
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
			LOGGER.debug(ExceptionToString.toString(e));
		}
		// IMPORTANT: this is only followed if an exception above occurs
		myCode = null;
		return false;
	}

	@Override
	public ComponentSaxHandler getHandler() {

		return AnalysisModuleXMLReaderWriter.AM_XML_ONLY.createDefaultHandler();

	}

	@Override
	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return AnalysisModuleXMLReaderWriter.AM_XML_ONLY
				.toTransportString(this);
	}

	@Override
	public void fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		AnalysisModuleXMLReaderWriter.AM_XML_ONLY.fromTransport(n);

	}

	@Override
	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException {
		AnalysisModuleXMLReaderWriter.AM_XML_ONLY.fromTransport(xml);

	}

	@Override
	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return AnalysisModuleXMLReaderWriter.AM_XML_ONLY.toTransport(this);
	}

	@Override
	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException {
		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_NAME)) {
			setName(new String(ch, start, length));
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_NAME);
		}

		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_CODE)) {
			// FIXME do i want to throw a more appropriate exception here
			String code = new String(ch, start, length);
			try {
				setCode(new String(Base64.decode(code)));
			} catch (IOException e) {

				LOGGER.debug(ExceptionToString.toString(e));
				setCode("");
			}
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_CODE);
		}
		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_DEPENDENCY)) {
			addDependencies(new String(ch, start, length));
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_DEPENDENCY);
		}

		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_TYPE)) {
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_TYPE);
			setModuleType(new String(ch, start, length));
		}
		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_NAME)) {
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_NAME);
			setName(new String(ch, start, length));
		}
		if (handler
				.checkTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_AVAILABLE)) {
			handler.endTag(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_AVAILABLE);
			setAvailability(new String(ch, start, length));
		}

	}

	@Override
	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException {
		if (qName
				.equalsIgnoreCase(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE)) {
			handler.endParsingComponent(qName);
		}

	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException {
		// FIXME is there anything that should be done here.

	}

	@Override
	public Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		// TODO Auto-generated method stub
		return null;
	}
}
