/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */
package com.thecoverofnight.service.tests;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.CommandResult;
import com.thecoverofnight.service.job.FileContainer;

public class CommandResultTest extends ExperimentalTests {

	@Test
	public void testBasicSetters() {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";

		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultId(testCmdId);
		cmdResult.setCommandResultName(testCmdName);
		cmdResult.addMessage(testMsg);
		cmdResult.setCommandResultStatusCode(0);

		assertTrue(cmdResult.getCommandResultName() == testCmdName);
		assertTrue(cmdResult.getCommandResultId() == testCmdId);
		assertTrue(cmdResult.getCommandResultMessages().get(0) == testMsg);
		assertTrue(cmdResult.getCommandResultStatusCode() == 0);
	}

	@Test
	public void testToTransportString() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";

		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultId(testCmdId);
		cmdResult.setCommandResultName(testCmdName);
		cmdResult.addMessage(testMsg);
		cmdResult.addMessage(testMsg + " Another one?");
		cmdResult.setCommandResultStatusCode(0);

		System.out.print(cmdResult.toTransportString());

	}

	@Test
	public void testToTransportStringFC() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";

		String xmlTestFile = "tests/xmlFiles/cmdResult_test.xml";

		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultId(testCmdId);
		cmdResult.setCommandResultName(testCmdName);
		cmdResult.addMessage(testMsg);
		cmdResult.addMessage(testMsg + " Another one?");
		cmdResult.setCommandResultStatusCode(0);
		FileContainer x = new FileContainer();
		x.setFromFullPath(xmlTestFile);
		cmdResult.addFileContainer(x);

		System.out.print(cmdResult.toTransportString());

	}

	@Test
	public void testFromTransportString() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";
		String xmlTestFile = "tests/xmlFiles/cmdResult_test.xml";
		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultId(testCmdId);
		cmdResult.setCommandResultName(testCmdName);
		cmdResult.addMessage(testMsg);
		cmdResult.addMessage(testMsg + " Another one?");
		cmdResult.setCommandResultStatusCode(0);

		String data = readDataFromFile(xmlTestFile);

		CommandResult from = new CommandResult();
		from.fromTransportString(data);

		assertTrue(cmdResult.getCommandResultName().equals(
				from.getCommandResultName()));
		assertTrue(cmdResult.getCommandResultId().equals(
				from.getCommandResultId()));
		assertTrue(cmdResult.getCommandResultMessages().get(0)
				.equals(from.getCommandResultMessages().get(0)));
		assertTrue(cmdResult.getCommandResultStatusCode() == from
				.getCommandResultStatusCode());

	}

	@Test
	public void testFromTransportStringFC() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";
		String xmlTestFile = "tests/xmlFiles/cmdResult_test.xml";
		String xmlTestFileFC = "tests/xmlFiles/cmdResult_testfc.xml";
		CommandResult cmdResult = new CommandResult();
		cmdResult.setCommandResultId(testCmdId);
		cmdResult.setCommandResultName(testCmdName);
		cmdResult.addMessage(testMsg);
		cmdResult.addMessage(testMsg + " Another one?");
		cmdResult.setCommandResultStatusCode(0);
		FileContainer x = new FileContainer();
		x.setFromFullPath(xmlTestFile);
		cmdResult.addFileContainer(x);

		String data = readDataFromFile(xmlTestFileFC);

		CommandResult from = new CommandResult();
		from.fromTransportString(data);

		assertTrue(cmdResult.getCommandResultName().equals(
				from.getCommandResultName()));
		assertTrue(cmdResult.getCommandResultId().equals(
				from.getCommandResultId()));
		assertTrue(cmdResult.getCommandResultMessages().get(0)
				.equals(from.getCommandResultMessages().get(0)));
		assertTrue(cmdResult.getCommandResultStatusCode() == from
				.getCommandResultStatusCode());

		String result = cmdResult.toTransportString();

		assertTrue(result.equals(data));

	}

}
