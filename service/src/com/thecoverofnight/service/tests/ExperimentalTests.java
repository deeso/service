/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.tests;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.thecoverofnight.service.util.Base64;

public abstract class ExperimentalTests {
	public ExperimentalTests() {
		DOMConfigurator.configure("example/log4j.xml");
	}

	static Document createDocument() throws ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		return docBuilder.newDocument();
	}

	String readDataFromFile(String path) throws IOException {
		DataInputStream dis;
		dis = new DataInputStream(new BufferedInputStream(new FileInputStream(
				new File(path))));

		byte[] data = new byte[(int) new File(path).length()];
		dis.read(data);
		return new String(data);

	}

	String readDataFromFile64(String path) throws IOException {
		return Base64.encodeBytes(readDataFromFile(path).getBytes());
	}

	String getFSSeparator() {
		return System.getProperty(new String("file.separator"));
	}

	String getJoinedPath(String path, String file) {

		return new String(path + getFSSeparator() + file);
	}

	Element getElementFromXMLString(String str) {
		return null;
	}

}
