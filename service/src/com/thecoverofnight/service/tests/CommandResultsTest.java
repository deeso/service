/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */
package com.thecoverofnight.service.tests;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.junit.Test;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.CommandResult;
import com.thecoverofnight.service.job.CommandResults;
import com.thecoverofnight.service.job.FileContainer;

public class CommandResultsTest extends ExperimentalTests {

	@Test
	public void testBasicSetters() {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";

		CommandResults cmdResults = new CommandResults();

		CommandResult cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId);
		cResult.setCommandResultName(testCmdName);
		cResult.addMessage(testMsg);
		cResult.setCommandResultStatusCode(0);

		cmdResults.addCommandResult(cResult);
		CommandResult cmdResult = cmdResults.getResults().get(0);

		assertTrue(cmdResult.getCommandResultName() == testCmdName);
		assertTrue(cmdResult.getCommandResultId() == testCmdId);
		assertTrue(cmdResult.getCommandResultMessages().get(0) == testMsg);
		assertTrue(cmdResult.getCommandResultStatusCode() == 0);
	}

	@Test
	public void testToTransportString() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";

		CommandResults cmdResults = new CommandResults();

		CommandResult cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId);
		cResult.setCommandResultName(testCmdName);
		cResult.addMessage(testMsg);
		cResult.setCommandResultStatusCode(0);

		cmdResults.addCommandResult(cResult);

		System.out.print(cmdResults.toTransportString());

	}

	@Test
	public void testToTransportStringMultiple() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";
		String xmlTestFile = "tests/xmlFiles/cmdResult_test.xml";
		CommandResults cmdResults = new CommandResults();

		CommandResult cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId);
		cResult.setCommandResultName(testCmdName);
		cResult.addMessage(testMsg);
		cResult.setCommandResultStatusCode(0);

		cmdResults.addCommandResult(cResult);

		cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId + " 1");
		cResult.setCommandResultName(testCmdName + " 1");
		cResult.addMessage(testMsg + " 1");
		cResult.setCommandResultStatusCode(10);
		FileContainer x = new FileContainer();
		x.setFromFullPath(xmlTestFile);
		cResult.addFileContainer(x);
		cmdResults.addCommandResult(cResult);

		System.out.print(cmdResults.toTransportString());

	}

	@Test
	public void testFromTransportString() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String testCmdId = "testId", testCmdName = "testCmdName", testMsg = "testMsg";
		String xmlTestFile = "tests/xmlFiles/cmdResult_test.xml";
		String xmlTestFiles = "tests/xmlFiles/cmdResults_test.xml";
		CommandResults cmdResults = new CommandResults();

		CommandResult cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId);
		cResult.setCommandResultName(testCmdName);
		cResult.addMessage(testMsg);
		cResult.setCommandResultStatusCode(0);

		cmdResults.addCommandResult(cResult);

		cResult = new CommandResult();
		cResult.setCommandResultId(testCmdId + " 1");
		cResult.setCommandResultName(testCmdName + " 1");
		cResult.addMessage(testMsg + " 1");
		cResult.setCommandResultStatusCode(10);
		FileContainer x = new FileContainer();
		x.setFromFullPath(xmlTestFile);
		cResult.addFileContainer(x);
		cmdResults.addCommandResult(cResult);

		String data = readDataFromFile(xmlTestFiles);

		CommandResults from = new CommandResults();
		from.fromTransportString(cmdResults.toTransportString());
		assertEquals(cmdResults.toTransportString(), from.toTransportString());
		assertEquals(data, from.toTransportString());
		assertTrue(from.toTransportString().equals(data));

	}

	@Test
	public void testWhatDoesDocNameReturn() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		String xmlTestFiles = "tests/xmlFiles/cmdResults_test.xml";

		DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		docBuilder.newDocument();
		String data = readDataFromFile(xmlTestFiles);

		CommandResults from = new CommandResults();
		from.fromTransportString(data);
		Element node = docBuilder.parse(
				new ByteArrayInputStream(data.getBytes())).getDocumentElement();

		String test = node.getNodeName();
		String test1 = node.getNodeValue();

		System.out.print(test + "\n" + test1);

	}

}
