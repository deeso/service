/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.FileContainer;

public class CommandTest extends ExperimentalTests {

	@Test
	public void testAddEnvironmentVar() {
		Command cmd = new Command();
		cmd.addEnvironmentVar("PATH", "/home/dso/cool");
		cmd.addEnvironmentVar("PATH2", "/home/dso/coolness");
		assertTrue(cmd.getCommandEnvironment().size() == 2);
	}

	@Test
	public void testCreateBasicOSCommand() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		Command cmd = new Command();
		String cwd = "tests/cmdWorkingDir", xmlTestCmp = "tests/xmlFiles/fileContainerXML_test.txt.txt.xml", outString = "";

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch testfile.txt");
		cmd.setCommandId("Adam's First Basic Command");
		outString = cmd.toTransportString();
		assertEquals("Basic Command Creation", readDataFromFile(xmlTestCmp),
				outString);
	}

	@Test
	public void testBasicCommandWithPrep() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		Command cmd = new Command(), prep = new Command();
		String cwd = "tests/cmdWorkingDir", xmlTestCmp = "tests/xmlFiles/command_basic_prep_cmd.xml", outputFile = "tests/cmdWorkingDir/testfile.txt", outString = "";

		File outDir = new File(cwd);
		outDir.mkdirs();

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("cat test >" + outputFile);
		cmd.setCommandId("Adam's First Basic Command");
		cmd.addResultFile(outputFile);

		prep.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		prep.setCommandName("Prep command to Create Empty File");
		prep.setCommandLine("mkdir -p " + cwd);
		prep.setCommandId("Adam's First Prep Command");
		

		cmd.addPrepCommand(prep);
		outString = cmd.toTransportString();
		System.out.print(outString);

		String testOutput = readDataFromFile(xmlTestCmp);
		// System.out.println(outString);
		assertEquals("Basic Command Creation", testOutput.trim(),
				outString.trim());

	}

	@Test
	public void testBasicCommandWithFilecontainerData() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		Command cmd = new Command(), prep = new Command();
		String cwd = "tests/cmdWorkingDir", pythonFile = "tests/data/python_code.py", xmlTestCmp = "tests/xmlFiles/command_basic_cmd_prep_fc.xml", outString = "";

		File outDir = new File(cwd);
		outDir.mkdirs();

		// create a data element and add it to the command
		FileContainer fc = new FileContainer();
		fc.setFromFullPath(pythonFile);
		cmd.addCommandData(fc);

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("python " + pythonFile);
		cmd.setCommandId("Adam's First Basic Command");

		prep.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		prep.setCommandName("Prep command to Create Dest Dir");
		prep.setCommandLine("mkdir -p " + cwd);
		prep.setCommandId("Adam's First Prep Command");

		cmd.addPrepCommand(prep);
		outString = cmd.toTransportString();

		String testOutput = readDataFromFile(xmlTestCmp);
		// System.out.println(outString);
		assertEquals("Basic Command Creation with a FileContainer",
				testOutput.trim(), outString.trim());

	}

	@Test
	public void testCommandWithPrepFileContainerFromTransport()
			throws DOMException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			SAXException, IOException, InterruptedException {
		Command cmd = new Command();
		String xmlTestCmp = "tests/xmlFiles/command_basic_cmd_prep_fc.xml", outString = "";
		String xmlData = readDataFromFile(xmlTestCmp);
		cmd.fromTransportString(xmlData);

		outString = cmd.toTransportString();

		String testOutput = readDataFromFile(xmlTestCmp);
		System.out.println(outString);
		assertEquals("Basic Command Creation with a FileContainer",
				testOutput.trim(), outString.trim());

	}

	@Test
	public void testAddPreProcessCommand() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddPostProcessCommand() {
		fail("Not yet implemented");
	}

	@Test
	public void testAddCleanUpCommand() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPrepCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPrepCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPreProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPreProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPostProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPostProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetManager() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetManager() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCleanUpCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCleanUpCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandCWD() {
		fail("Not yet implemented");
	}

	@Test
	public void testCommand() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandEnvironment() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandExecutable() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandId() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandLine() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandName() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetData() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPreprocessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandCWD() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetParent() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandEnvironment() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandExecutable() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandId() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandLine() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandName() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetData() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetPreprocessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetWorkingDirectory() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetParent() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetCommandData() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommandData() {
		fail("Not yet implemented");
	}

	@Test
	public void testToTransport() {
		fail("Not yet implemented");
	}

	@Test
	public void testToTransportString() {
		fail("Not yet implemented");
	}

	@Test
	public void testFromTransport() {
		fail("Not yet implemented");
	}

	@Test
	public void testFromTransportString() {
		fail("Not yet implemented");
	}

	@Test
	public void testCharacters() {
		fail("Not yet implemented");
	}

	@Test
	public void testEndElement() {
		fail("Not yet implemented");
	}

	@Test
	public void testStartElement() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetHandler() {
		fail("Not yet implemented");
	}

}
