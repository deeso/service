/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.junit.Test;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.FileContainer;
import com.thecoverofnight.service.util.Base64;

public class FileContainerTest extends ExperimentalTests {

	@Test
	public void testSetGetPermissions() {
		FileContainer fc = new FileContainer();
		fc.setPermissions("rwx");
		assertEquals("Test set/get Permissions", "rwx", fc.getPermissions());
	}

	@Test
	public void testGetFilenameEasy() {
		String path = "tests/SetFile", answer = "SetFile", name = "Set/Get Filename";

		FileContainer fc = new FileContainer();
		fc.setFromFullPath(path);
		assertEquals(name, answer, fc.getFilename());

		fc = new FileContainer("SetFile", "tests");
		// fc.setFromFullPath(path);
		assertEquals(name, answer, fc.getFilename());
		assertEquals(name, path, fc.getJoinedOrigFilename());
		assertEquals(name, path, fc.getJoinedDestFilename());
		fc.setDestinationBaseDirectory("blah");
		assertFalse(name, fc.getJoinedDestFilename().equals(answer));
	}

	@Test
	public void testReadDataFromFile() throws IOException {
		String path = "tests/SetFile", name = "ReadDataFromFile";
		String filedata = readDataFromFile64(path);

		// check by setting the full path
		FileContainer fc = new FileContainer();
		fc.setFromFullPath(path);
		assertEquals(name, filedata, fc.getFiledata());

		// check by initializing the class
		fc = new FileContainer("SetFile", "tests");
		assertEquals(name, filedata, fc.getFiledata());

		// check by initializing the class
		fc = new FileContainer("SetFile", "tests");
		assertEquals(name, filedata, fc.getFiledata());

		// check by initializing the class
		fc = new FileContainer("SetFile", "tests", "blah");
		assertEquals(name, filedata, fc.getFiledata());

	}

	@Test
	public void testReadDataFromFileSpace() throws IOException {
		String path = "tests/additional directory/test.txt.txt", name = "ReadDataFromFileSpace";
		String filedata = readDataFromFile64(path);

		// check by setting the full path
		FileContainer fc = new FileContainer();
		fc.setFromFullPath(path);
		assertEquals(name, filedata, fc.getFiledata());

	}

	@Test
	public void testWriteDataToFile() throws IOException {
		String path = "tests/additional directory/", dest1 = "tests/destDir", dest2 = "tests/destDir/dest space dir", filename = "test.txt.txt", name = "ReadDataFromFileSpace";

		String filedata = readDataFromFile64(getJoinedPath(path, filename));

		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest1);
		fc.writeDataToFile();
		String writeFileData = readDataFromFile64(getJoinedPath(dest1, filename));
		assertEquals(name, getJoinedPath(dest1, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);

		// check by setting from the class init, with spaces
		fc = new FileContainer(filename, path, dest2);
		fc.writeDataToFile();
		writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);
	}

	@Test
	public void testCleanupFile() throws IOException {
		String path = "tests/additional directory/", dest2 = "tests/destDir/dest space dir", filename = "test.txt.txt", name = "ReadDataFromFileSpace", filedata = readDataFromFile64(getJoinedPath(
				path, filename));

		File file = null;
		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest2);
		fc.writeDataToFile();
		String writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);
		assertTrue(name, fc.cleanupFile());

		file = new File(dest2);
		assertTrue(name, file.exists());

		// check by setting from the class init, with spaces
		fc = new FileContainer(filename, path, dest2);
		fc.writeDataToFile();
		writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);

		fc.setDeleteDestDirectory(true);
		assertTrue(name, fc.cleanupFile());
		file = new File(dest2);
		assertTrue(name, !file.exists());

	}

	@Test
	public void testToTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			IOException {
		String xmlTestCmp = "tests/xmlFiles/fileContainerXML_test.txt.txt.xml", path = "tests/additional directory/", dest1 = "tests/destDir", filename = "test.txt.txt", name = "ToTransportString test", fileData = "";

		String filedata = readDataFromFile(xmlTestCmp);

		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest1);
		fileData = fc.toTransportString();
		assertEquals(name, filedata, fileData);
	}

	@Test
	public void testToTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		fail("Not yet implemented");
	}

	@Test
	public void testFromTransportNode() throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		String xmlTestCmp = "tests/xmlFiles/fileContainerXML_test.txt.txt.xml", path = "tests/additional directory/", dest1 = "tests/destDir", filename = "test.txt.txt", name = "ToTransportString test";

		FileContainer fc = new FileContainer(filename, path, dest1);
		Element e = fc.toTransport();
		FileContainer nc = new FileContainer();
		nc.fromTransport(e);
		assertEquals(name, fc.toTransportString(), nc.toTransportString());
		assertEquals(name, readDataFromFile64(xmlTestCmp),
				nc.toTransportString());

	}

	@Test
	public void testFromTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			SAXException, IOException {
		String xmlTestCmp = "tests/xmlFiles/fileContainerXML_test.txt.txt.xml", path = "tests/additional directory/", dest1 = "tests/destDir", filename = "test.txt.txt", name = "ToTransportString test";

		FileContainer fc = new FileContainer(filename, path, dest1);
		String e = fc.toTransportString();
		FileContainer nc = new FileContainer();
		nc.fromTransportString(e);
		assertEquals(name, fc.toTransportString(), nc.toTransportString());
		assertEquals(name, readDataFromFile64(xmlTestCmp),
				nc.toTransportString());

	}

	@Test
	public void testCleanupData() throws IOException {
		String path = "tests/additional directory/", dest2 = "tests/destDir/dest space dir", filename = "test.txt.txt", name = "ReadDataFromFileSpace", filedata = readDataFromFile64(getJoinedPath(
				path, filename));

		File file = null;
		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest2);
		fc.writeDataToFile();
		String writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);
		// assertTrue(name, fc.cleanupFile());
		fc.cleanupData();
		file = new File(dest2);
		assertTrue(name, file.exists());

		// check by setting from the class init, with spaces
		fc = new FileContainer(filename, path, dest2);
		fc.writeDataToFile();
		writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata, writeFileData);

		fc.setDeleteDestDirectory(true);
		// assertTrue(name, fc.cleanupFile());
		fc.cleanupData();
		file = new File(dest2);
		assertTrue(name, !file.exists());

	}

	@Test
	public void testReadWriteDataToString() throws IOException {
		String path = "tests/additional directory/", dest2 = "tests/destDir/dest space dir", filename = "test.txt.txt", name = "ReadDataFromFileSpace", newData = "har har har", filedata = readDataFromFile(getJoinedPath(
				path, filename)), filedata64 = readDataFromFile64(getJoinedPath(
				path, filename)), writeFileData = "";

		File file = null;
		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest2);
		assertEquals(name, filedata, fc.readDataToString());
		assertEquals(name, filedata64, fc.readDataToBase64());

		fc.writeDataToFile();
		writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));
		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, filedata64, writeFileData);
		// assertTrue(name, fc.cleanupFile());
		fc.setDeleteDestDirectory(true);
		fc.cleanupData();
		file = new File(dest2);
		assertTrue(name, !file.exists());

		fc.writeDataFromString(newData);
		assertEquals(name, Base64.encodeBytes(newData.getBytes()),
				fc.getFiledata());
		writeFileData = readDataFromFile(getJoinedPath(dest2, filename));

		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		assertEquals(name, newData, writeFileData);
		fc.cleanupData();
		file = new File(dest2);
		assertTrue(name, !file.exists());
	}

	@Test
	public void testAppendDataFromString() throws IOException {
		String path = "tests/additional directory/", dest2 = "tests/destDir/dest space dir", filename = "test.txt.txt", name = "ReadDataFromFileSpace", newData = "har har har", filedata = readDataFromFile(getJoinedPath(
				path, filename)), writeFileData = "";

		File file = null;
		// check by setting from the class init
		FileContainer fc = new FileContainer(filename, path, dest2);
		fc.appendDataFromString(newData);

		assertEquals(name, filedata + newData, fc.readDataToString());
		assertEquals(name, Base64.encodeBytes((filedata + newData).getBytes()),
				fc.readDataToBase64());

		assertEquals(name, getJoinedPath(dest2, filename),
				fc.getJoinedDestFilename());
		writeFileData = readDataFromFile64(getJoinedPath(dest2, filename));

		fc.setDeleteDestDirectory(true);
		fc.cleanupData();
		file = new File(dest2);
		assertTrue(name, !file.exists());

		assertEquals(name, Base64.encodeBytes((filedata + newData).getBytes()),
				writeFileData);
		// assertTrue(name, fc.cleanupFile());
	}
}
