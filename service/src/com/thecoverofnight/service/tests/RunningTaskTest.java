/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.JobManager;
import com.thecoverofnight.service.job.RunningTask;
import com.thecoverofnight.service.nodes.Worker;

public class RunningTaskTest extends ExperimentalTests {

	@Test
	public void testGetMyLastCommandState() {
		fail("Not yet implemented");
	}

	@Test
	public void testStateMachineTransition() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetProcess() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTaskId() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetTaskId() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsKeepRunning() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetKeepRunning() {
		fail("Not yet implemented");
	}

	@Test
	public void testRunningTask() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetExecutePrepCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsExecutePrepCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsExecutePreProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetExecutePreProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsExecutePostProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetExecutePostProcessCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsExecuteCleanupCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testSetExecuteCleanupCommands() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckStderrStream() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckStdoutStream() {
		fail("Not yet implemented");
	}

	@Test
	public void testPerformCommand() {
		fail("Not yet implemented");
	}

	@Test
	public void testTokenize() {
		fail("Not yet implemented");
	}

	@Test
	public void testYield() {
		fail("Not yet implemented");
	}

	@Test
	public void testPoisonPill() {
		fail("Not yet implemented");
	}

	@Test
	public void testStart() {
		fail("Not yet implemented");
	}

	@Test
	public void testBasicCommand() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		JobManager jm = new JobManager(new Worker());
		Command cmd = new Command();
		String cwd = "tests/cmdWorkingDir", xmlTestCmp = "tests/xmlFiles/command_basic_cmd.xml", outputFile = "testfile.txt", outString = "", name = "testBasicCommand";

		File outDir = new File(cwd);
		outDir.mkdirs();
		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch " + outputFile);
		cmd.setCommandId("Adam's First Basic Command");
		outString = cmd.toTransportString();
		String testOutput = readDataFromFile(xmlTestCmp);
		assertEquals("Basic Command Creation", testOutput.trim(),
				outString.trim());

		jm.addNewCommand(cmd);
		HashMap<String, RunningTask> c = jm.getRunningCommands();
		RunningTask t = null;
		for (String tid : c.keySet()) {
			t = c.get(tid);
		}

		if (t == null) {
			fail("Unable to add the running task to the running tasks");
		}
		t.start();
		Thread x = t.getMyMainThread();

		int g = 0;
		while (x.isAlive()) {
			Thread.sleep(1000);
			if (++g == 1000) {
				break;
			}
		}

		File file = new File(cwd + getFSSeparator() + outputFile);
		assertTrue(name, file.exists());
		FileUtils.deleteDirectory(new File(cwd));

		// fail("Not yet implemented");
	}

	@Test
	public void testBasicCommandWithPrepCleanup() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		JobManager jm = new JobManager(new Worker());
		Command cmd = new Command(), prep = new Command(), cleanup = new Command();

		String cwd = "tests/cmdWorkingDir", xmlTestCmp = "tests/xmlFiles/command_basic_cmd.xml", outputFile = "testfile.txt", outString = "", name = "testBasicCommandWithPrepCleanup";

		File outDir = new File(cwd);
		outDir.mkdirs();

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch " + outputFile);
		cmd.setCommandId("Adam's First Basic Command");

		prep.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		prep.setCommandName("Prep command to Create Empty File");
		prep.setCommandLine("mkdir -p " + cwd);
		prep.setCommandId("Adam's First Prep Command");

		cleanup.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cleanup.setCommandName("Prep command to Create Empty File");
		// becareful!!!
		cleanup.setCommandLine("rm -rf " + cwd);
		cleanup.setCommandId("Adam's First Cleanup Command");

		outString = cmd.toTransportString();
		cmd.addPrepCommand(prep);
		cmd.addCleanUpCommand(cleanup);

		String testOutput = readDataFromFile(xmlTestCmp);
		assertEquals("Basic Command Creation", testOutput.trim(),
				outString.trim());

		jm.addNewCommand(cmd);
		HashMap<String, RunningTask> c = jm.getRunningCommands();
		RunningTask t = null;
		for (String tid : c.keySet()) {
			t = c.get(tid);
		}

		if (t == null) {
			fail("Unable to add the running task to the running tasks");
		}

		t.setExecutePrepCommands(true);
		t.setExecuteCleanupCommands(true);

		t.start();
		Thread x = t.getMyMainThread();

		x.join();

		File file = new File(cwd + getFSSeparator() + outputFile);
		assertTrue(name, !file.exists());

	}

	@Test
	public void testBasicCommandWithPrep() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		JobManager jm = new JobManager(new Worker());
		Command cmd = new Command(), prep = new Command();
		String cwd = "tests/cmdWorkingDir", xmlTestCmp = "tests/xmlFiles/command_basic_cmd.xml", outputFile = "testfile.txt", outString = "", name = "testBasicCommand";

		File outDir = new File(cwd);
		outDir.mkdirs();

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch " + outputFile);
		cmd.setCommandId("Adam's First Basic Command");

		prep.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		prep.setCommandName("Prep command to Create Empty File");
		prep.setCommandLine("mkdir -p " + cwd);
		prep.setCommandId("Adam's First Prep Command");

		cmd.addPrepCommand(prep);
		outString = cmd.toTransportString();

		String testOutput = readDataFromFile(xmlTestCmp);
		System.out.println(testOutput);
		assertEquals("Basic Command Creation", testOutput.trim(),
				outString.trim());

		// jm.addNewCommand(cmd);
		HashMap<String, RunningTask> c = jm.getRunningCommands();
		RunningTask t = null;
		for (String tid : c.keySet()) {
			t = c.get(tid);
		}

		if (t == null) {
			fail("Unable to add the running task to the running tasks");
		}

		t.setExecutePrepCommands(true);
		t.start();
		Thread x = t.getMyMainThread();

		x.join();

		File file = new File(cwd + getFSSeparator() + outputFile);
		assertTrue(name, file.exists());
		FileUtils.deleteDirectory(new File(cwd));

		// fail("Not yet implemented");
	}

	// TODO perform a command on a piece of data or a file, and post process the
	// results
	@Test
	public void testBasicCommandWithPrepCleanupData() throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException,
			InterruptedException {
		JobManager jm = new JobManager(new Worker());
		Command cmd = new Command(), prep = new Command();
		String cwd = "tests/cmdWorkingDir/dataTest", xmlTestCmp = "tests/xmlFiles/command_basic_cmd.xml", outputFile = "testfile.txt", outString = "", name = "testBasicCommand";

		File outDir = new File(cwd);
		outDir.mkdirs();

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch " + outputFile);
		cmd.setCommandId("Adam's First Basic Command");

		prep.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		prep.setCommandName("Prep command to Create Empty File");
		prep.setCommandLine("mkdir -p " + cwd);
		prep.setCommandId("Adam's First Prep Command");

		outString = cmd.toTransportString();
		cmd.addPrepCommand(prep);

		String testOutput = readDataFromFile(xmlTestCmp);
		assertEquals("Basic Command Creation", testOutput.trim(),
				outString.trim());

		jm.addNewCommand(cmd);
		HashMap<String, RunningTask> c = jm.getRunningCommands();
		RunningTask t = null;
		for (String tid : c.keySet()) {
			t = c.get(tid);
		}

		if (t == null) {
			fail("Unable to add the running task to the running tasks");
		}

		t.setExecutePrepCommands(true);
		t.start();
		Thread x = t.getMyMainThread();

		x.join();

		File file = new File(cwd + getFSSeparator() + outputFile);
		assertTrue(name, file.exists());
		FileUtils.deleteDirectory(new File(cwd));

		// fail("Not yet implemented");
	}

}
