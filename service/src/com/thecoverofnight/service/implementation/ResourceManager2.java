/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.implementation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.PriorityBlockingQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.analysismodule.AnalysisModule;
import com.thecoverofnight.service.comms.BasicMessage;
import com.thecoverofnight.service.comms.NodeClientSocket;
import com.thecoverofnight.service.comms.PlainTextNodeSocket;
import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.CommandResults;
import com.thecoverofnight.service.job.JobManager;
import com.thecoverofnight.service.job.ResourceAssignmentPollTask;
import com.thecoverofnight.service.job.RunningTask;
import com.thecoverofnight.service.nodes.BaseNode;
import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.NodeXMLReaderWriter;

public class ResourceManager2 extends BaseNode {
	protected static Logger LOGGER = Logger.getLogger(ResourceManager2.class);
	HashMap<String, AnalysisModule> myAnalysisModules = new HashMap<String, AnalysisModule>();
	JobManager jb = null;
	CommandHandler cmdHandler = null;
	ResourceAssignmentPollTask rapt = null;
	private static ResourceManager2 RESOURCE_MANAGER;

	final Object LockCmdHandlerMaps = new Object();
	HashMap<Integer, Command> msgIdToCommandHandlerCommandMap = new HashMap<Integer, Command>();
	HashMap<Command, Integer> commandHandlerCommandToMsgIdMap = new HashMap<Command, Integer>();

	HashMap<Integer, String> msgIdToCmdHandlerPeerNodeMap = new HashMap<Integer, String>();
	HashMap<String, Integer> cmdHandlerPeerNodeMapToMsgId = new HashMap<String, Integer>();

	
	
	final Object lockCmdHandlerResultMaps = new Object();
	HashMap<Integer, CommandResults> msgIdToCmdResultsMap = new HashMap<Integer, CommandResults>();
	HashMap<CommandResults, Integer> cmdResultsToMsgIdMap = new HashMap<CommandResults, Integer>();

	// FIXME these mappings need to be dropped into a class that is responsible
	// for mapping commands and available hosts, and this class will also
	// be responsible for redirecting results back to the original destination
	final Object lockProcessorMsgIdCommandMappings = new Object();
	HashMap<String, Command> processorToCommandMap = new HashMap<String, Command>();
	HashMap<Command, String> commandToProcessorMap = new HashMap<Command, String>();

	HashMap<String, Command> commandIdToCommandMap = new HashMap<String, Command>();
	HashMap<Command, String> commandToCommandIdMap = new HashMap<Command, String>();

	HashMap<Integer, String> msgIdToCommandIdMap = new HashMap<Integer, String>();
	HashMap<String, Integer> commandIdToMsgIdMap = new HashMap<String, Integer>();

	
	
	final Object lockResourceElements = new Object();
	PriorityBlockingQueue<Command> pendingCmdQueue = new PriorityBlockingQueue<Command>();
	PriorityBlockingQueue<String> availableHostQueue = new PriorityBlockingQueue<String>();

	public boolean checkAvailableJobsAndWorkers() {
		return pendingCmdQueue.size() > 0 && availableHostQueue.size() > 0;
	}

	private void clearJobMappings(int msgId, String cmdId, String peerNode) {
		Command cmd = null;

		if (commandIdToCommandMap.containsKey(cmdId)) {
			cmd = commandIdToCommandMap.get(cmdId);
			if (cmd != null)
				unmapProcessorMsgIdCommandMappings(peerNode, msgId, cmd);
		}
		

	}

	public CommandResults checkForResults(int msgId) {
		CommandResults cr = null;
		synchronized (lockCmdHandlerResultMaps) {
			if (msgIdToCmdResultsMap.containsKey(msgId)) {
				cr = msgIdToCmdResultsMap.get(msgId);
			}
			// if cr is not null then the CommandResults are guaranteed to be in
			// the hashmap
			if (cr != null) {
				// clear out the mappings
				msgIdToCmdResultsMap.remove(msgId);
				cmdResultsToMsgIdMap.remove(cr);
			}
		}
		return cr;
	}

	public void handleCommandResults(String peerNodeName, CommandResults cr)
			throws Exception {
		int msgId = -1;
		String cmdId = cr.getCommandId();
		Command cmd = null;
		synchronized (lockProcessorMsgIdCommandMappings) {
			if (!commandIdToMsgIdMap.containsKey(cmdId)) {
				// FIXME should we throw an exception
				LOGGER.debug("The following Command ID could not be found in outstanding commands map: "
						+ cmdId);
				return;
			}
			msgId = commandIdToMsgIdMap.get(cmdId);
			if (!processorToCommandMap.containsKey(peerNodeName)) {
				// here I need to clean up everything
				// clearJobMappingsUnSynchronized(msgId, peerNodeName, cmdId);
				unmapProcessorMsgIdCommandMappingsUnSynchronized(msgId,
						peerNodeName, cmdId);
				// FIXME we shoudl definitely throw an exception here
				LOGGER.debug("The following Peer Node could not be found in outstanding commands map: "
						+ peerNodeName);
				throw new Exception(
						"The peerNode did not appear in the outstanding commands map (e.g. processorToCommandMap).");
			}
			// FIXME I dont think there is a possibility that a Command may be
			// in the commandToCommandIdMap
			// and not in the processorToCommandMap, so I will verify this in
			// the future.
			cmd = processorToCommandMap.get(peerNodeName);
		}

		String cmdHandlerHost = null;
		int cmdHandlerMsgId = msgId;
		Command cmdHandlerCommand = cmd;

		synchronized (LockCmdHandlerMaps) {
			if (msgIdToCmdHandlerPeerNodeMap.containsKey(cmdHandlerMsgId)) {
				cmdHandlerHost = msgIdToCmdHandlerPeerNodeMap
						.get(cmdHandlerMsgId);
			}
			if (msgIdToCommandHandlerCommandMap.containsKey(cmdHandlerMsgId)) {
				cmdHandlerCommand = msgIdToCommandHandlerCommandMap
						.get(cmdHandlerMsgId);
			}

		}

		if (cmdHandlerCommand != cmd) {
			LOGGER.debug("It appears that the commandHandlerCommand and the Run CMD are not the same.");
		}

		if (cmdHandler != null) {
			// clear mappings
			unmapCmdHandlerMaps(cmdHandlerHost, cmdHandlerMsgId,
					cmdHandlerCommand);
			unmapProcessorMsgIdCommandMappings(peerNodeName, cmdHandlerMsgId,
					cmdHandlerCommand);
			if (!cmdHandler.resultsRecieved(cmdHandlerHost, cmdHandlerMsgId, cr)) {

				// TODO polling thread to actively push results
				synchronized (lockCmdHandlerResultMaps) {
					msgIdToCmdResultsMap.put(cmdHandlerMsgId, cr);
					cmdResultsToMsgIdMap.put(cr, cmdHandlerMsgId);
				}
			}
		}

		clearJobMappings(msgId, cmdId, peerNodeName);

		// if everything works out here:
		requeueAvailableNode(peerNodeName);

	}

	public void requeueAvailableNode(String peerNode) {
		synchronized (lockResourceElements) {
			availableHostQueue.add(peerNode);
		}
	}

	public void jobAssignementFailed(String peerNode, Command pendingCommand) {
		synchronized (lockResourceElements) {
			availableHostQueue.add(peerNode);
			pendingCmdQueue.add(pendingCommand);
		}
	}

	private void mapProcessorMsgIdCommandMappings(String peerNode, int msgId,
			Command cmd) {
		synchronized (lockProcessorMsgIdCommandMappings) {
			processorToCommandMap.put(peerNode, cmd);
			commandToProcessorMap.put(cmd, peerNode);
			msgIdToCommandIdMap.put(msgId, cmd.getCommandId());
			commandIdToMsgIdMap.put(cmd.getCommandId(), msgId);
			commandIdToCommandMap.put(cmd.getCommandId(), cmd);
			commandToCommandIdMap.put(cmd, cmd.getCommandId());
		}
	}
	
	
	private Command getCommandMappedToCommandHandler(int msgId, String peerNodeName){
		synchronized (LockCmdHandlerMaps) {
			if (cmdHandlerPeerNodeMapToMsgId.containsKey(peerNodeName) &&
					cmdHandlerPeerNodeMapToMsgId.get(peerNodeName) == msgId &&
				msgIdToCommandHandlerCommandMap.containsKey(msgId))
				return msgIdToCommandHandlerCommandMap.get(msgId);
		}
		return null;
	}
	
	private String getProcessorMappedToCommand(Command cmd){
		
		synchronized (lockProcessorMsgIdCommandMappings) {
			if(commandToProcessorMap.containsKey(cmd))
				return commandToProcessorMap.get(cmd);
		}
		return null;
	}

	private void unmapProcessorMsgIdCommandMappings(String peerNode, int msgId,
			Command cmd) {
		synchronized (lockProcessorMsgIdCommandMappings) {
			if (processorToCommandMap.containsKey(peerNode))
				processorToCommandMap.remove(peerNode);

			if (commandToProcessorMap.containsKey(cmd))
				commandToProcessorMap.remove(cmd);

			if (msgIdToCommandIdMap.containsKey(msgId))
				msgIdToCommandIdMap.remove(msgId);

			if (commandIdToMsgIdMap.containsKey(msgId))
				commandIdToMsgIdMap.remove(cmd.getCommandId());

			if (commandIdToCommandMap.containsKey(cmd.getCommandId()))
				commandIdToCommandMap.remove(cmd.getCommandId());

			if (commandToCommandIdMap.containsKey(cmd))
				commandToCommandIdMap.remove(cmd);
		}
	}

	private void clearJobMappingsUnSynchronized(int msgId, String cmdId,
			String peerNode) {
		Command cmd = null;
		// handle mapping for command Id to command
		if (commandIdToCommandMap.containsKey(cmdId)) {
			cmd = commandIdToCommandMap.get(cmdId);
			commandIdToCommandMap.remove(cmdId);
		}
		if (commandToCommandIdMap.containsKey(cmd)) {
			commandToCommandIdMap.remove(cmd);
		}

		// handle mapping for command Id to msg id
		if (commandIdToMsgIdMap.containsKey(cmdId))
			commandIdToMsgIdMap.remove(cmdId);
		if (msgIdToCommandIdMap.containsKey(msgId))
			msgIdToCommandIdMap.remove(msgId);

		// handle mapping for command to processor
		if (processorToCommandMap.containsKey(peerNode))
			processorToCommandMap.remove(peerNode);
		if (commandToProcessorMap.containsKey(cmd))
			commandToProcessorMap.remove(cmd);

	}

	private void unmapProcessorMsgIdCommandMappingsUnSynchronized(int msgId,
			String peerNode, String cmdId) {
		Command cmd = null;
		if (commandIdToCommandMap.containsKey(cmdId))
			cmd = commandIdToCommandMap.get(cmdId);

		if (processorToCommandMap.containsKey(peerNode))
			processorToCommandMap.remove(peerNode);

		if (commandToProcessorMap.containsKey(cmd))
			commandToProcessorMap.remove(cmd);

		if (msgIdToCommandIdMap.containsKey(msgId))
			msgIdToCommandIdMap.remove(msgId);

		if (commandIdToMsgIdMap.containsKey(cmdId))
			commandIdToMsgIdMap.remove(cmdId);

		if (commandIdToCommandMap.containsKey(cmdId))
			commandIdToCommandMap.remove(cmdId);

		if (commandToCommandIdMap.containsKey(cmd))
			commandToCommandIdMap.remove(cmd);

	}

	public void assignWork() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Command cmd = null;
				String peerNode = null;
				BasicMessage bm = null;
				synchronized (lockResourceElements) {
					if (checkAvailableJobsAndWorkers()) {
						try {
							peerNode = availableHostQueue.take();
							cmd = pendingCmdQueue.take();
						} catch (InterruptedException e) {
							LOGGER.debug("Exception while assigning work");
							LOGGER.debug(ExceptionToString.toString(e));
						}
					}
				}
				if (cmd == null || peerNode == null) {
					LOGGER.debug("Polled the priority queues for workers and work, but nothing was available.");
					rapt.doneAssigning();
					return;
				}

				try {
					bm = convertCommandToBasicMessage(cmd);
					if (bm == null)
						throw new NullPointerException(
								"NullPointer came from convertCommandToBasicMessage.");

					sendBasicMessage(peerNode, bm);

					// wait until here before we map, b/c if we get an
					// exception, we
					// need to reset everything
					mapProcessorMsgIdCommandMappings(peerNode, bm.getMsgId(),
							cmd);
					rapt.doneAssigning();
					// ftw sent a command.
				} catch (DOMException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				} catch (ParserConfigurationException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				} catch (TransformerFactoryConfigurationError e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e.getException()));
					jobAssignementFailed(peerNode, cmd);
				} catch (TransformerException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				} catch (SAXException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				} catch (IOException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				} catch (NullPointerException e) {
					LOGGER.debug("An exception happened while we were trying to convert the Command to a BasicMessage and send it off.");
					LOGGER.debug(ExceptionToString.toString(e));
					jobAssignementFailed(peerNode, cmd);
				}
			}
		}).start();

	}

	private void mapCmdHandlerMaps(String peerNodeName, int msgId, Command cmd) {
		synchronized (LockCmdHandlerMaps) {
			msgIdToCommandHandlerCommandMap.put(msgId, cmd);
			commandHandlerCommandToMsgIdMap.put(cmd, msgId);

			msgIdToCmdHandlerPeerNodeMap.put(msgId, peerNodeName);
			cmdHandlerPeerNodeMapToMsgId.put(peerNodeName, msgId);
		}
	}
	
	protected boolean purgeCommand(int msgId, String peerNodeName){
		
		Command cmd = getCommandMappedToCommandHandler(msgId, peerNodeName);
		String workerNode = null;
		if (cmd != null){
			workerNode = getProcessorMappedToCommand(cmd);
			unmapCmdHandlerMaps(peerNodeName, msgId, cmd);
			unmapProcessorMsgIdCommandMappings(workerNode, msgId, cmd);
			return true;
		}
		
		return false;
	}
	
	
	private void unmapCmdHandlerMaps(String peerNodeName, int msgId, Command cmd) {
		synchronized (LockCmdHandlerMaps) {

			if (msgIdToCommandHandlerCommandMap.containsKey(msgId))
				msgIdToCommandHandlerCommandMap.remove(msgId);
			if (commandHandlerCommandToMsgIdMap.containsKey(cmd))
				commandHandlerCommandToMsgIdMap.remove(cmd);
			if (msgIdToCmdHandlerPeerNodeMap.containsKey(msgId))
				msgIdToCmdHandlerPeerNodeMap.remove(msgId);
			if (cmdHandlerPeerNodeMapToMsgId.containsKey(peerNodeName))
				cmdHandlerPeerNodeMapToMsgId.remove(peerNodeName);
		}
	}

	public boolean registerCommand(String peerNodeName, int msgId, Command cmd) {
		boolean result = false;
		if (msgIdToCmdHandlerPeerNodeMap.containsKey(msgId)
				|| msgIdToCommandHandlerCommandMap.containsKey(msgId)) {
			LOGGER.debug("MsgId currently has a pending command: " + msgId);
			return result;
		}

		// this.jb.addNewCommand(cmd);
		// The above statement will add the command to the job manager and try
		// to execute here,
		// and i want the command sent to a remote host
		synchronized (pendingCmdQueue) {
			pendingCmdQueue.add(cmd);
		}
		// FIXME there is (was) a race condition here.
		// 1 day later: damn me for not annotating why :(

		mapCmdHandlerMaps(peerNodeName, msgId, cmd);
		result = true;
		return result;

	}

	public boolean registerCommand(String peerNodeName, int msgId, String cmdXml) {
		return registerCommand(peerNodeName, msgId, new Command(cmdXml));
	}

	private void startAllServices() {
		LOGGER.debug("Starting the server. Now we can accept node connections or server connections.");
		LOGGER.debug("The IP Address of the Processor Service is: "
				+ myServer.getHost() + " and the port: " + myServer.getPort());
		this.startServer();
		try {
			LOGGER.debug("The IP Address of the Client/Command Handler Service is: "
					+ cmdHandler.getHost()
					+ " and the port: "
					+ cmdHandler.getPort());
			this.startCommandHandler();
		} catch (Exception e) {
			LOGGER.debug("Failed to start the command handler.");
			LOGGER.debug(ExceptionToString.toString(e));
		}

	}

	protected void parseCommandLineArgs(String[] args) {
		CommandLineParser parser = new PosixParser();
		String host = "0.0.0.0";
		int port = 9000, timeout = 2000, backlog = 20, cmdHandler_port = 8989;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.print("Error parsing arguments");
			System.err.print(ExceptionToString.toString(e));
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("manager", options);
			System.exit(1);
		}

		if (cmd.hasOption(CONFIGURATION_HELP)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("manager", options);
			System.exit(1);
		}

		String logger_conf = "example/log4j.xml";
		if (cmd.hasOption(CONFIGURATION_LOGGER)) {
			logger_conf = cmd.getOptionValue(CONFIGURATION_LOGGER);
			try {
				DOMConfigurator.configure(logger_conf);
			} catch (Exception e) {
				LOGGER.debug("Failed to set up the logger, did you provide the appropriate file location?.");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Exiting.");
				System.exit(-1);
			}

		}

		if (cmd.hasOption(CONFIGURATION_FILE)) {
			String filename = cmd.getOptionValue(CONFIGURATION_FILE);
			try {
				NodeXMLReaderWriter.parseFromFile(this, filename);
				this.startAllServices();
				LOGGER.debug("Connecting to all peers: "
						+ this.connectToAllPeers());
				return;
			} catch (ParserConfigurationException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			} catch (SAXException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			} catch (IOException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			}

		}

		if (cmd.hasOption(CONFIGURATION_PORT)) {
			try {
				int x = Integer
						.parseInt(cmd.getOptionValue(CONFIGURATION_PORT));
				port = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + port);
			}
		}

		if (cmd.hasOption(CONFIGURATION_SERVER_PORT)) {
			try {
				int x = Integer.parseInt(cmd
						.getOptionValue(CONFIGURATION_SERVER_PORT));
				cmdHandler_port = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + port);
			}
		}

		if (cmd.hasOption(CONFIGURATION_TIMEOUT)) {
			try {
				int x = Integer.parseInt(cmd
						.getOptionValue(CONFIGURATION_TIMEOUT));
				timeout = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + timeout);
			}
		}

		if (cmd.hasOption(CONFIGURATION_BACKLOG)) {
			try {
				int x = Integer.parseInt(cmd
						.getOptionValue(CONFIGURATION_BACKLOG));
				backlog = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + timeout);
			}
		}

		if (cmd.hasOption(CONFIGURATION_HOST)) {
			host = cmd.getOptionValue(CONFIGURATION_HOST);
		}

		window = (long) timeout;
		this.initServerParameters(host, port, timeout, backlog);
		this.setUpCommandHandler(host, cmdHandler_port, timeout);
		startAllServices();

	}

	private ResourceManager2(String[] args) {
		jb = new JobManager(this);
		parseCommandLineArgs(args);
		rapt = new ResourceAssignmentPollTask(this, myServer.getTimeout() / 2);
		rapt.start();
	}

	public void setUpCommandHandler(String host, int port, int timeout) {
		cmdHandler = new CommandHandler(this, host, port, timeout);
	}

	public void startCommandHandler() throws Exception {
		if (cmdHandler != null)
			cmdHandler.myServer.start();
		else
			throw new Exception("CommandHandler has not been set up");
	}

	public void stopCommandHandler() throws Exception {
		if (cmdHandler != null)
			cmdHandler.myServer.stop();
		else
			throw new Exception("CommandHandler has not been set up");
	}

	public void sendNodeData(String host, String data) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			NodeClientSocket ncs = myPeerNodes.get(host);
			ncs.send(data);
		}
	}

	public void sendNodeEcho(String host, String echoString) {
		if (myPeerNodes.containsKey(host) && myPeerNodes.get(host) != null) {
			NodeClientSocket ncs = myPeerNodes.get(host);
			ncs.send(echoString);
		}
	}

	@Override
	public void connectionOpened(Socket connection) {
		LOGGER.debug("Client connection received, adding the peer.\n");
		String peerNode = connection.getInetAddress().getHostAddress() + ":"
				+ connection.getPort();

		if (myPeerNodes.containsKey(peerNode)
				&& myPeerNodes.get(peerNode) != null) {
			LOGGER.debug("Appears that a stale connection may exist from peernode"
					+ peerNode);
			myPeerNodes.get(peerNode).cleanupDisconnectedSocket();
			LOGGER.debug("Cleaning up the existing node");
		}

		try {
			addPeerNode(connection);
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
	}

	@Override
	public void connectionClosed(Socket connection) {
		LOGGER.debug("Client Connection closed.\n");

	}

	private String getXmlDocName(String data) throws SAXException, IOException,
			ParserConfigurationException {
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		Element node = docBuilder.parse(
				new ByteArrayInputStream(data.getBytes())).getDocumentElement();
		return node.getNodeName();

	}

	private boolean isCommandTransport(String data) throws SAXException,
			IOException, ParserConfigurationException {
		String name = getXmlDocName(data);
		return name != null && name.equalsIgnoreCase("command");
	}

	private boolean isCommandResultsTransport(String data) throws SAXException,
			IOException, ParserConfigurationException {
		String name = getXmlDocName(data);
		return name != null && name.equalsIgnoreCase("commandresults");
	}

	@Override
	public void dataSent() {
		LOGGER.debug("Data sent to the other end.\n");
	}

	BasicMessage convertCommandToBasicMessage(Command cmd) throws DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException, SAXException, IOException {
		Integer msgId = null;
		synchronized (LockCmdHandlerMaps) {
			if (commandHandlerCommandToMsgIdMap.containsKey(cmd)) {
				msgId = commandHandlerCommandToMsgIdMap.get(cmd);
			}
		}

		if (msgId == null) {
			return null;
		}
		BasicMessage bm = new BasicMessage();
		bm.setData(cmd.toTransportString());
		bm.setMsgId(msgId);
		return bm;
	}

	String getBasicCommand() {
		Command cmd = new Command();
		String cwd = "";

		cmd.setCommandCWD(cwd);
		cmd.addEnvironmentVar("PATH", "/sbin/:/bin/:/usr/bin/:/usr/sbin/");
		cmd.setCommandName("Create Empty File");
		cmd.setCommandLine("touch testfile.txt");
		cmd.setCommandId("Adam's First Basic Command");

		try {
			return cmd.toTransportString();
		} catch (DOMException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (ParserConfigurationException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (TransformerFactoryConfigurationError e) {
			LOGGER.debug(ExceptionToString.toString(e.getException()));
		} catch (TransformerException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (SAXException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		// check for legal argument list
		ResourceManager2 x = new ResourceManager2(args);

		final Dummy neverEndingStory = new Dummy();
		neverEndingStory.setDone(true);
		// Timer t = new Timer();
		// TimerTask tt = new TimerTask() {
		// @Override
		// public void run() {
		// neverEndingStory.setDone(false);
		// }
		// };

		// t.schedule(tt, 60 * 10000);
		try {
			// sleep for 2 seconds to let everything get fired up
			Thread.sleep(60 * 1000);
		} catch (InterruptedException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
		while (x.myServer.isServerRunning() && x.cmdHandler.isServerRunning()) {
			try {
				Thread.sleep(60 * 1000);
			} catch (InterruptedException e) {
				LOGGER.debug(ExceptionToString.toString(e));
			}
		}
		// t.cancel();
		// t = null;

		LOGGER.debug("Stopping the service.");
		x.shutdown();

	}

	public void addNewCommand(Command cmd) throws InterruptedException {
		jb.addNewCommand(cmd);
	}

	@Override
	public void commandCompleted(RunningTask rt) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleMessage(String peerNodeName, BasicMessage message) {
		// FIXME copied from node processor, modify to perfomr the correct
		// operations.
		try {
			String data = message.getData();
			LOGGER.debug("Recieved the following message:\n" + data);
			LOGGER.debug("Attempting to parse the command and add the message to the JobManager");

			if (isCommandTransport(data)) {
				Command cmd = new Command(data);
				// myJobManager.addNewCommand(cmd);
				// FIXME not sure how to unmap the command
				// cmdToHostMapping.put(cmd.getCommandId(), peerNodeName);
			} else if (isCommandResultsTransport(data)) {
				CommandResults cr = new CommandResults();
				cr.fromTransportString(data);
				handleCommandResults(peerNodeName, cr);
			}

		} catch (IOException e) {
			LOGGER.debug("Failed tso decode message string");
			LOGGER.debug(e.getMessage());
		} catch (ParserConfigurationException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (SAXException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (NullPointerException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (TransformerFactoryConfigurationError e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e.getException()));
		} catch (TransformerException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (Exception e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		}
		// remove message
		pendingMessages.remove(peerNodeName);
	}

	@Override
	public void connectionClosed(String host, int port) {
		String peerNodeString = host + ":" + port;

		synchronized (myPeerNodes) {
			if (myPeerNodes.containsKey(peerNodeString)) {
				myPeerNodes.remove(peerNodeString);
			}
		}
		synchronized (pendingMessages) {
			if (pendingMessages.containsKey(peerNodeString)) {
				pendingMessages.remove(peerNodeString);
			}
		}

	}

	private boolean shuttingDown = false;

	public boolean isShuttingDown() {
		return shuttingDown;
	}

	@Override
	public void shutdown() {
		if (shuttingDown)
			return;
		shuttingDown = true;
		rapt.cancel();
		myServer.shutdown();
		cmdHandler.shutdown();
		synchronized (myPeerNodes) {
			for (String peerNode : myPeerNodes.keySet()) {
				NodeClientSocket pt = null;
				if (myPeerNodes.containsKey(peerNode)) {
					pt = myPeerNodes.get(peerNode);
					myPeerNodes.remove(peerNode);
				}
				if (pt != null) {
					pt.shutdown();

				}
			}

		}

	}

	@Override
	public boolean addPeerNode(String host, int port, int timeout) {

		if (myPeerNodes.containsKey(host + ":" + port)) {
			return true;
		}

		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, host, port,
				timeout);
		myPeerNodes.put(host + ":" + pt.getPort(), pt);

		requeueAvailableNode(host + ":" + pt.getPort());
		pt.startPolling();

		return true;
	}

	@Override
	public boolean addPeerNode(Socket connection) throws IOException {
		String peerNode = connection.getInetAddress().getHostAddress() + ":"
				+ connection.getPort();

		if (myPeerNodes.containsKey(peerNode)) {
			return true;
		}
		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, connection,
				myServer.getTimeout());
		pt.setPollTime(myServer.getTimeout());
		pt.setTimeout(myServer.getTimeout());
		pt.startPolling();
		myPeerNodes.put(pt.getHost() + ":" + pt.getPort(), pt);
		requeueAvailableNode(peerNode);

		return true;
	}

}