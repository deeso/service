package com.thecoverofnight.service.implementation;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.comms.BasicMessage;
import com.thecoverofnight.service.comms.IncompleteBasicMessage;
import com.thecoverofnight.service.comms.NodeClientSocket;
import com.thecoverofnight.service.comms.NodeEvents;
import com.thecoverofnight.service.comms.NodeServerSocket;
import com.thecoverofnight.service.comms.PlainTextNodeSocket;
import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.CommandResults;
import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.ExceptionToString;

public class CommandHandler implements NodeEvents {

	public static final String COMMAND_CONNECT_PEER_NODE = "connect";
	public static final String COMMAND_DISCONNECT_PEER_NODE = "disconnect";
	public static final String COMMAND_EXECUTE_COMMAND = "execute";
	public static final String COMMAND_POLL_STATUS = "poll";
	public static final String COMMAND_RESULTS_AVAILABLE = "available";
	public static final String COMMAND_RESULTS_PURGE = "purge";
	public static final String COMMAND_RESULTS_PURGE_ALL = "purgeall";

	Socket clientSocket = null;
	static Logger LOGGER = Logger.getLogger(CommandHandler.class);
	protected HashMap<String, IncompleteBasicMessage> pendingMessages = new HashMap<String, IncompleteBasicMessage>();
	protected HashMap<String, PlainTextNodeSocket> myPeerNodes = new HashMap<String, PlainTextNodeSocket>();
	protected HashMap<Integer, String> pendingCommands = new HashMap<Integer, String>();

	ResourceManager2 myParent = null;
	NodeServerSocket myServer;
	long window;
	
	
	boolean purgeCommand(int msgId, String peerNodeName, String pMsgid){
		boolean result = false;
		int purgeMsgId = -1;
		try{
			purgeMsgId = Integer.parseInt(pMsgid);
		}catch(NumberFormatException e){
			LOGGER.debug("Failed to parse the msg ID that need to be parsed");
			LOGGER.debug(ExceptionToString.toString(e));
			result = false;
		}
		if (result)
			return result && myParent.purgeCommand(purgeMsgId, peerNodeName);
		return false;
	}
	
	boolean purgeCommand(int msgId, String peerNodeName, int purgeMsgId){
		boolean result = false;
		if (result)
			return result && myParent.purgeCommand(purgeMsgId, peerNodeName);
		return false;
	}
	
	boolean executePurgeCommand(int msgId, String peerNodeName, String []args){
		// TODO implement this
		boolean result = false;
		String message = "purged,";
		int purgeMsgId = -1;
		
		if (purgeCommand(purgeMsgId, peerNodeName, args[0])){
			message += args[0];
			result = true;
		}else{
			message += "None";
		}
		
		pendingCommands.remove(msgId);
		myPeerNodes.get(peerNodeName).send(
				getBasicMessageResult(message, msgId));
		return result;
	}
	
	
	
	boolean executePurgeAllCommand(int msgId, String peerNodeName, String []args){
		// TODO implement this
		boolean result = true;
		String message = "";
		
		try{
			int purgeMsgId = Integer.parseInt(args[0]);
		}catch(NumberFormatException e){
			LOGGER.debug("Failed to parse the msg ID that need to be parsed");
			LOGGER.debug(ExceptionToString.toString(e));
			result = false;
			message = "purged,NONE";
		}
		
		ArrayList<String> purgedMsgIds = new ArrayList<String>();
		for(int pendingId: pendingCommands.keySet()){
			if (pendingId == msgId)
				continue;
			
			if (purgeCommand(msgId, peerNodeName, pendingId))
				purgedMsgIds.add(Integer.toString(pendingId));
		}
		if (purgedMsgIds.size() > 0){
			result = true;
			message = "purged,";
			for(int i = 0; i < purgedMsgIds.size(); i++){
				message += purgedMsgIds.get(i);
				if (i < purgedMsgIds.size()-1){
					message+=",";
				}
			}
		}
		myPeerNodes.get(peerNodeName).send(
				getBasicMessageResult(message, msgId));
		pendingCommands.remove(msgId);
		return result;
	}
	

	public boolean isServerRunning() {
		return myServer.isServerRunning();
	}

	public int getPort() {
		return myServer.getPort();
	}

	public String getHost() {
		return myServer.getHost();
	}

	public CommandHandler(ResourceManager2 bn, String host, int port,
			int timeout) {
		myParent = bn;
		myServer = new NodeServerSocket(this, host, port, timeout);
		window = timeout;
	}

	BasicMessage getBasicMessageResult(String data, int msgId) {
		BasicMessage bm = new BasicMessage(msgId, data);
		return bm;
	}

	String[] getCommandTokens(String peerNodeName, BasicMessage bm) {
		String[] command = null;
		command = bm.getData().split(",");
		return command;
	}

	boolean resultsRecieved(String peerNodeName, int msgId, CommandResults cr){
		pendingCommands.remove(msgId);
		return sendBackCommandResults(peerNodeName, msgId, cr);
	}
	
	boolean executecheckForResults(String peerNodeName, int msgId,
			int pendingMsgId) {

		CommandResults cr = myParent.checkForResults(pendingMsgId);
		if (cr != null && myPeerNodes.containsKey(peerNodeName)) {
			myPeerNodes.get(peerNodeName).send(
					getBasicMessageResult("results ready", msgId));
			sendBackCommandResults(peerNodeName, pendingMsgId, cr);
			return true;
		} else if (myPeerNodes.containsKey(peerNodeName))
			myPeerNodes.get(peerNodeName).send(
					getBasicMessageResult("results not ready", msgId));
		return false;
	}

	private boolean sendBackCommandResults(String peerNodeName, int msgId,
			CommandResults cr) {
		if (myPeerNodes.containsKey(peerNodeName)) {
			BasicMessage bm;
			try {
				String data = COMMAND_RESULTS_AVAILABLE + ","
						+ cr.toTransportString();
				bm = new BasicMessage(msgId, data);
				myPeerNodes.get(peerNodeName).send(bm);
				return true;
			} catch (DOMException e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e));
			} catch (ParserConfigurationException e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e));
			} catch (TransformerFactoryConfigurationError e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e.getException()));
			} catch (TransformerException e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e));
			} catch (SAXException e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e));
			} catch (IOException e) {
				LOGGER.debug("Attempting to send results, but failed.");
				LOGGER.debug(ExceptionToString.toString(e));
			}
		}
		return false;

	}

	String[] getCommandArgs(String[] cmdTokens) {
		String[] cmdArgs = new String[cmdTokens.length - 1];
		for (int i = 1; i < cmdTokens.length; i++)
			cmdArgs[i - 1] = cmdTokens[i];

		return cmdArgs;
	}

	boolean executeConnectPeerNode(String peerNodeName,
			BasicMessage basicMessage, String[] cmdArgs) {
		boolean result = false;
		String data = "Failed to process incoming command\n"
				+ "unable to connect to the remote host\n";
		if (handleConnectToPeer(cmdArgs)) {
			data = "The connections was made.\n";
			result = true;
		}
		myPeerNodes.get(peerNodeName).send(
				getBasicMessageResult(data, basicMessage.getMsgId()));
		pendingCommands.remove(basicMessage.getMsgId());

		return result;
	}

	boolean executeDisconnectPeerNode(String peerNodeName,
			BasicMessage basicMessage, String[] cmdArgs) {
		boolean result = false;
		String data = "Failed to process incoming command\n"
				+ "unable to disconnect to the remote host\n";
		if (handleDisconnectFromPeer(cmdArgs)) {
			data = "The disconnections was made.\n";
			result = true;
		}
		myPeerNodes.get(peerNodeName).send(
				getBasicMessageResult(data, basicMessage.getMsgId()));
		pendingCommands.remove(basicMessage.getMsgId());

		return result;
	}

	boolean executeCommand(String peerNodeName, BasicMessage basicMessage,
			String[] cmdArgs) {
		// FIXME this is a bad hack. I should be returning a bool, but at the
		// momemnt
		// i am not sure how to handle this boundary crossing from the
		// CommandHandler to the parent (aka ResourceManager)
		// FIXME need a better way to track results here.
		boolean result = false;
		String message = "";
		if (cmdArgs.length > 0)
			result = myParent.registerCommand(peerNodeName,
					basicMessage.getMsgId(), cmdArgs[0]);
		else
			message = "Command was not properly formatted";

		if (result) {
			message = "Command successfully added for processing";
		} else if (message.length() == 0) {
			message = "Failed to add the command for processing";
		}
		if (!result)
			pendingCommands.remove(basicMessage.getMsgId());

		myPeerNodes.get(peerNodeName).send(
				getBasicMessageResult(message, basicMessage.getMsgId()));

		return result;
	}

	boolean handleCommand(String peerNodeName, BasicMessage basicMessage) {
		boolean result = false;

		if (pendingCommands.containsKey(basicMessage.getMsgId())) {
			String data = "Failed to process incoming command\n"
					+ "Message ID is already pending\n";
			myPeerNodes.get(peerNodeName).send(
					getBasicMessageResult(data, basicMessage.getMsgId()));
			return result;
		}
		String[] cmdTokens = getCommandTokens(peerNodeName, basicMessage);
		if (cmdTokens == null) {
			return result;
		}
		pendingCommands.put(basicMessage.getMsgId(), peerNodeName);

		String[] cmdArgs = getCommandArgs(cmdTokens);

		if (cmdTokens[0].equals(COMMAND_CONNECT_PEER_NODE)
				&& cmdArgs.length >= 2) {
			result = executeConnectPeerNode(peerNodeName, basicMessage, cmdArgs);
		} else if (cmdTokens[0].equals(COMMAND_DISCONNECT_PEER_NODE)
				&& cmdArgs.length >= 2) {
			result = executeDisconnectPeerNode(peerNodeName, basicMessage,
					cmdArgs);
		} else if (cmdTokens[0].equals(COMMAND_EXECUTE_COMMAND)
				&& cmdArgs.length == 1) {
			result = executeCommand(peerNodeName, basicMessage, cmdArgs);
			if (!result)
				pendingCommands.put(basicMessage.getMsgId(), peerNodeName);
		} else if (cmdTokens[0].equals(COMMAND_POLL_STATUS)) {

			int pendingMsgId = Integer.parseInt(cmdArgs[0]);
			result = executecheckForResults(peerNodeName,
					basicMessage.getMsgId(), pendingMsgId);

		}else if (cmdTokens[0].equals(COMMAND_RESULTS_PURGE)
				&& cmdArgs.length >= 1) {
			result = executePurgeCommand(basicMessage.getMsgId(), peerNodeName, cmdArgs);
		}else if (cmdTokens[0].equals(COMMAND_RESULTS_PURGE_ALL)) {
			result = executePurgeAllCommand(basicMessage.getMsgId(), peerNodeName, cmdArgs);
		}
		
		return result;
	}

	boolean handleDisconnectFromPeer(String[] cmdTokens) {
		String host = cmdTokens[0];
		int port = -1;
		try {
			port = Integer.parseInt(cmdTokens[1]);
		} catch (NumberFormatException e) {
			LOGGER.debug("ConnectToPeer failed: could not parse port, "
					+ cmdTokens[1]);
			return false;
		}

		String peerNodeName = host + ":" + port;

		return myParent.disconnectFromNode(peerNodeName);

	}

	boolean handleConnectToPeer(String[] cmdTokens) {
		String host = cmdTokens[0];
		int port = -1, timeout = this.myServer.getTimeout();

		try {
			port = Integer.parseInt(cmdTokens[1]);
		} catch (NumberFormatException e) {
			LOGGER.debug("ConnectToPeer failed: could not parse port, "
					+ cmdTokens[1]);
			return false;
		}
		if (cmdTokens.length == 3) {
			try {
				timeout = Integer.parseInt(cmdTokens[2]);
			} catch (NumberFormatException e) {
				LOGGER.debug("ConnectToPeer failed: could not parse timeout, "
						+ cmdTokens[3]);
				return false;
			}
		}

		if (myParent.addPeerNode(host, port, timeout)) {
			return myParent.connectToPeer(host, port);
		}
		return false;
	}

	boolean handleAddCommand(String[] cmdTokens) {
		String commandXml = "";

		for (int i = 1; i < cmdTokens.length; i++) {
			commandXml += cmdTokens[i];
			if (i + 1 < cmdTokens.length) {
				commandXml += ",";
			}
		}
		Command cmd = new Command();
		try {
			cmd.fromTransportString(commandXml);
			myParent.addNewCommand(cmd);
			return true;
		} catch (ParserConfigurationException e) {
			LOGGER.debug("ParserConfiguration Exception: " + e.getMessage());
		} catch (SAXException e) {
			LOGGER.debug("SAX Exception: " + e.getMessage());
		} catch (IOException e) {
			LOGGER.debug("IOException: " + e.getMessage());
		} catch (InterruptedException e) {
			LOGGER.debug("Interruption Exception: " + e.getMessage());
		}
		return false;
	}

	@Override
	public void dataSent() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataRecieved(String host, String data) {
		// TODO Auto-generated method stub
		LOGGER.debug("Recieved a host: " + host);
		LOGGER.debug("Recieved a data: " + data);

	}

	@Override
	public void connectionOpened(Socket connection) {
		String peerNode = connection.getInetAddress().getHostAddress() + ":"
				+ connection.getPort();

		if (myPeerNodes.containsKey(peerNode)
				&& myPeerNodes.get(peerNode) != null) {
			LOGGER.debug("Appears that a stale connection may exist from peernode"
					+ peerNode);
			myPeerNodes.get(peerNode).cleanupDisconnectedSocket();
			LOGGER.debug("Cleaning up the existing node");
		}

		LOGGER.debug("Recieved a client socket, and adding to the my peers");
		try {
			addPeerNode(connection);
		} catch (IOException e) {
			LOGGER.debug("Failed to add the connection to the map of nodes\n"
					+ ExceptionToString.toString(e));
		}
	}

	@Override
	public void connectionClosed(Socket connection) {
		if (connection == clientSocket) {
			clientSocket = null;
			LOGGER.debug("Closed the client socket.");
		} else {
			LOGGER.debug("Recieved a client disconnection, but it does not appear to work for me");
		}

	}

	@Override
	public void dataRecieved(String host, int port, BufferedReader inStream) {

		LOGGER.debug("Recieved message from " + host + " " + port);
		String peerNodeName = host + ":" + port;
		Long time = new Long((new Date()).getTime() / 1000);
		IncompleteBasicMessage ibm = null;

		if (pendingMessages.containsKey(peerNodeName)
				&& pendingMessages.get(peerNodeName).isMessageStale(time,
						window)) {
			pendingMessages.remove(peerNodeName);
		}
		int numBytesToRead = 4; // size of the length
		if (pendingMessages.containsKey(peerNodeName)) {
			ibm = pendingMessages.get(peerNodeName);
			numBytesToRead = ibm.getLength() - ibm.getData().length();
			// ibm.append(data);
		}
		char[] bytesToRead = new char[numBytesToRead];
		int bytesRead = 0;
		try {
			bytesRead = inStream.read(bytesToRead);
		} catch (IOException e1) {
			LOGGER.debug("Error reading the input buffer.");
			LOGGER.debug(e1);
		}

		if (ibm != null) {
			ibm.append(new String(bytesToRead));
		} else {
			try {
				if (bytesRead < 4) {
					LOGGER.debug("Error reading the bytes for the message length.");
				}
				char[] msgId = new char[numBytesToRead];
				bytesRead = inStream.read(msgId);
				if (bytesRead < 4) {
					LOGGER.debug("Error reading the bytes for the message ID.");
				}
				ibm = new IncompleteBasicMessage(bytesToRead, msgId);
				pendingMessages.put(peerNodeName, ibm);
			} catch (IOException e) {
				LOGGER.debug("Error handling recieved data.");
				LOGGER.debug(e.getMessage());
			}
		}

		if (ibm != null && ibm.isMessageComplete()) {
			LOGGER.debug("Handling the completed basic message");
			pendingMessages.remove(peerNodeName);
			handleCommand(peerNodeName, new BasicMessage(ibm));
		}

	}

	public boolean addPeerNode(Socket connection) throws IOException {
		PlainTextNodeSocket pt = new PlainTextNodeSocket(this, connection,
				myServer.getTimeout());
		// pt.setPollTime(myServer.getTimeout());
		// pt.setTimeout(myServer.getTimeout());
		pt.startPolling();
		myPeerNodes.put(pt.getHost() + ":" + pt.getPort(), pt);
		return true;
	}

	@Override
	public void dataRecieved(Socket connection, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionClosed(String host, int port) {
		String peerNodeString = host + ":" + port;

		synchronized (myPeerNodes) {
			if (myPeerNodes.containsKey(peerNodeString)) {
				myPeerNodes.remove(peerNodeString);
			}
		}
		synchronized (pendingMessages) {
			if (pendingMessages.containsKey(peerNodeString)) {
				pendingMessages.remove(peerNodeString);
			}
		}

	}

	private boolean parentShutDown = false;

	@Override
	public void shutdown() {
		myServer.shutdown();
		if (!parentShutDown) {
			// break the cycle of shutting the down the parent
			// which in-turns shuts down the command handler.
			parentShutDown = true;
			myParent.shutdown();
		}
		synchronized (myPeerNodes) {
			for (String peerNode : myPeerNodes.keySet()) {
				NodeClientSocket pt = null;
				if (myPeerNodes.containsKey(peerNode)) {
					pt = myPeerNodes.get(peerNode);
					myPeerNodes.remove(peerNode);
				}
				if (pt != null) {
					pt.shutdown();

				}
			}

		}

	}

}
