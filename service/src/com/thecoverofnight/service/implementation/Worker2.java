/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.implementation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HostnameVerifier;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;



import com.thecoverofnight.service.comms.BasicMessage;
import com.thecoverofnight.service.comms.NodeClientSocket;
import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.CommandResults;
import com.thecoverofnight.service.job.JobManager;
import com.thecoverofnight.service.job.RunningTask;
import com.thecoverofnight.service.nodes.BaseNode;
import com.thecoverofnight.service.nodes.Worker;
import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.ExceptionToString;
import com.thecoverofnight.service.xml.NodeXMLReaderWriter;

class Dummy {

	Boolean done;

	public Boolean getDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

}

public class Worker2 extends BaseNode {
	Worker myWorkerListener = null;

	HashMap<String, String> cmdToHostMapping = new HashMap<String, String>();

	private void startAllServices() {
		LOGGER.debug("Starting the server. Now we can accept node connections or server connections.");
		LOGGER.debug("The IP Address of the Processor Service is: "
				+ myServer.getHost() + " and the port: " + myServer.getPort());
		this.startServer();
		/*
		 * try {
		 * LOGGER.debug("The IP Address of the Client/Command Handler Service is: "
		 * +cmdHandler.getHost()+" and the port: "+cmdHandler.getPort());
		 * this.startCommandHandler(); } catch (Exception e) {
		 * LOGGER.debug("Failed to start the command handler.");
		 * LOGGER.debug(ExceptionToString.toString(e)); }
		 */

	}

	protected void parseCommandLineArgs(String[] args) {
		CommandLineParser parser = new GnuParser();
		String host = "0.0.0.0";
		int port = 9000, timeout = 2000, backlog = 20;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.print("Error parsing arguments");
			System.err.print(ExceptionToString.toString(e));
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("worker", options);
			System.exit(1);
		}

		if (cmd.hasOption(CONFIGURATION_HELP)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("worker", options);
			System.exit(1);
		}
		String logger_conf = "example/log4j.xml";
		if (cmd.hasOption(CONFIGURATION_LOGGER)) {
			logger_conf = cmd.getOptionValue(CONFIGURATION_LOGGER);
			try {
				DOMConfigurator.configure(logger_conf);
			} catch (Exception e) {
				LOGGER.debug("Failed to set up the logger, did you provide the appropriate file location?.");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Exiting.");
				System.exit(-1);
			}

		}

		if (cmd.hasOption(CONFIGURATION_FILE)) {
			String filename = cmd.getOptionValue(CONFIGURATION_FILE);
			try {
				NodeXMLReaderWriter.parseFromFile(this, filename);
			} catch (ParserConfigurationException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			} catch (SAXException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			} catch (IOException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				System.exit(1);
			}

		}

		if (cmd.hasOption(CONFIGURATION_PORT)) {
			try {
				int x = Integer
						.parseInt(cmd.getOptionValue(CONFIGURATION_PORT));
				port = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + port);
			}
		}

		if (cmd.hasOption(CONFIGURATION_PORT)) {
			try {
				int x = Integer
						.parseInt(cmd.getOptionValue(CONFIGURATION_PORT));
				port = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + port);
			}
		}

		if (cmd.hasOption(CONFIGURATION_TIMEOUT)) {
			try {
				int x = Integer.parseInt(cmd
						.getOptionValue(CONFIGURATION_TIMEOUT));
				timeout = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + timeout);
			}
		}
		if (cmd.hasOption(CONFIGURATION_BACKLOG)) {
			try {
				int x = Integer.parseInt(cmd
						.getOptionValue(CONFIGURATION_BACKLOG));
				backlog = x;
			} catch (Exception e) {
				LOGGER.debug("Failed to parse port value");
				LOGGER.debug(ExceptionToString.toString(e));
				LOGGER.debug("Using the default value of: " + timeout);
			}
		}

		if (cmd.hasOption(CONFIGURATION_HOST)) {
			host = cmd.getOptionValue(CONFIGURATION_HOST);
		}

		window = (long) timeout;
		this.initServerParameters(host, port, timeout, backlog);
		startAllServices();

	}

	private String getXmlDocName(String data) throws SAXException, IOException,
			ParserConfigurationException {
		DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		Element node = docBuilder.parse(
				new ByteArrayInputStream(data.getBytes())).getDocumentElement();
		return node.getNodeName();

	}

	private boolean isCommandTransport(String data) throws SAXException,
			IOException, ParserConfigurationException {
		String name = getXmlDocName(data);
		return name != null && name.equalsIgnoreCase("command");
	}

	private boolean isCommandResultsTransport(String data) throws SAXException,
			IOException, ParserConfigurationException {
		String name = getXmlDocName(data);
		return name != null && name.equalsIgnoreCase("commandresults");
	}

	private JobManager myJobManager;

	protected static Logger LOGGER = Logger.getLogger(Worker2.class);

	public Worker2(String filename, Long timeout) {
		window = timeout;
		myJobManager = new JobManager(this);
		try {
			NodeXMLReaderWriter.parseFromFile(this, filename);
		} catch (ParserConfigurationException e) {

			LOGGER.debug(e.getMessage());
		} catch (SAXException e) {

			LOGGER.debug(e.getMessage());
		} catch (IOException e) {

			LOGGER.debug(e.getMessage());
		}
	}

	public Worker2(String[] args) {
		super();
		myJobManager = new JobManager(this);
		parseCommandLineArgs(args);

	}

	public static void main(String[] args) throws IOException,
			ParserConfigurationException {
		// moved log4j into the constructor
		// DOMConfigurator.configure("example/log4j.xml");
		// LOGGER.debug("Initializing the server from args.");
		Worker2 x = new Worker2(args);

		// LOGGER.debug("Starting the server.");
		// x.startServer();
		// the class actually starts the server

		/*
		 * final Dummy neverEndingStory = new Dummy();
		 * neverEndingStory.setDone(true); Timer t = new Timer(); TimerTask tt =
		 * new TimerTask() {
		 * 
		 * @Override public void run() { neverEndingStory.setDone(false); } };
		 * t.schedule(tt, 600 * 1000);
		 */
		try {
			// sleep for 2 seconds to let everything get fired up
			Thread.sleep(60 * 1000);
		} catch (InterruptedException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
		while (x.myServer.isServerRunning()) {
			try {
				Thread.sleep(60 * 1000);
			} catch (InterruptedException e) {
				LOGGER.debug(ExceptionToString.toString(e));
			}

		}
		// t = null;

		LOGGER.debug("Stopping the server.");
		x.shutdown();
	}

	@Override
	public void connectionOpened(Socket connection) {
		LOGGER.debug("Client connection received, adding the peer.\n");
		String peerNode = connection.getInetAddress().getHostAddress() + ":"
				+ connection.getPort();

		if (myPeerNodes.containsKey(peerNode)
				&& myPeerNodes.get(peerNode) != null) {
			LOGGER.debug("Appears that a stale connection may exist from peernode"
					+ peerNode);
			myPeerNodes.get(peerNode).cleanupDisconnectedSocket();
			LOGGER.debug("Cleaning up the existing node");
		}
		try {
			addPeerNode(connection);
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
	}

	@Override
	public void connectionClosed(Socket connection) {
		// TODO Auto-generated method stub
		LOGGER.debug("Client Connection closed.\n");

	}

	@Override
	public void handleMessage(String peerNodeName, BasicMessage message) {
		try {
			String data = message.getData();
			LOGGER.debug("Recieved the following message:\n" + (data));
			LOGGER.debug("Attempting to parse the command and add the message to the JobManager");

			if (isCommandTransport(data)) {
				Command cmd = new Command(data);
				myJobManager.addNewCommand(cmd);
				// FIXME not sure how to unmap the command
				cmdToHostMapping.put(cmd.getCommandId(), peerNodeName);
			} else if (isCommandResultsTransport(data)) {
				CommandResults cr = new CommandResults();
				cr.fromTransportString(data);
			}

		} catch (IOException e) {
			LOGGER.debug("Failed tso decode message string");
			LOGGER.debug(e.getMessage());
		} catch (ParserConfigurationException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (SAXException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (NullPointerException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (InterruptedException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (TransformerFactoryConfigurationError e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e.getException()));
		} catch (TransformerException e) {
			LOGGER.debug("Failed to parse the Command sent from the resource manager.");
			LOGGER.debug(e.getMessage());
			LOGGER.debug(ExceptionToString.toString(e));
		}
		// remove message
		pendingMessages.remove(peerNodeName);
	}

	@Override
	public void dataSent() {
		// TODO Auto-generated method stub
		LOGGER.debug("Data sent to the other end.\n");
	}

	public void jobCompleted(String taskId, Command cmd) {
		// FIXME use taskId to get running commands
		String cmdId = cmd.getCommandId();
		if (!cmdToHostMapping.containsKey(cmdId)) {
			return;
		}
		String peerNodeName = cmdToHostMapping.get(cmdId);
		// FIXME get results from the running command
		cmdToHostMapping.remove(cmdId);
	
		String cmdResultTransport = "";// = cmd.getResultToTransport();
		//
		sendData(peerNodeName, cmdResultTransport);

	}

	@Override
	public void commandCompleted(RunningTask rt) {
		String cmdId = rt.getCommand().getCommandId();
		if (!cmdToHostMapping.containsKey(cmdId)) {
			return;
		}
		String peerNode = cmdToHostMapping.get(rt.getCommand().getCommandId());
		String data = "Failed to Render Results Transort String: ";
		try {
			data = rt.getCommandResults().toTransportString();
		} catch (DOMException e) {
			data += ExceptionToString.toString(e);
			LOGGER.debug(data);
			data = null;
		} catch (ParserConfigurationException e) {
			data += ExceptionToString.toString(e);
			LOGGER.debug(data);
			data = null;
		} catch (TransformerFactoryConfigurationError e) {
			data += ExceptionToString.toString(e.getException());
			LOGGER.debug(data);
			data = null;
		} catch (TransformerException e) {
			data += ExceptionToString.toString(e);
			LOGGER.debug(data);
			data = null;
		} catch (SAXException e) {
			data += ExceptionToString.toString(e);
			LOGGER.debug(data);
			data = null;
		} catch (IOException e) {
			data += ExceptionToString.toString(e);
			LOGGER.debug(data);
			data = null;
		}
		try {
			isCommandTransport(data);
		} catch (SAXException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (ParserConfigurationException e) {
			LOGGER.debug(ExceptionToString.toString(e));
		}
		if (data != null) {
			sendData(peerNode, data);
		}
	}

	@Override
	public void connectionClosed(String host, int port) {
		String peerNodeString = host + ":" + port;

		synchronized (myPeerNodes) {
			if (myPeerNodes.containsKey(peerNodeString)) {
				myPeerNodes.remove(peerNodeString);
			}
		}
		synchronized (pendingMessages) {
			if (pendingMessages.containsKey(peerNodeString)) {
				pendingMessages.remove(peerNodeString);
			}
		}

	}

	private boolean shuttingDown = false;

	@Override
	public void shutdown() {
		if (shuttingDown)
			return;
		shuttingDown = true;
		myServer.shutdown();
		synchronized (myPeerNodes) {
			for (String peerNode : myPeerNodes.keySet()) {
				NodeClientSocket pt = null;
				if (myPeerNodes.containsKey(peerNode)) {
					pt = myPeerNodes.get(peerNode);
					myPeerNodes.remove(peerNode);
				}
				if (pt != null) {
					pt.shutdown();
				}
			}

		}

	}

}
