/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.CommandResult;
import com.thecoverofnight.service.job.CommandResults;
import com.thecoverofnight.service.job.FileContainer;

public class CommandResultsXMLReaderWriter extends
		BaseXMLReaderWriter<CommandResults> {

	FileContainerXMLReaderWriter FC_XML = FileContainerXMLReaderWriter.FC_XML_ONLY;
	CommandResultXMLReaderWriter CMDRES_XML = CommandResultXMLReaderWriter.CMDRES_XML_ONLY;
	static public final CommandResultsXMLReaderWriter CMDRESS_XML_ONLY = new CommandResultsXMLReaderWriter();

	public static final String XML_COMMAND_RESULTS = "CommandResults";
	public static final String XML_COMMAND_RESULTS_ID = "CommandResultsId";

	public static final String XML_COMMAND_RESULT_VALUE_ATTR = "value";

	String[] XML_COMMAND_RESULT_ATTRS;

	HashSet<String> XML_COMMAND_RESULT_ATTRS_HS;// = new HashSet<String>();

	private CommandResultsXMLReaderWriter() {
		super();

		XML_COMMAND_RESULT_ATTRS = new String[] { XML_COMMAND_RESULTS,
				XML_COMMAND_RESULTS_ID };

		XML_COMMAND_RESULT_ATTRS_HS = new HashSet<String>();
		for (String tag : XML_COMMAND_RESULT_ATTRS) {
			XML_COMMAND_RESULT_ATTRS_HS.add(tag);
		}
	}

	private Element getCommandResults(CommandResults component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		return getCommandResults(component, doc);
	}

	private Element getCommandResults(CommandResults component, Document doc)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Element rootElement = doc.createElement(XML_COMMAND_RESULTS);
		rootElement.appendChild(createElement(doc, XML_COMMAND_RESULTS_ID,
				component.getCommandId()));
		for (CommandResult cmdRes : component.getResults()) {
			rootElement.appendChild(cmdRes.toTransport(doc));
		}
		return rootElement;
	}

	@Override
	public Element toElement(CommandResults component) throws DOMException,
			SAXException, IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		return getCommandResults(component);

	}

	@Override
	public Document toDocument(CommandResults component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		// TODO verify this does not cause wierd behavior
		doc.appendChild(getCommandResults(component, doc));
		return doc;
	}

	@Override
	public void appendToDocument(CommandResults component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		doc.appendChild(getCommandResults(component, doc));
	}

	@Override
	public Element toTransport(CommandResults component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getCommandResults(component);
	}

	@Override
	public Element toTransport(CommandResults component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		// TODO Need to define whether the element should be added to the doc
		// prior to return
		return getCommandResults(component, doc);
	}

	@Override
	public String toTransportString(CommandResults component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommandResults(component));
	}

	@Override
	public String toTransportString(CommandResults component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommandResults(component, doc));
	}

	@Override
	public CommandResults[] fromTransport(Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return fromTransport(n, createDefaultHandler());

	}

	@Override
	public CommandResults[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		return fromTransport(xmlString, createDefaultHandler());

	}

	@Override
	public CommandResults[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return parse(n, handler);
	}

	@Override
	public CommandResults[] fromTransport(String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResults[0]);
	}

	@Override
	public CommandResults[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResults[0]);
	}

	@Override
	public CommandResults[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {

		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResults[0]);

	}

	@Override
	public ComponentSaxHandler createDefaultHandler() {
		return new ComponentSaxHandler(XML_COMMAND_RESULT_ATTRS) {
			private Logger LOGGER = Logger.getLogger(ComponentSaxHandler.class);

			private CommandResults cmdResults = null;
			private CommandResult currentCmdResult = null;

			@SuppressWarnings("static-access")
			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				if (component == null) {
					cmdResults = new CommandResults();
					component = cmdResults;
				} else if (cmdResults == null) {
					cmdResults = (CommandResults) component;
				}
				LOGGER.debug("StartTag: " + qName);
				if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)) {
					// This component is explicitly created, and it will be
					// processed further by calling
					// the interface startElement, endElement, and characters
					component = new FileContainer();
					addTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
				} else if (qName
						.equalsIgnoreCase(CMDRES_XML.XML_COMMAND_RESULT)) {
					currentCmdResult = new CommandResult();
					component = currentCmdResult;
				}

				if (component != null) {
					this.startTag(qName);
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@SuppressWarnings("static-access")
			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				LOGGER.debug("EndTag: " + qName);

				if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)
						&& component instanceof FileContainer) {
					currentCmdResult
							.addFileContainer((FileContainer) component);
					component = currentCmdResult;
					resetTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
				} else if (qName
						.equalsIgnoreCase(CMDRES_XML.XML_COMMAND_RESULT)) {
					cmdResults.addCommandResult(currentCmdResult);
					component = cmdResults;
					currentCmdResult = null;
					endTag(CMDRES_XML.XML_COMMAND_RESULT);
					resetTags(CMDRES_XML.XML_COMMAND_RESULT_ATTRS);
				}
			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {
				if (component != null)
					component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// FIXME pass?
			}

			@Override
			public void endParsingComponent(String componentName)
					throws SAXException {
				LOGGER.debug("EndParsing called for " + componentName);
			}
		};
	}

	@Override
	public void fromTransport(CommandResults component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		parse(component, n, createDefaultHandler());

	}

	@Override
	public void fromTransport(CommandResults component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		parse(component, xmlString, createDefaultHandler());

	}

	@Override
	public void fromTransport(CommandResults component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		parse(component, n, handler);

	}

	@Override
	public void fromTransport(CommandResults component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		parse(component, xmlString, handler);

	}

	@Override
	public void parse(CommandResults component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
	}

	@Override
	public void parse(CommandResults component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
	}

}
