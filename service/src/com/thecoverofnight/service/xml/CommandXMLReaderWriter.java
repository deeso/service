/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.analysismodule.AnalysisModule;
import com.thecoverofnight.service.job.Command;
import com.thecoverofnight.service.job.CommandData;
import com.thecoverofnight.service.job.FileContainer;

public class CommandXMLReaderWriter extends BaseXMLReaderWriter<Command> {

	static public final CommandXMLReaderWriter CMD_XML_ONLY = new CommandXMLReaderWriter();
	FileContainerXMLReaderWriter FC_XML = FileContainerXMLReaderWriter.FC_XML_ONLY;

	public static final String XML_COMMAND = "Command";
	public static final String XML_COMMAND_ID = "CommandId";
	public static final String XML_COMMAND_NAME = "CommandName";

	public static final String XML_COMMAND_ENV_VARS = "CommandEnvVars";
	public static final String XML_COMMAND_ENV_VAR = "CommandEnvVar";
	public static final String XML_COMMAND_ENV_VAR_KEY = "CommandEnvKey";
	public static final String XML_COMMAND_ENV_VAR_VAL = "CommandEnvVal";

	public static final String XML_COMMAND_PREP = "CommandPrep"; // followed by
																	// commands
	public static final String XML_COMMAND_PREPROCESS = "CommandPreprocess"; // followed
																				// by
																				// commands

	public static final String XML_COMMAND_LINE = "CommandLine";
	public static final String XML_COMMAND_CMD_LINE_VALUE = "value";
	public static final String XML_COMMAND_EXECUTABLE = "CommandExecutable"; // file
																				// containers
	public static final String XML_COMMAND_DATA = "CommandData"; // file
																	// containers
	public static final String XML_COMMAND_CWD = "CommandCWD";// set the CWD
																// before
																// running
																// commands

	public static final String XML_COMMAND_POST_PROCESS = "CommandPostProcess"; // followed
																				// by
																				// commands
	public static final String XML_COMMAND_CLEANUP = "CommandCleanUp"; // //
																		// followed
																		// by
																		// commands

	public static final String XML_COMMAND_RESULT = "CommandResult"; // //
																		// followed
																		// by
																		// commands

	String[] XML_COMMAND_ATTRS;
	HashSet<String> XML_COMMAND_ATTRS_HS;

	private CommandXMLReaderWriter() {
		super();
		XML_COMMAND_ATTRS = new String[] { XML_COMMAND, XML_COMMAND_ID,
				XML_COMMAND_NAME, XML_COMMAND_ENV_VAR, XML_COMMAND_ENV_VARS,
				XML_COMMAND_PREP, XML_COMMAND_PREPROCESS, XML_COMMAND_LINE,
				XML_COMMAND_EXECUTABLE, XML_COMMAND_DATA, XML_COMMAND_CWD,
				XML_COMMAND_POST_PROCESS, XML_COMMAND_CLEANUP };

		XML_COMMAND_ATTRS_HS = new HashSet<String>();
		for (String tag : XML_COMMAND_ATTRS) {

			XML_COMMAND_ATTRS_HS.add(tag);
		}
	}

	private Element getCommand(Command component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		return getCommand(component, doc);
	}

	private Element getCommand(Command component, Document doc)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Element rootElement = doc.createElement(XML_COMMAND);

		rootElement.appendChild(getCommandId(doc, component.getCommandId()));
		rootElement
				.appendChild(getCommandName(doc, component.getCommandName()));

		rootElement.appendChild(getCommandEnvVars(doc,
				component.getCommandEnvironment()));

		rootElement.appendChild(getPrepCommands(doc,
				component.getPrepCommands()));
		rootElement.appendChild(getPreProcessCommands(doc,
				component.getPreprocessCommands()));

		rootElement
				.appendChild(getCommandLine(doc, component.getCommandLine()));
		rootElement.appendChild(getCommandExecutable(doc,
				component.getCommandExecutable()));
		rootElement
				.appendChild(getCommandData(doc, component.getCommandData()));
		rootElement.appendChild(getCommandCWD(doc, component.getCommandCWD()));

		rootElement.appendChild(getPostProcessCommands(doc,
				component.getPostProcessCommands()));
		rootElement.appendChild(getCleanupCommands(doc,
				component.getCleanUpCommands()));

		for (String value : component.getResultFiles()) {
			rootElement.appendChild(createElement(doc, XML_COMMAND_RESULT,
					value));
		}

		return rootElement;
	}

	private Node getCleanupCommands(Document doc, ArrayList<Command> theCommands)
			throws SAXException, IOException, DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException {
		return getCommands(doc, theCommands, XML_COMMAND_CLEANUP);

	}

	private Node getCommands(Document doc, ArrayList<Command> theCommands,
			String tag) throws DOMException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			SAXException, IOException {

		Element commands = doc.createElement(tag);
		for (Command command : theCommands) {
			commands.appendChild(command.toTransport(doc));
		}

		return commands;
	}

	private Node getPostProcessCommands(Document doc,
			ArrayList<Command> theCommands) throws SAXException, IOException,
			DOMException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		return getCommands(doc, theCommands, XML_COMMAND_POST_PROCESS);

	}

	private Node getCommandCWD(Document doc, String myCommandCWD) {
		return createElement(doc, XML_COMMAND_CWD, myCommandCWD);
	}

	private Node getCommandData(Document doc,
			ArrayList<CommandData> myCommandData) throws DOMException,
			ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		Element cmdData = doc.createElement(XML_COMMAND_DATA);
		for (CommandData data : myCommandData) {
			cmdData.appendChild(data.toTransport(doc));
		}
		return cmdData;
	}

	private Node getCommandExecutable(Document doc, FileContainer fileContainer)
			throws SAXException, IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		Element e = doc.createElement(XML_COMMAND_EXECUTABLE);
		e.appendChild(fileContainer.toTransport(doc));
		return e;
	}

	private Node getCommandLine(Document doc, String myCommandLine) {
		Element e = doc.createElement(XML_COMMAND_LINE);
		// return createElement(doc, XML_COMMAND_LINE, myCommandLine);
		e.setAttribute(XML_COMMAND_CMD_LINE_VALUE, myCommandLine);
		return e;
	}

	private Node getPreProcessCommands(Document doc,
			ArrayList<Command> theCommands) throws SAXException, IOException,
			DOMException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getCommands(doc, theCommands, XML_COMMAND_PREPROCESS);
	}

	private Node getPrepCommands(Document doc, ArrayList<Command> theCommands)
			throws SAXException, IOException, DOMException,
			ParserConfigurationException, TransformerFactoryConfigurationError,
			TransformerException {
		return getCommands(doc, theCommands, XML_COMMAND_PREP);
	}

	private Node getEnvKeyValueElement(Document doc, String key, String value) {
		Element var = doc.createElement(XML_COMMAND_ENV_VAR);
		var.setAttribute(XML_COMMAND_ENV_VAR_KEY, key);
		var.setAttribute(XML_COMMAND_ENV_VAR_VAL, value);
		return var;
	}

	private Node getCommandEnvVars(Document doc,
			HashMap<String, String> myCommandEnvironment) {

		Element envVars = doc.createElement(XML_COMMAND_ENV_VARS);
		for (String envKey : myCommandEnvironment.keySet()) {
			envVars.appendChild(getEnvKeyValueElement(doc, envKey,
					myCommandEnvironment.get(envKey)));
		}
		return envVars;
	}

	private Node getCommandName(Document doc, String myCommandName) {
		return createElement(doc, XML_COMMAND_NAME, myCommandName);
	}

	private Node getCommandId(Document doc, String myCommandId) {
		return createElement(doc, XML_COMMAND_ID, myCommandId);
	}

	@Override
	public Element toElement(Command component) throws DOMException,
			SAXException, IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		return getCommand(component);

	}

	@Override
	public Document toDocument(Command component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		// TODO verify this does not cause wierd behavior
		doc.appendChild(getCommand(component, doc));
		return doc;
	}

	@Override
	public void appendToDocument(Command component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		doc.appendChild(getCommand(component, doc));
	}

	@Override
	public Element toTransport(Command component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getCommand(component);
	}

	@Override
	public Element toTransport(Command component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		// TODO Need to define whether the element should be added to the doc
		// prior to return
		return getCommand(component, doc);
	}

	@Override
	public String toTransportString(Command component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommand(component));
	}

	@Override
	public String toTransportString(Command component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommand(component, doc));
	}

	@Override
	public Command[] fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		return fromTransport(n, createDefaultHandler());

	}

	@Override
	public Command[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		return fromTransport(xmlString, createDefaultHandler());

	}

	@Override
	public Command[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return parse(n, handler);
	}

	@Override
	public Command[] fromTransport(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new Command[0]);
	}

	@Override
	public Command[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new Command[0]);
	}

	@Override
	public Command[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {

		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new Command[0]);

	}

	@Override
	public ComponentSaxHandler createDefaultHandler() {
		// TODO Auto-generated method stub
		return new ComponentSaxHandler(XML_COMMAND_ATTRS) {
			private Logger LOGGER = Logger.getLogger(ComponentSaxHandler.class);
			private Stack<HashMap<String, Boolean>> tagStack = new Stack<HashMap<String, Boolean>>();
			private Stack<Command> cmdStack = new Stack<Command>();

			/*
			 * Notes: I had to use some hokeyness to get the parser to work
			 * correctly with nested commands since i did not truly distinguish
			 * the difference (object wise) the difference between a Command and
			 * a Command in a CommandPrep set of commands.
			 * 
			 * To make it easier to write the parser, I also performed a several
			 * things. First, I use a cmdStack to track nested commands, and
			 * then I use a tagsStack to save the state of a context before
			 * delving further into the XML parse structure. For example, if I
			 * am parsing a Command, and I come across a CommandPrep list, I
			 * want to save the tags I have parsed for the current Command,
			 * before I move on and parse the CommandPrep Commands.
			 */

			void pushTags(String qName) {
				tagStack.push(this.cloneXmlTags());
				resetTags();
				startTag(qName);
			}

			void popTags(String qName) {
				this.resetTags();
				this.setTags(tagStack.pop());
				// this.endTag(qName);
			}

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				/*
				 * Notice that startTag is called to note where I am in the XML
				 * parse tree, and also note that in the condition of checking
				 * for a set of Commands, I perform a pushTags operation.
				 * 
				 * This is setup like this because the startTag should be
				 * recognized in the current context, and if I perform this
				 * method at the beginning of the routine, and then end up
				 * processing a list of Commands, the next context does not
				 * recognize I am parsing a set of Commands
				 */
				LOGGER.debug("StartTag: " + qName);
				if (qName.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND)) {
					// this will be the Root
					this.startTag(qName);
					if (component == null)
						component = new Command();
					cmdStack.push((Command) component);
					return;
				} else if (qName
						.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_POST_PROCESS)
						|| qName.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_PREPROCESS)
						|| qName.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_PREP)
						|| qName.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_CLEANUP)) {
					// Push the state of the current Tags processed onto a stack
					// (e.g. save it), and
					// then create a fresh context that reflects us parsing a
					// set of commands.
					pushTags(qName);
					component = null;
				} else if (qName
						.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_EXECUTABLE)) {
					// Component is set to null here because it will be used to
					// overide the call
					// to the components.startElement. This component will be
					// processed when the
					// FileContainer tag is processed.
					// need a startTag here since it wont be set otherwise
					this.startTag(qName);
					component = null;
				} else if (qName
						.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND_DATA)) {
					// Component is set to null here because it will be used to
					// overide the call
					// to the components.startElement. This component will be
					// processed when the
					// FileContainer(s) tags are processed.
					// need a startTag here since it wont be set otherwise
					this.startTag(qName);
					component = null;
				} else if (qName
						.equalsIgnoreCase(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE)) {
					// This component is explicitly created, and it will be
					// processed further by calling
					// the interface startElement, endElement, and characters
					component = new AnalysisModule();
				} else if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)) {
					// This component is explicitly created, and it will be
					// processed further by calling
					// the interface startElement, endElement, and characters
					component = new FileContainer();
				}

				if (component != null) {
					this.startTag(qName);
					component.startElement(uri, localName, qName, attributes,
							this);
				} else {
					LOGGER.debug("component was null");
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				LOGGER.debug("EndTag: " + qName);
				// handle the CommandExecutable which would be like an
				// executable file, but
				// its encapsulated in a FileContainer
				if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)
						&& checkTag(XML_COMMAND_EXECUTABLE)) {
					cmdStack.peek().setCommandExecutable(
							(FileContainer) component);
					// reset the FileContainers tags, since this state is not
					// tracked
					// like the Commands tags are

					resetTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
					endTag(XML_COMMAND_EXECUTABLE);
					component = cmdStack.peek();
					return;
					// handle the FileContainers in a CommandData set or list
				} else if (quickTagCheck(FC_XML.XML_FILE_CONTAINER, qName)
						&& checkTag(XML_COMMAND_DATA)) {
					// add the container to the most recent command we are
					// parsing
					cmdStack.peek().addCommandData((FileContainer) component);
					// reset the FileContainers tags, since this state is not
					// tracked
					// like the Commands tags are
					resetTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
					component = cmdStack.peek();
					return;
					// handle the end tag for CommandData set or list
				} else if (qName.equalsIgnoreCase(XML_COMMAND_DATA)) {
					// return is needed to prevent further processing
					component = cmdStack.peek();
					return;

				} else if (qName
						.equalsIgnoreCase(CommandXMLReaderWriter.XML_COMMAND)) {
					// add the most recent command to the corresponding list
					// being processed
					// in the second most recent command
					// FIXME need to handle the errors when there are not enough
					// components
					// on the stack (e.g. 2 Commands are needed for these ops to
					// work)
					if (checkTag(XML_COMMAND_PREP)) {
						cmdStack.pop();
						cmdStack.peek().addPrepCommand((Command) component);
					} else if (checkTag(XML_COMMAND_PREPROCESS)) {
						cmdStack.pop();
						cmdStack.peek().addPreProcessCommand(
								(Command) component);
					} else if (checkTag(XML_COMMAND_POST_PROCESS)) {
						cmdStack.pop();
						cmdStack.peek().addPostProcessCommand(
								(Command) component);
					} else if (checkTag(XML_COMMAND_CLEANUP)) {
						cmdStack.pop();
						cmdStack.peek().addCleanUpCommand((Command) component);
					} else {
						LOGGER.debug("Unable to identify the EndTag in the handler.xmlTags: "
								+ qName);
					}
					// set to null to prevent further processing
					// FIXME is setting to null necessary?
					// it prevents the components endElement from being called,
					// so this may be an
					// issue in the future.
					component = null;

				} else if (qName.equalsIgnoreCase(XML_COMMAND_POST_PROCESS)
						|| qName.equalsIgnoreCase(XML_COMMAND_PREPROCESS)
						|| qName.equalsIgnoreCase(XML_COMMAND_PREP)
						|| qName.equalsIgnoreCase(XML_COMMAND_CLEANUP)) {

					// set the current component to the one on the top of the
					// stack
					// FIXME this code assumes we have at least 1 Command on the
					// stack which is erroneous
					component = cmdStack.peek();
					if (tagStack.peek().containsKey(qName)
							&& !tagStack.peek().get(qName)) {
						popTags(qName);
						endTag(qName);
					} else {
						popTags(qName);
					}

				}
				// FIXME determine if this snippet of code is really necessary
				// in this parser
				if (component != null) {
					component.endElement(uri, localName, qName, this);
				}

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

				if (component != null)
					component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// FIXME pass?
			}

			@Override
			public void endParsingComponent(String componentName)
					throws SAXException {
				LOGGER.debug("EndParsing called for " + componentName);
				if (quickTagCheck(
						AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE,
						componentName)) {
					resetTags(AnalysisModuleXMLReaderWriter.XML_ANALYSIS_MODULE_ATTRS);
					component = null;
					component = cmdStack.peek();
				} else if (quickTagCheck(CommandXMLReaderWriter.XML_COMMAND,
						componentName)) {
					component = null;
					component = cmdStack.peek();
				} else if (componentName
						.equalsIgnoreCase(XML_COMMAND_POST_PROCESS)
						|| componentName
								.equalsIgnoreCase(XML_COMMAND_PREPROCESS)
						|| componentName.equalsIgnoreCase(XML_COMMAND_PREP)
						|| componentName.equalsIgnoreCase(XML_COMMAND_CLEANUP)) {
				}
			}
		};
	}

	@Override
	public void fromTransport(Command component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		parse(component, n, createDefaultHandler());

	}

	@Override
	public void fromTransport(Command component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		parse(component, xmlString, createDefaultHandler());

	}

	@Override
	public void fromTransport(Command component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		parse(component, n, handler);

	}

	@Override
	public void fromTransport(Command component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		parse(component, xmlString, handler);

	}

	@Override
	public void parse(Command component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
	}

	@Override
	public void parse(Command component, Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
	}

}
