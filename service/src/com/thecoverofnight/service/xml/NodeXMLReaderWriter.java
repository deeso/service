/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.thecoverofnight.service.nodes.BaseNode;
import com.thecoverofnight.service.nodes.ResourceManager;
import com.thecoverofnight.service.nodes.Worker;

public class NodeXMLReaderWriter {

	private NodeXMLReaderWriter() {
	}

	public static final NodeXMLReaderWriter NODE_XML = new NodeXMLReaderWriter();

	public final static String XML_NODE_NAME = "NodeName";
	public final static String XML_WORKER_NODE = "WorkerNode";

	public final static String XML_PEER_NODE = "PeerNode";
	public final static String XML_PEER_ALIAS = "PeerAlias";
	public final static String XML_PEER_NAME = "PeerName";
	public final static String XML_PEER_PORT = "PeerPort";
	public final static String XML_PEER_TIMEOUT = "PeerTimeout";
	public final static String XML_PEER_CONNECT = "PeerConnect";

	public final static String XML_RESOURCE_MANAGER = "ResourceManager";
	public final static String XML_SERVER_PARAMETERS = "ServerParameters";
	public final static String XML_SERVER_PORT = "Port";
	public final static String XML_SERVER_INTERFACE = "Interface";
	public final static String XML_SERVER_TIMEOUT = "Timeout";
	public final static String XML_SERVER_BACKLOG = "Backlog";

	public static String getXMLString(Document doc)
			throws TransformerFactoryConfigurationError, TransformerException {
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource();
		transformer.transform(source, result);
		return result.getWriter().toString();
	}

	static public void parse(final ResourceManager rm, String inputXML)
			throws ParserConfigurationException, SAXException, IOException {
		DefaultHandler mySAXHandler = new DefaultHandler() {
			boolean bfNodeName = false;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				System.out.println("Start Element :" + qName);

				if (qName.equalsIgnoreCase(XML_SERVER_PARAMETERS)) {
					initServer(rm, attributes);
				}
				if (qName.equalsIgnoreCase(XML_PEER_NODE)) {
					addPeerNode(rm, attributes);
				}
				if (qName.equalsIgnoreCase(XML_NODE_NAME)) {
					bfNodeName = true;
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
			}

			@Override
			public void characters(char ch[], int start, int length)
					throws SAXException {
				if (bfNodeName) {
					rm.setName(new String(ch, start, length));
					bfNodeName = false;
				}
			}
		};

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		InputStream is = new ByteArrayInputStream(inputXML.getBytes());
		saxParser.parse(is, mySAXHandler);

	}

	static private void addPeerNode(BaseNode bn, Attributes attributes) {
		Integer timeout = null, port = null;
		String host = null, alias = null;
		boolean connect = false;

		for (int i = 0; i < attributes.getLength(); i++) {
			if (attributes.getQName(i).equalsIgnoreCase(XML_PEER_PORT)) {
				port = new Integer(attributes.getValue(i));
			} else if (attributes.getQName(i)
					.equalsIgnoreCase(XML_PEER_TIMEOUT)) {
				timeout = new Integer(attributes.getValue(i));
			} else if (attributes.getQName(i).equalsIgnoreCase(XML_PEER_NAME)) {
				host = attributes.getValue(i);
			} else if (attributes.getQName(i).equalsIgnoreCase(XML_PEER_ALIAS)) {
				alias = attributes.getValue(i);
			} else if (attributes.getQName(i)
					.equalsIgnoreCase(XML_PEER_CONNECT)) {
				connect = Boolean.getBoolean(attributes.getValue(i)
						.toLowerCase());
			}
		}
		if (host != null && port != null && timeout != null) {
			bn.addPeerNode(host, port, timeout);
		}
		if (alias != null) {
			bn.addAlias(host, alias);
		}
		if (connect) {
			bn.connectToPeer(host + ":" + port);
		}

	}

	static private void initServer(BaseNode bn, Attributes attributes) {
		Integer timeout = null, port = null, backlog = null;
		String interface_ = null;

		for (int i = 0; i < attributes.getLength(); i++) {
			if (attributes.getQName(i).equalsIgnoreCase(XML_SERVER_PORT)) {
				port = new Integer(attributes.getValue(i));
			}
			if (attributes.getQName(i).equalsIgnoreCase(XML_SERVER_TIMEOUT)) {
				timeout = new Integer(attributes.getValue(i));
			}
			if (attributes.getQName(i).equalsIgnoreCase(XML_SERVER_BACKLOG)) {
				backlog = new Integer(attributes.getValue(i));
			}
			if (attributes.getQName(i).equalsIgnoreCase(XML_SERVER_INTERFACE)) {
				interface_ = attributes.getValue(i);
			}
			if (interface_ != null && backlog != null && port != null
					&& timeout != null) {
				bn.initServerParameters(interface_, port, timeout, backlog);
				break;
			}
		}

	}

	static public void parse(final Worker wn, String inputXML)
			throws ParserConfigurationException, SAXException, IOException {
		DefaultHandler mySAXHandler = new DefaultHandler() {
			boolean bfNodeName = false;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				System.out.println("Start Element :" + qName);

				if (qName.equalsIgnoreCase(XML_SERVER_PARAMETERS)) {
					initServer(wn, attributes);
				}
				if (qName.equalsIgnoreCase(XML_PEER_NODE)) {
					addPeerNode(wn, attributes);
				}
				if (qName.equalsIgnoreCase(XML_NODE_NAME)) {
					bfNodeName = true;
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
			}

			@Override
			public void characters(char ch[], int start, int length)
					throws SAXException {
				if (bfNodeName) {
					wn.setName(new String(ch, start, length));
					bfNodeName = false;
				}
			}
		};
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		InputStream is = new ByteArrayInputStream(inputXML.getBytes());
		saxParser.parse(is, mySAXHandler);
	}

	static public void parse(final BaseNode wn, String inputXML)
			throws ParserConfigurationException, SAXException, IOException {
		DefaultHandler mySAXHandler = new DefaultHandler() {
			boolean bfNodeName = false;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				System.out.println("Start Element :" + qName);

				if (qName.equalsIgnoreCase(XML_SERVER_PARAMETERS)) {
					initServer(wn, attributes);
				}
				if (qName.equalsIgnoreCase(XML_PEER_NODE)) {
					addPeerNode(wn, attributes);
				}
				if (qName.equalsIgnoreCase(XML_NODE_NAME)) {
					bfNodeName = true;
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
			}

			@Override
			public void characters(char ch[], int start, int length)
					throws SAXException {
				if (bfNodeName) {
					wn.setName(new String(ch, start, length));
					bfNodeName = false;
				}
			}
		};
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		InputStream is = new ByteArrayInputStream(inputXML.getBytes());
		saxParser.parse(is, mySAXHandler);
	}

	static public void parseFromFile(BaseNode bn, String filename)
			throws ParserConfigurationException, SAXException, IOException {
		File file = new File(filename);
		DataInputStream dis;
		dis = new DataInputStream(new BufferedInputStream(new FileInputStream(
				file)));
		byte[] data = new byte[(int) file.length()];
		dis.read(data);
		parse(bn, new String(data));
	}

	static public void parseFromFile(Worker wn, String filename)
			throws ParserConfigurationException, SAXException, IOException {
		File file = new File(filename);
		DataInputStream dis;
		dis = new DataInputStream(new BufferedInputStream(new FileInputStream(
				file)));
		byte[] data = new byte[(int) file.length()];
		dis.read(data);
		parse(wn, new String(data));
	}

	static public void parseFromFile(ResourceManager rm, String filename)
			throws IOException, ParserConfigurationException, SAXException {
		File file = new File(filename);
		DataInputStream dis;
		dis = new DataInputStream(new BufferedInputStream(new FileInputStream(
				file)));
		byte[] data = new byte[(int) file.length()];
		dis.read(data);
		parse(rm, new String(data));
	}
}
