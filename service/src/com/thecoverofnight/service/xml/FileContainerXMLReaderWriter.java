/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.FileContainer;

public class FileContainerXMLReaderWriter extends
		BaseXMLReaderWriter<FileContainer> {

	public final String XML_FILE_CONTAINER = "FileContainer";
	public final String XML_FILE_NAME = "Filename";
	public final String XML_FILE_DATA = "FileData";
	public final String XML_FILE_ORIGINAL_BASE = "FileOriginalBaseDirectory";
	public final String XML_FILE_DESTINATION_BASE = "FileDestinationBaseDirectory";
	public final String XML_FILE_PERMISSIONS = "FilePermissions";

	public final String[] XML_FILE_CONTAINER_ATTRS = new String[] {
			XML_FILE_CONTAINER, XML_FILE_NAME, XML_FILE_DATA,
			XML_FILE_ORIGINAL_BASE, XML_FILE_DESTINATION_BASE,
			XML_FILE_PERMISSIONS };

	private FileContainerXMLReaderWriter() {
	}

	static public final FileContainerXMLReaderWriter FC_XML_ONLY = new FileContainerXMLReaderWriter();

	private Element getFileContainter(FileContainer component)
			throws ParserConfigurationException {
		Document doc = createDocument();
		return getFileContainter(component, doc);
	}

	private Element getFileContainter(FileContainer component, Document doc)
			throws ParserConfigurationException {
		Element rootElement = doc.createElement(XML_FILE_CONTAINER);
		rootElement
				.appendChild(getFilenameElement(doc, component.getFilename()));
		rootElement
				.appendChild(getFileDataElement(doc, component.getFiledata()));
		rootElement.appendChild(getFileOriginalBaseElement(doc,
				component.getOriginalBaseDirectory()));
		rootElement.appendChild(getFileDestinationBaseElement(doc,
				component.getDestinationBaseDirectory()));
		rootElement.appendChild(getFilePermissionsElement(doc,
				component.getPermissions()));
		return rootElement;
	}

	private Element getFilenameElement(Document doc, String value) {
		return createElement(doc, XML_FILE_NAME, value);
	}

	private Element getFileDataElement(Document doc, String value) {
		return createElement(doc, XML_FILE_DATA, value);
	}

	private Element getFileOriginalBaseElement(Document doc, String value) {
		return createElement(doc, XML_FILE_ORIGINAL_BASE, value);
	}

	private Element getFileDestinationBaseElement(Document doc, String value) {
		return createElement(doc, XML_FILE_DESTINATION_BASE, value);
	}

	private Element getFilePermissionsElement(Document doc, String value) {
		return createElement(doc, XML_FILE_PERMISSIONS, value);
	}

	@Override
	public Element toElement(FileContainer component) {
		try {
			return getFileContainter(component);
		} catch (ParserConfigurationException e) {
			// FIXME throw exception here, dont swallow
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Document toDocument(FileContainer component)
			throws ParserConfigurationException {
		Document doc = createDocument();
		// TODO verify this does not cause wierd behavior
		doc.appendChild(getFileContainter(component, doc));
		return doc;
	}

	@Override
	public void appendToDocument(FileContainer component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		doc.appendChild(getFileContainter(component, doc));
	}

	@Override
	public Element toTransport(FileContainer component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getFileContainter(component);
	}

	@Override
	public Element toTransport(FileContainer component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		// TODO Need to define whether the element should be added to the doc
		// prior to return
		return getFileContainter(component, doc);
	}

	@Override
	public String toTransportString(FileContainer component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getXMLString(getFileContainter(component));
	}

	@Override
	public String toTransportString(FileContainer component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getXMLString(getFileContainter(component, doc));
	}

	@Override
	public FileContainer[] fromTransport(Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return fromTransport(n, createDefaultHandler());

	}

	@Override
	public FileContainer[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		return fromTransport(xmlString, createDefaultHandler());

	}

	@Override
	public FileContainer[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return parse(n, handler);
	}

	@Override
	public FileContainer[] fromTransport(String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new FileContainer[0]);
	}

	@Override
	public FileContainer[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new FileContainer[0]);
	}

	@Override
	public FileContainer[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {

		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new FileContainer[0]);

	}

	@Override
	public ComponentSaxHandler createDefaultHandler() {

		return new ComponentSaxHandler(XML_FILE_CONTAINER_ATTRS) {

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				this.startTag(qName);
				if (qName.equalsIgnoreCase(XML_FILE_CONTAINER)) {
					component = new FileContainer();
					this.endTag(qName);
					return;
				}
				if (component != null) {
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {

				component.endElement(uri, localName, qName, this);

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

				if (component == null) {
					return;
				}
				component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// TODO pass?

			}

			@Override
			public void endParsingComponent(String componentName)
					throws SAXException {
				if (component != null) {
					result.add(component);
					component = null;
				}
				resetTags(XML_FILE_CONTAINER_ATTRS);

			}
		};
	}

	@Override
	public void fromTransport(FileContainer component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		parse(component, n, createSingleHandler());

	}

	@Override
	public void fromTransport(FileContainer component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		parse(component, xmlString, createSingleHandler());

	}

	@Override
	public void fromTransport(FileContainer component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		parse(component, n, handler);

	}

	@Override
	public void fromTransport(FileContainer component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		parse(component, xmlString, handler);

	}

	@Override
	public void parse(FileContainer component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
	}

	@Override
	public void parse(FileContainer component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
	}

	public ComponentSaxHandler createSingleHandler() {

		return new ComponentSaxHandler(XML_FILE_CONTAINER_ATTRS) {

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				this.startTag(qName);
				if (qName.equalsIgnoreCase(XML_FILE_CONTAINER)) {
					// component = new FileContainer();
					this.endTag(qName);
					return;
				}
				if (component != null) {
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {

				component.endElement(uri, localName, qName, this);

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

				if (component == null) {
					return;
				}
				component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// TODO pass?

			}

			@Override
			public void endParsingComponent(String componentName)
					throws SAXException {
				if (component != null) {
					result.add(component);
					component = null;
				}
				resetTags(XML_FILE_CONTAINER_ATTRS);

			}
		};
	}

}
