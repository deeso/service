/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.job.CommandResult;
import com.thecoverofnight.service.job.FileContainer;

public class CommandResultXMLReaderWriter extends
		BaseXMLReaderWriter<CommandResult> {

	FileContainerXMLReaderWriter FC_XML = FileContainerXMLReaderWriter.FC_XML_ONLY;
	static public final CommandResultXMLReaderWriter CMDRES_XML_ONLY = new CommandResultXMLReaderWriter();

	public static final String XML_COMMAND_RESULT = "CommandResult";
	public static final String XML_COMMAND_RESULT_ID = "CommandResultId";
	public static final String XML_COMMAND_RESULT_NAME = "CommandResultName";
	public static final String XML_COMMAND_RESULT_FILES = "CommandResultFiles"; // file
																				// containers
	public static final String XML_COMMAND_RESULT_MESSAGE = "CommandResultMessage";
	public static final String XML_COMMAND_RESULT_MESSAGES = "CommandResultMessages";
	public static final String XML_COMMAND_RESULT_STATUS_CODE = "CommandResultStatusCode";

	public static final String XML_COMMAND_RESULT_VALUE_ATTR = "value";

	String[] XML_COMMAND_RESULT_ATTRS;

	HashSet<String> XML_COMMAND_RESULT_ATTRS_HS;// = new HashSet<String>();

	private CommandResultXMLReaderWriter() {
		super();

		XML_COMMAND_RESULT_ATTRS = new String[] { XML_COMMAND_RESULT,
				XML_COMMAND_RESULT_ID, XML_COMMAND_RESULT_NAME,
				XML_COMMAND_RESULT_STATUS_CODE, XML_COMMAND_RESULT_FILES,
				XML_COMMAND_RESULT_MESSAGES, XML_COMMAND_RESULT_MESSAGE };

		XML_COMMAND_RESULT_ATTRS_HS = new HashSet<String>();
		for (String tag : XML_COMMAND_RESULT_ATTRS) {
			XML_COMMAND_RESULT_ATTRS_HS.add(tag);
		}
	}

	private Element getCommandResult(CommandResult component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		return getCommandResult(component, doc);
	}

	private Element getCommandResult(CommandResult component, Document doc)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Element rootElement = doc.createElement(XML_COMMAND_RESULT);

		rootElement.appendChild(getCommandResultId(doc,
				component.getCommandResultId()));
		rootElement.appendChild(getCommandResultName(doc,
				component.getCommandResultName()));
		rootElement.appendChild(getCommandResultStatusCode(doc,
				Integer.toString(component.getCommandResultStatusCode())));

		rootElement.appendChild(getCommandResultMessages(doc,
				component.getCommandResultMessages()));
		rootElement.appendChild(getCommandResultFiles(doc,
				component.getCommandResultFiles()));

		return rootElement;
	}

	private Node getCommandResultFiles(Document doc,
			ArrayList<FileContainer> fileContainers) throws SAXException,
			IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		Element e = doc.createElement(XML_COMMAND_RESULT_FILES);
		for (FileContainer fc : fileContainers) {
			e.appendChild(fc.toTransport(doc));
		}

		return e;
	}

	private Node getCommandResultMessages(Document doc,
			ArrayList<String> messages) {
		Element xmlMessages = doc.createElement(XML_COMMAND_RESULT_MESSAGES);
		for (String message : messages) {
			Element n = doc.createElement(XML_COMMAND_RESULT_MESSAGE);
			n.setAttribute(XML_COMMAND_RESULT_VALUE_ATTR, message);
			xmlMessages.appendChild(n);
		}
		return xmlMessages;
	}

	private Node getCommandResultName(Document doc, String myCommandName) {
		Element n = doc.createElement(XML_COMMAND_RESULT_NAME);
		n.setAttribute(XML_COMMAND_RESULT_VALUE_ATTR, myCommandName);
		return n;
	}

	private Node getCommandResultId(Document doc, String myCommandId) {
		Element n = doc.createElement(XML_COMMAND_RESULT_ID);
		n.setAttribute(XML_COMMAND_RESULT_VALUE_ATTR, myCommandId);
		return n;
	}

	private Node getCommandResultStatusCode(Document doc, String statusCode) {
		Element n = doc.createElement(XML_COMMAND_RESULT_STATUS_CODE);
		n.setAttribute(XML_COMMAND_RESULT_VALUE_ATTR, statusCode);
		return n;
	}

	@Override
	public Element toElement(CommandResult component) throws DOMException,
			SAXException, IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {

		return getCommandResult(component);

	}

	@Override
	public Document toDocument(CommandResult component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException {
		Document doc = createDocument();
		// TODO verify this does not cause wierd behavior
		doc.appendChild(getCommandResult(component, doc));
		return doc;
	}

	@Override
	public void appendToDocument(CommandResult component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		doc.appendChild(getCommandResult(component, doc));
	}

	@Override
	public Element toTransport(CommandResult component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getCommandResult(component);
	}

	@Override
	public Element toTransport(CommandResult component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		// TODO Need to define whether the element should be added to the doc
		// prior to return
		return getCommandResult(component, doc);
	}

	@Override
	public String toTransportString(CommandResult component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommandResult(component));
	}

	@Override
	public String toTransportString(CommandResult component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException {
		return getXMLString(getCommandResult(component, doc));
	}

	@Override
	public CommandResult[] fromTransport(Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return fromTransport(n, createDefaultHandler());

	}

	@Override
	public CommandResult[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		return fromTransport(xmlString, createDefaultHandler());

	}

	@Override
	public CommandResult[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return parse(n, handler);
	}

	@Override
	public CommandResult[] fromTransport(String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResult[0]);
	}

	@Override
	public CommandResult[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResult[0]);
	}

	@Override
	public CommandResult[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {

		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new CommandResult[0]);

	}

	@Override
	public ComponentSaxHandler createDefaultHandler() {
		return new ComponentSaxHandler(XML_COMMAND_RESULT_ATTRS) {
			private Logger LOGGER = Logger.getLogger(ComponentSaxHandler.class);

			private CommandResult cmdResult = null;

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				if (component == null) {
					cmdResult = new CommandResult();
					component = cmdResult;
				} else if (cmdResult == null) {
					cmdResult = (CommandResult) component;
				}

				LOGGER.debug("StartTag: " + qName);
				if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)) {
					// This component is explicitly created, and it will be
					// processed further by calling
					// the interface startElement, endElement, and characters
					component = new FileContainer();
				}

				if (component != null) {
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {
				LOGGER.debug("EndTag: " + qName);

				if (qName.equalsIgnoreCase(FC_XML.XML_FILE_CONTAINER)
						&& component instanceof FileContainer) {
					cmdResult.addFileContainer((FileContainer) component);
					component = cmdResult;
					resetTags(FC_XML.XML_FILE_CONTAINER_ATTRS);
				}

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

			}

			@Override
			public void beginParsingComponent(String componentName) {
				// FIXME pass?
			}

			@Override
			public void endParsingComponent(String componentName)
					throws SAXException {
				LOGGER.debug("EndParsing called for " + componentName);
			}
		};
	}

	@Override
	public void fromTransport(CommandResult component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		parse(component, n, createDefaultHandler());

	}

	@Override
	public void fromTransport(CommandResult component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		parse(component, xmlString, createDefaultHandler());

	}

	@Override
	public void fromTransport(CommandResult component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		parse(component, n, handler);

	}

	@Override
	public void fromTransport(CommandResult component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		parse(component, xmlString, handler);

	}

	@Override
	public void parse(CommandResult component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
	}

	@Override
	public void parse(CommandResult component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
	}

}
