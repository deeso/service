/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.analysismodule.AnalysisModule;
import com.thecoverofnight.service.xml.ComponentSaxHandler;

public class AnalysisModuleXMLReaderWriter extends
		BaseXMLReaderWriter<AnalysisModule> {

	private AnalysisModuleXMLReaderWriter() {
	}

	public static final AnalysisModuleXMLReaderWriter AM_XML_ONLY = new AnalysisModuleXMLReaderWriter();

	public final static String XML_ANALYSIS_MODULE = "AnalysisModule";
	public final static String XML_ANALYSIS_MODULE_FILE = "AnalysisModuleFile";
	public final static String XML_ANALYSIS_MODULE_NAME = "AnalysisModuleName";
	public final static String XML_ANALYSIS_MODULE_CODE = "AnalysisModuleCode";
	public final static String XML_ANALYSIS_MODULE_AVAILABLE = "AnalysisModuleAvailable";
	public final static String XML_ANALYSIS_MODULE_DEPENDENCY = "AnalysisModuleDependency";
	public final static String XML_ANALYSIS_MODULE_TYPE = "AnalysisModuleType";

	static final String[] XML_ANALYSIS_MODULE_ATTRS = { XML_ANALYSIS_MODULE,
			XML_ANALYSIS_MODULE_FILE, XML_ANALYSIS_MODULE_NAME,
			XML_ANALYSIS_MODULE_CODE, XML_ANALYSIS_MODULE_AVAILABLE,
			XML_ANALYSIS_MODULE_DEPENDENCY, XML_ANALYSIS_MODULE_TYPE, };

	public static Element getAnalysisModule(AnalysisModule component,
			Document doc) throws ParserConfigurationException {

		Element rootElement = doc.createElement(XML_ANALYSIS_MODULE);
		rootElement.appendChild(getName(doc, component));
		rootElement.appendChild(getCode(doc, component));
		rootElement.appendChild(getAvailability(doc, component));

		Element[] deps = getDependencies(doc, component);
		for (Element e : deps)
			rootElement.appendChild(e);

		return rootElement;
	}

	public static Element getAnalysisModule(AnalysisModule component)
			throws ParserConfigurationException {
		return getAnalysisModule(component, createDocument());
	}

	public static Element getName(Document doc, AnalysisModule component) {
		return createElement(doc, XML_ANALYSIS_MODULE_NAME, component.getName());
	}

	public static Element getCode(Document doc, AnalysisModule component) {
		return createElement(doc, XML_ANALYSIS_MODULE_CODE, component.getCode());
	}

	public static Element getType(Document doc, AnalysisModule component) {
		return createElement(doc, XML_ANALYSIS_MODULE_TYPE,
				component.getModuleType());
	}

	public static Element getAvailability(Document doc, AnalysisModule component) {
		return createElement(doc, XML_ANALYSIS_MODULE_AVAILABLE,
				component.getAvailability());
	}

	public static Element[] getDependencies(Document doc,
			AnalysisModule component) {
		ArrayList<Element> elements = new ArrayList<Element>();
		for (String dep : component.getDependencies()) {
			Element e = createElement(doc, XML_ANALYSIS_MODULE_DEPENDENCY, dep);
			elements.add(e);
		}
		return elements.toArray(new Element[0]);
	}

	@Override
	public Element toElement(AnalysisModule component) {
		try {
			return getAnalysisModule(component);
		} catch (ParserConfigurationException e) {
			// FIXME throw exception here, dont swallow
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Document toDocument(AnalysisModule component)
			throws ParserConfigurationException {
		Document doc = createDocument();
		// TODO verify this does not cause wierd behavior
		doc.appendChild(getAnalysisModule(component, doc));
		return doc;
	}

	@Override
	public void appendToDocument(AnalysisModule component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		doc.appendChild(getAnalysisModule(component, doc));
	}

	@Override
	public Element toTransport(AnalysisModule component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getAnalysisModule(component);
	}

	@Override
	public Element toTransport(AnalysisModule component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		// TODO Need to define whether the element should be added to the doc
		// prior to return
		return getAnalysisModule(component, doc);
	}

	@Override
	public String toTransportString(AnalysisModule component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getXMLString(getAnalysisModule(component));
	}

	@Override
	public String toTransportString(AnalysisModule component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException {
		return getXMLString(getAnalysisModule(component, doc));
	}

	@Override
	public AnalysisModule[] fromTransport(Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return fromTransport(n, createDefaultHandler());

	}

	@Override
	public AnalysisModule[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		return fromTransport(xmlString, createDefaultHandler());

	}

	@Override
	public AnalysisModule[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		return parse(n, handler);
	}

	@Override
	public AnalysisModule[] fromTransport(String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new AnalysisModule[0]);
	}

	@Override
	public AnalysisModule[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException {
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new AnalysisModule[0]);
	}

	@Override
	public AnalysisModule[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {

		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
		return handler.getResult().toArray(new AnalysisModule[0]);

	}

	@Override
	public ComponentSaxHandler createDefaultHandler() {

		return new ComponentSaxHandler(XML_ANALYSIS_MODULE_ATTRS) {

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				this.startTag(qName);
				if (qName.equalsIgnoreCase(XML_ANALYSIS_MODULE)) {
					component = new AnalysisModule();
					this.endTag(qName);
					return;
				}
				if (component != null) {
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {

				component.endElement(uri, localName, qName, this);

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

				if (component == null) {
					return;
				}
				component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// TODO Auto-generated method stub

			}

			@Override
			public void endParsingComponent(String componentName) {
				if (component != null) {
					result.add(component);
					component = null;
				}
				resetTags(XML_ANALYSIS_MODULE_ATTRS);
			}
		};
	}

	@Override
	public void fromTransport(AnalysisModule component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException {
		parse(component, n, createSingleHandler());

	}

	@Override
	public void fromTransport(AnalysisModule component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException {
		parse(component, xmlString, createSingleHandler());

	}

	@Override
	public void fromTransport(AnalysisModule component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		parse(component, n, handler);

	}

	@Override
	public void fromTransport(AnalysisModule component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		parse(component, xmlString, handler);

	}

	@Override
	public void parse(AnalysisModule component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(xmlString.getBytes());
		createSaxParser().parse(is, handler);
	}

	@Override
	public void parse(AnalysisModule component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException {
		handler.setComponent(component);
		InputStream is = new ByteArrayInputStream(getXMLString(n).getBytes());
		createSaxParser().parse(is, handler);
	}

	public ComponentSaxHandler createSingleHandler() {

		return new ComponentSaxHandler(XML_ANALYSIS_MODULE_ATTRS) {

			@Override
			public void startElement(String uri, String localName,
					String qName, Attributes attributes) throws SAXException {

				this.startTag(qName);
				if (qName.equalsIgnoreCase(XML_ANALYSIS_MODULE)) {
					// component = new AnalysisModule();
					this.endTag(qName);
					return;
				}
				if (component != null) {
					component.startElement(uri, localName, qName, attributes,
							this);
				}

			}

			@Override
			public void endElement(String uri, String localName, String qName)
					throws SAXException {

				component.endElement(uri, localName, qName, this);

			}

			@Override
			public void characters(char[] ch, int start, int length)
					throws SAXException {

				if (component == null) {
					return;
				}
				component.characters(ch, start, length, this);
			}

			@Override
			public void beginParsingComponent(String componentName) {
				// TODO Auto-generated method stub

			}

			@Override
			public void endParsingComponent(String componentName) {
				if (component != null) {
					result.add(component);
					component = null;
				}
				resetTags(XML_ANALYSIS_MODULE_ATTRS);
			}
		};
	}
}
