/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.util.ArrayList;
import java.util.HashMap;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.thecoverofnight.service.util.Component;

public abstract class ComponentSaxHandler extends DefaultHandler {

	protected ArrayList<Component> result = new ArrayList<Component>();

	protected Component component = null;

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	HashMap<String, Boolean> xmlTags = new HashMap<String, Boolean>();

	public ComponentSaxHandler(String[] tags) {
		super();
		initializeTags(tags);
	}

	public void endTag(String tag) {
		if (xmlTags.containsKey(tag)) {
			xmlTags.put(tag, false);
		}
	}

	public void startTag(String tag) {
		xmlTags.put(tag, true);
	}

	public void resetTags() {
		for (String tag : xmlTags.keySet()) {
			xmlTags.put(tag, false);
		}
	}

	public void resetTags(String[] xmlTags) {
		for (String tag : xmlTags) {
			this.xmlTags.put(tag, false);
		}
	}

	public void addTags(String[] tags) {
		for (String tag : tags) {
			xmlTags.put(tag, false);
		}
	}

	public void addTag(String tag) {
		xmlTags.put(tag, false);
	}

	public void removeTag(String tag) {
		if (xmlTags.containsKey(tag))
			xmlTags.remove(tag);
	}

	public void removeTags(String[] tags) {
		for (String tag : tags) {
			if (xmlTags.containsKey(tag))
				xmlTags.remove(tag);
		}
	}

	public HashMap<String, Boolean> cloneXmlTags() {
		return new HashMap<String, Boolean>(xmlTags);
	}

	public void setTags(HashMap<String, Boolean> cloneXmlTags) {
		xmlTags = new HashMap<String, Boolean>(cloneXmlTags);
	}

	public boolean checkTag(String tag) {
		if (xmlTags.containsKey(tag)) {
			return xmlTags.get(tag);
		}
		return false;
	}

	private void initializeTags(String[] tags) {
		for (String key : tags) {
			xmlTags.put(key, false);
		}
	}

	public ArrayList<Component> getResult() {

		return result;
	}

	public void setResult(ArrayList<Component> result) {
		this.result = result;
	}

	public Component getLastResult() {
		if (result.size() == 0)
			return null;
		return result.get(result.size() - 1);
	}

	public Component getFirstResult() {
		if (result.size() == 0)
			return null;
		return result.get(0);
	}

	public boolean quickTagCheck(String tag, String qName) {
		return checkTag(tag) && qName.equalsIgnoreCase(tag);
	}

	@Override
	abstract public void characters(char ch[], int start, int length)
			throws SAXException;

	@Override
	abstract public void startElement(String uri, String localName,
			String qName, Attributes attributes) throws SAXException;

	@Override
	abstract public void endElement(String uri, String localName, String qName)
			throws SAXException;

	abstract public void beginParsingComponent(String componentName)
			throws SAXException;

	abstract public void endParsingComponent(String componentName)
			throws SAXException;

}
