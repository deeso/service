/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.xml;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public abstract class BaseXMLReaderWriter<T> {

	static final String XML_TYPE_ELEMENT = "XML_ELEMENT";
	static final String XML_TYPE_DOCUMENT = "XML_DOCUMENT";
	static final String XML_TYPE_STRING = "XML_STRING";

	public static String getXMLString(Node doc)
			throws TransformerFactoryConfigurationError, TransformerException {
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		StreamResult result = new StreamResult(new StringWriter());
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);
		return result.getWriter().toString();
	}

	public static Element createElement(Document doc, String tag, String value) {
		// FIXME revisit later and determine if there is a cleaner way to
		// creating an element.
		Element element = doc.createElement(tag);
		element.appendChild(doc.createTextNode(value));
		return element;
	}

	protected static Document createDocument()
			throws ParserConfigurationException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		return docBuilder.newDocument();
	}

	protected static SAXParser createSaxParser()
			throws ParserConfigurationException, SAXException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		return factory.newSAXParser();
	}

	public abstract Element toElement(T component) throws DOMException,
			SAXException, IOException, ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException;

	public abstract Document toDocument(T component)
			throws ParserConfigurationException, DOMException, SAXException,
			IOException, TransformerFactoryConfigurationError,
			TransformerException;

	public abstract void appendToDocument(T component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public abstract Element toTransport(T component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public abstract Element toTransport(T component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public abstract String toTransportString(T component)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public abstract String toTransportString(T component, Document doc)
			throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public abstract T[] fromTransport(Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	public abstract T[] fromTransport(String xmlString)
			throws ParserConfigurationException, SAXException, IOException;

	public abstract T[] fromTransport(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	public abstract T[] fromTransport(String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException;

	// ///////////////////
	public abstract void fromTransport(T component, Node n)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	public abstract void fromTransport(T component, String xmlString)
			throws ParserConfigurationException, SAXException, IOException;

	public abstract void fromTransport(T component, Node n,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException;

	public abstract void fromTransport(T component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException;

	public abstract T[] parse(String xmlString, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException;

	public abstract T[] parse(Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	public abstract void parse(T component, String xmlString,
			ComponentSaxHandler handler) throws ParserConfigurationException,
			SAXException, IOException;

	public abstract void parse(T component, Node n, ComponentSaxHandler handler)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	abstract public ComponentSaxHandler createDefaultHandler();

}
