package com.thecoverofnight.service.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionToString {

	public static String toString(Exception e) {
		StringWriter sw = new StringWriter();
		sw.write(e.getMessage() + "\n");
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}

	public static String toString(OutOfMemoryError e) {
		StringWriter sw = new StringWriter();
		sw.write(e.getMessage() + "\n");
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
}
