/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.util;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.thecoverofnight.service.xml.ComponentSaxHandler;

public interface Component {
	public ComponentSaxHandler getHandler();

	public Element toTransport() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public String toTransportString() throws ParserConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			DOMException, SAXException, IOException;

	public void fromTransport(Node n) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException;

	public void fromTransportString(String xml)
			throws ParserConfigurationException, SAXException, IOException,
			TransformerFactoryConfigurationError, TransformerException;

	public void characters(char[] ch, int start, int length,
			ComponentSaxHandler handler) throws SAXException;

	public void endElement(String uri, String localName, String qName,
			ComponentSaxHandler handler) throws SAXException;

	public void startElement(String uri, String localName, String qName,
			Attributes attributes, ComponentSaxHandler handler)
			throws SAXException;

	Node toTransport(Document doc) throws ParserConfigurationException,
			SAXException, IOException, TransformerFactoryConfigurationError,
			TransformerException;

}
