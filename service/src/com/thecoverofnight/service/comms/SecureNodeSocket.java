/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetAddress;
import javax.net.ssl.SSLSocketFactory;

public class SecureNodeSocket extends PlainTextNodeSocket {

	String myHost;
	int myPort;

	int myTimeout;
	Long myRecvMsgCnt;
	Long mySendMsgCnt;
	BufferedWriter myOutStream;
	BufferedReader myInStream;

	public SecureNodeSocket(NodeEvents parent, String host, int port,
			int timeout) {
		super(parent, host, port, timeout);

	}

	@Override
	// TODO test to see if this gets called, but I think it should atp
	public void setupSocket() throws IOException {
		InetAddress addr = InetAddress.getByName(myHost);
		SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory
				.getDefault();
		mySocket = sslsocketfactory.createSocket(addr, myPort);
		setupSocketChannels();
	}

}
