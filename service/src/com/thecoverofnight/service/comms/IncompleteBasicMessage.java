/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */
package com.thecoverofnight.service.comms;

import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.Date;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.util.Base64;

public class IncompleteBasicMessage {

	boolean isStale;

	Long time;
	int msgId;

	public String getData() {
		return data;
	}

	public int getLength() {
		return length;
	}

	public int getMsgId() {
		return msgId;
	}

	String data;
	int length;
	protected static Logger LOGGER = Logger
			.getLogger(IncompleteBasicMessage.class);

	public IncompleteBasicMessage(char[] len, char[] msgid) {
		length = byteArrayToInt(len);
		msgId = byteArrayToInt(msgid);
		data = new String();
		updateTime();
	}

	public IncompleteBasicMessage(int length, String data) {
		this.length = length;
		this.data = data;
		this.msgId = 0;
		updateTime();
	}

	public IncompleteBasicMessage(int length, int msgId, String data) {
		this.length = length;
		this.data = data;
		this.msgId = msgId;
		updateTime();
	}

	private void updateTime() {
		Date now = new Date();
		time = new Long(now.getTime() / 1000);
	}

	public boolean isMessageStale(Long time, Long window) {
		return (time - this.time) > window;
	}

	public void append(String data) {
		this.data += data;
		updateTime();
	}

	public boolean isMessageComplete() {
		return length == data.length();
	}

	public static int extractLength(String msg) {
		String[] tokens = msg.split(",");
		if (tokens.length < 3) {
			return -1;
		}
		return byteArrayToInt(tokens[0].getBytes());
	}

	public static int extractMsgId(String msg) {

		String[] tokens = msg.split(",");
		if (tokens.length < 3) {
			return 0;
		}
		return byteArrayToInt(tokens[1].getBytes());
	}

	public static IncompleteBasicMessage fromMessage(String msg)
			throws Exception {
		char[] len = new char[4], msg_id = new char[4];

		msg.getChars(0, 4, len, 0);
		msg.getChars(4, 8, msg_id, 0);
		int length = byteArrayToInt(len);
		int msgId = byteArrayToInt(msg_id);
		String data = msg.substring(8);
		return new IncompleteBasicMessage(length, msgId, data);
	}

	public static String extractData(String msg) {

		String[] tokens = msg.split(",");
		if (tokens.length < 3)
			return "";
		try {
			return new String(Base64.decode(tokens[2]));
		} catch (IOException e) {
			LOGGER.debug("Failed to Base64 decode the message.");
			LOGGER.debug(e.getMessage());
		}
		return tokens[2];
	}

	public void appendData(String data) {
		this.data += data;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public static int byteToInt(byte val, int bits) {
		return (val & 0xff) << bits;
	}

	public static final int byteArrayToInt(char[] bytes) {
		int result = 0;
		int bits = 32;
		for (char b : bytes) {
			result += byteToInt((byte) b, 32 - bits);
			bits -= 8;
		}
		return result;
	}

	public static final int byteArrayToInt(byte[] bytes) {
		int result = 0;
		int bits = 24;
		for (byte b : bytes) {
			result += byteToInt(b, bits);
			bits -= 8;
		}
		return result;
	}

	static byte andVal(int value, int bits) {
		return (byte) ((value >>> bits) & 0xFF);
	}

	public static final byte[] intToByteArray(int value) {
		// return new byte[] { andVal(value, 24), andVal(value, 16),
		// andVal(value, 8), andVal(value, 0)};
		return new byte[] { andVal(value, 0), andVal(value, 8),
				andVal(value, 16), andVal(value, 24), };
	}
}