/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.implementation.ResourceManager2;
import com.thecoverofnight.service.util.ExceptionToString;

@SuppressWarnings("serial")
class NodeServerException extends RuntimeException {
	public NodeServerException(String s) {
		super(s);
	}

	public NodeServerException(Exception e) {
		super(e);
	}
}

public class NodeServerSocket implements Runnable {

	protected static Logger LOGGER = Logger.getLogger(ResourceManager2.class);
	NodeEvents myParent;
	String myHost;
	int myPort;

	int myConnectionCounts;
	String[] myHostsConnected;

	int myTimeout;
	int myBacklog;
	int myRecvMsgCnt;
	int mySendMsgCnt;
	ServerSocket mySocket;
	BufferedWriter myOutStream;
	BufferedReader myInStream;

	boolean myServerRunning;
	Thread myThread;

	private void init(String host, int port, int timeout) {
		myHost = host;
		myPort = port;
		myTimeout = timeout;
		myServerRunning = false;
		init_io();
	}

	private void init_io() {
		mySocket = null;
	}

	private void init_socket() {
		try {
			mySocket = new ServerSocket(myPort, myBacklog,
					InetAddress.getByName(myHost));
			mySocket.setSoTimeout(myTimeout);
		} catch (UnknownHostException e) {
			LOGGER.debug(ExceptionToString.toString(e));
			LOGGER.debug(ExceptionToString.toString(e));
		} catch (IOException e) {
			LOGGER.debug(ExceptionToString.toString(e));
			LOGGER.debug(ExceptionToString.toString(e));
		}

	}

	public NodeServerSocket(NodeEvents parent, String host, int port,
			int timeout) {
		init_io();
		init(host, port, timeout);
		myParent = parent;
		myThread = null;
	}

	@Override
	public void run() {

		init_socket();
		myServerRunning = true;

		while (myServerRunning) {

			try {
				if (mySocket == null)
					break;
				Socket s = mySocket.accept();
				myParent.connectionOpened(s);
			} catch (UnknownHostException e) {
				LOGGER.debug(ExceptionToString.toString(e));
				// init_io();
				myServerRunning = false;
				throw new NodeServerException(e);

			} catch (SocketException e) {
				// LOGGER.debug(ExceptionToString.toString(e));
				myServerRunning = false;

			} catch (SocketTimeoutException e) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
				}

			} catch (IOException e) {
				LOGGER.debug(ExceptionToString.toString(e));
			} catch (NullPointerException e) {
				LOGGER.debug("mySocket was set to null, need to quit.");
				LOGGER.debug(ExceptionToString.toString(e));
			}

		}
		myServerRunning = false;

	}

	public void start() {
		myThread = new Thread(this);
		myThread.start();
	}

	public void stop() throws IOException {
		shutdown();
	}

	public NodeEvents getParent() {
		return myParent;
	}

	public void setParent(NodeEvents myParent) {
		this.myParent = myParent;
	}

	public String getHost() {
		return myHost;
	}

	public void setHost(String myHost) {
		this.myHost = myHost;
	}

	public int getPort() {
		return myPort;
	}

	public void setPort(int myPort) {
		this.myPort = myPort;
	}

	public int getTimeout() {
		return myTimeout;
	}

	public void setTimeout(int myTimeout) {
		this.myTimeout = myTimeout;
	}

	public int getBacklog() {
		return myBacklog;
	}

	public void setBacklog(int myBacklog) {
		this.myBacklog = myBacklog;
	}

	public boolean isServerRunning() {
		return myServerRunning;
	}

	public void setServerRunning(boolean myServerRunning) {
		this.myServerRunning = myServerRunning;
	}

	public int getConnectionCounts() {
		return myConnectionCounts;
	}

	/**
	 * @return
	 */
	public String[] getHostsConnected() {
		return myHostsConnected;
	}

	public void shutdown() {
		myServerRunning = false;

		try {
			if (mySocket != null && !mySocket.isClosed()) {
				mySocket.close();
				mySocket = null;
			}

		} catch (IOException e) {
			LOGGER.debug("Exception Performing Server Shutdown");
			LOGGER.debug(ExceptionToString.toString(e));
		}

	}

}
