/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.util.Base64;
import com.thecoverofnight.service.util.ExceptionToString;

public class BasicMessage {

	protected static Logger LOGGER = Logger.getLogger(BasicMessage.class);

	int length;
	int msgId;
	String data;
	String bufferedData;

	public BasicMessage() {
		this.data = "";
		this.length = data.length();
		this.msgId = 0;
	}

	public BasicMessage(String data) {
		this.data = data;
		this.length = data.length();
		this.msgId = 0;
	}

	public BasicMessage(int msgId, String data) {
		this.data = data;
		this.length = data.length();
		this.msgId = msgId;
	}

	public BasicMessage(IncompleteBasicMessage ibm) {
		try {
			this.data = new String(Base64.decode(ibm.getData()));
		} catch (IOException e) {
			LOGGER.debug("Failed to Base64 decode string.");
			LOGGER.debug(ExceptionToString.toString(e));
			this.data = ibm.getData();
		}
		length = getLength();
		msgId = ibm.getMsgId();
	}

	@Override
	public String toString() {
		return toTransportString();
	}

	public String toTransportString() {
		String data = Base64.encodeString(this.data);
		char[] blah = intToChrArray(data.length());
		char[] msgId = intToChrArray(this.msgId);
		return new String(blah) + new String(msgId) + data;
	}

	public int getLength() {
		return data.length();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public static BasicMessage fromMessage(String msg) {
		if (!isMessageComplete(msg))
			return null;
		String data = extractData(msg);
		int msgId = extractMsgId(msg);
		if (data == null)
			return null;
		return new BasicMessage(msgId, data);
	}

	boolean isMessageComplete() {
		return data.length() == length;
	}

	public static boolean isMessageComplete(String msg) {
		String[] tokens = msg.split(",");
		int length = extractLength(msg);
		return tokens.length < 3 && length > -1 && tokens[2].length() == length;
	}

	public static int extractLength(String msg) {
		String[] tokens = msg.split(",");
		if (tokens.length < 3) {
			return -1;
		}
		return byteArrayToInt(tokens[0].getBytes());
	}

	public static int extractMsgId(String msg) {
		if (!isMessageComplete(msg)) {
			return 0;
		}

		String[] tokens = msg.split(",");
		if (tokens.length < 3) {
			return 0;
		}
		return byteArrayToInt(tokens[1].getBytes());
	}

	public static String extractData(String msg) {
		if (!isMessageComplete(msg)) {
			return null;
		}
		String[] tokens = msg.split(",");
		if (tokens.length < 3)
			return "";
		try {
			return new String(Base64.decode(tokens[2]));
		} catch (IOException e) {
			LOGGER.debug("Failed to Base64 decode the message.");
			LOGGER.debug(e.getMessage());
		}
		return tokens[2];
	}

	public void appendData(String data) {
		this.data += data;
	}

	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public static int byteToInt(byte val, int bits) {
		return (val & 0xff) << bits;
	}

	public static final int byteArrayToInt(byte[] bytes) {
		int result = 0;
		int bits = 32;
		for (byte b : bytes) {
			result += byteToInt(b, 32 - bits);
			bits -= 8;
		}
		return result;
	}

	static char andVal(int val, int bits) {
		return (char) ((val >>> bits) & 0xFF);
	}

	public static final char[] intToChrArray(int value) {
		// return new byte[] { andVal(value, 24), andVal(value, 16),
		// andVal(value, 8), andVal(value, 0)};
		return new char[] { andVal(value, 0), andVal(value, 8),
				andVal(value, 16), andVal(value, 24), };

	}
}
