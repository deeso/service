/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.util.ExceptionToString;

class ReceivePollTask {
	final Object lock = new Object();

	NodeClientSocket myNT;
	Timer timer = null;

	int pollTime = 0;
	private static Logger LOGGER = Logger.getLogger(ReceivePollTask.class);

	int state = NOT_POLLING;
	static final int NOT_POLLING = 0;
	static final int POLLING = 1;

	Boolean SOCKET_BEING_READ = new Boolean(false);
	static final int NOT_READING = 0;
	static final int READING = 1;

	public ReceivePollTask(NodeClientSocket nt, int timing) {
		myNT = nt;
		pollTime = timing;
		state = NOT_POLLING;
	}

	public void start() {

		if (state == POLLING)
			return;

		synchronized (lock) {

			if (state == NOT_POLLING) {
				state = POLLING;
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						synchronized (lock) {
							if (!isPolling()) {
								LOGGER.debug("Looks like state == NOT_POLLING");

							} else if (myNT.checkInstream() && !SOCKET_BEING_READ) {
								SOCKET_BEING_READ = true;
								myNT.recv();
								LOGGER.debug("ReceivePollTask.run: recv'ing data.");
							}

						} // end of synchronized block s

						// FIXME need to desk check this for a race condition
						// not doing it now because of a time crunch
						if (!myNT.isKeepPolling()) {
							cancel();
							LOGGER.debug("cancelling socket.");
						} else if (!myNT.isConnected()) {
							LOGGER.debug("It appears the connection was lost.");
							cancel();
							myNT.cleanupDisconnectedSocket();

						}

					}
				}, pollTime, pollTime);
			}
		}
	}

	public void doneReading() {
		synchronized (lock) {
			SOCKET_BEING_READ = false;
		}
	}

	public void reset() {
		cancel();
	}

	public void setPollTime(int time) {
		pollTime = time;
	}

	public boolean isPolling() {
		return state == POLLING;
	}

	public void cancel() {
		synchronized (lock) {
			state = NOT_POLLING;
			SOCKET_BEING_READ = false;

			if (timer == null) {
				return;
			}

			timer.cancel();

		}

	}

}
