/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.io.IOException;

public interface NodeClientSocket extends NodeEvents {
	public boolean isConnected();

	public boolean connect();

	public void disconnect();

	public void recv();

	public void send(final String data);

	public NodeEvents getParent();

	public void startPolling();

	public void stopPolling();

	public void close();

	public void schedulePolling();

	public boolean isKeepPolling();

	public boolean checkInstream();

	void setupSocket() throws IOException;

	public void startWriteDataThread(String data);

	public void cleanupDisconnectedSocket();

	// public void sendShutdown();
	// public void sendShutdown(String msg);

}
