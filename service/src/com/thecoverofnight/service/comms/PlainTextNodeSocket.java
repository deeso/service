/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.Date;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.util.ExceptionToString;

public class PlainTextNodeSocket implements NodeClientSocket {

	private static Logger LOGGER = Logger.getLogger(PlainTextNodeSocket.class);
	Integer bytesExpectingNext;
	final String HEARTBEAT = "HEARTBEAT";
	boolean keepPolling;

	long lastConnection;

	String myHost;

	BufferedReader myInStream;

	BufferedWriter myOutStream;

	NodeEvents myParent;

	int myPollTime;
	int myPort;
	Long myRecvMsgCnt;

	ReceivePollTask myRecvPollTimer;

	Long mySendMsgCnt;

	SendPollTask mySendPollTimer;

	Socket mySocket;
	int myTimeout;
	long myTwoMinuteWindow = 30; // needs to be in seconds
	final String SHUTDOWN = "SHUTDOWN";

	/*
	 * public PlainTextNodeSocket(NodeEvents parent, Socket client) throws
	 * IOException { myParent = parent;
	 * 
	 * myTimeout = 0; mySocket = client; myHost =
	 * client.getInetAddress().toString(); myPort = client.getPort();
	 * setupSocketChannels(); myTimeout = client.getSoTimeout(); myPollTime =
	 * myTimeout/2;
	 * 
	 * myRecvPollTimer = new ReceivePollTask(this, myPollTime); mySendPollTimer
	 * = new SendPollTask(this, myPollTime); }
	 */
	public PlainTextNodeSocket(NodeEvents parent, Socket client, int timeout)
			throws IOException {
		myParent = parent;
		lastConnection = 0;
		myTimeout = timeout;
		mySocket = client;
		myHost = client.getInetAddress().toString();
		myPort = client.getPort();
		setupSocketChannels();
		// myTimeout = client.getSoTimeout();
		myPollTime = myTimeout / 2;
		updateLastConnectionTime();
		myRecvPollTimer = new ReceivePollTask(this, myPollTime);
		mySendPollTimer = new SendPollTask(this, myPollTime);
		bytesExpectingNext = 0;

	}

	public PlainTextNodeSocket(NodeEvents parent, String host, int port,
			int timeout) {
		myParent = parent;
		myPollTime = myTimeout / 2;
		updateLastConnectionTime();
		myRecvPollTimer = new ReceivePollTask(this, myPollTime);
		mySendPollTimer = new SendPollTask(this, myPollTime);
		bytesExpectingNext = 0;
		init(host, port, timeout);
		init_io();
	}

	@Override
	public boolean checkInstream() {

		try {
			if (!isConnected() || !myInStream.ready()) {
				return false;
			}

		} catch (IOException e) {
			LOGGER.debug("Experienced an exception checking the input stream.");
			LOGGER.debug(ExceptionToString.toString(e));
		}
		return true;

	}

	@Override
	public void cleanupDisconnectedSocket() {
		this.stopPolling();
		myParent.connectionClosed(myHost, myPort);
	}

	@Override
	public void close() {

		try {
			LOGGER.debug("ending polling.");
			stopPolling();
			LOGGER.debug("closing socket.");
			mySocket.close();
			LOGGER.debug("closing down buffered readers and writers.");
			myInStream.close();
			myOutStream.close();
		} catch (IOException e) {
			LOGGER.debug("Experienced an exception trying to close the connection.");
			LOGGER.debug(ExceptionToString.toString(e));
		}

	}

	@Override
	public boolean connect() {

		if (isConnected())
			return true;
		init_io();
		try {
			setupSocket();
			return true;

		} catch (IOException e) {
			LOGGER.debug("Experienced an exception trying to make a connection.");
			LOGGER.debug(ExceptionToString.toString(e));
			init_io();
		}
		return false;
	}

	@Override
	public void connectionClosed(Socket connection) {
	}

	@Override
	public void connectionClosed(String host, int port) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionOpened(Socket connection) {
	}

	@Override
	public void dataRecieved(Socket connection, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataRecieved(String host, int port, BufferedReader inStream) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataRecieved(String host, String data) {
	}

	@Override
	public void dataSent() {
	}

	@Override
	public void disconnect() {
		stopPolling();
		try {
			mySocket.close();
		} catch (IOException e) {
			LOGGER.debug("Experienced an exception disconnecting socket.");
			LOGGER.debug(ExceptionToString.toString(e));
		}
	}

	public int getBytesExpectingNext() {
		return bytesExpectingNext;
	}

	public String getHost() {
		return myHost;
	}

	@Override
	public NodeEvents getParent() {
		return myParent;
	}

	public int getPollTime() {
		return myPollTime;
	}

	public int getPort() {
		return myPort;
	}

	public int getTimeout() {
		return myTimeout;
	}

	private void init(String host, int port, int timeout) {
		myHost = host;
		myPort = port;
		myTimeout = timeout;
		myPollTime = timeout;
		myRecvPollTimer.setPollTime(timeout);

	}

	private void init_io() {
		mySocket = null;
		myOutStream = null;
		myInStream = null;
		// myRecvPollTimer.reset();
		myRecvPollTimer.setPollTime(myPollTime);
		mySendPollTimer.setPollTime(myPollTime);
	}

	@Override
	public boolean isConnected() {
		return mySocket != null && !mySocket.isClosed() && !isTimeoutNeeded();
	}

	@Override
	public boolean isKeepPolling() {
		return keepPolling;
	}

	public boolean isTimeoutNeeded() {
		// return ((new Date().getTime()/1000) - lastConnection) > myTimeout;
		// this is a test, if socket bombed, then this will trigger an
		// exceptions.
		// java wont detect if the other side is dead :(
		long currentWindow = ((new Date().getTime() / 1000) - lastConnection);
		if (currentWindow > myTwoMinuteWindow) {
			// need to update this here or a ton of heartbeats will be sent out
			LOGGER.debug("Sending Heart beat message, no activity for: "
					+ currentWindow + " seconds");
			updateLastConnectionTime();
			sendHeartBeat();
		}
		return false;

	}

	@Override
	public void recv() {
		// TODO handle when not connected
		if (isConnected()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					// try {
					// TODO read message buffer on the following conditions
					// 0) read message length
					// 1) all data is present
					// 2) 0 or more but not all data is present, and we need
					// to wait for more data
					// FIXME need to do something about reading
					// char[] buf = new char[10];
					// if (myInStream.ready()) {
					// myInStream.read(buf);
					// }
					// String msgdata = new String(buf);
					// FIXME this is a really bad HACK, to check the socket is
					// alive
					// trust, trusting, trust

					// myParent.dataRecieved(myHost, myPort, msgdata);
					myParent.dataRecieved(myHost, myPort, myInStream);
					myRecvPollTimer.doneReading();
					updateLastConnectionTime();
					/*
					 * }catch (SocketException e){ LOGGER.debug(
					 * "Experienced an error reading from the socket, cleaning up the socket.."
					 * ); LOGGER.debug(ExceptionToString.toString(e));
					 * myRecvPollTimer.doneReading();
					 * cleanupDisconnectedSocket(); } catch (IOException e) {
					 * LOGGER
					 * .debug("Experienced an exception trying to recieve data."
					 * ); LOGGER.debug(ExceptionToString.toString(e)); }
					 */

				}
			}).start();
		}

	}

	@Override
	public void schedulePolling() {

		// FIXME big question, could this lead to a potential DOS
		// if I schedule a poll excessively, seems like it maybe possible?
		if (!keepPolling) {
			myRecvPollTimer.cancel();
			mySendPollTimer.cancel();
			LOGGER.debug("not scheduling polls, keepPolling is false.");
			return;
		}

		myRecvPollTimer.start();
		mySendPollTimer.start();
		LOGGER.debug("scheduling polls for " + myPollTime + " ms.");

	}

	public void send(BasicMessage bn) {
		mySendPollTimer.addMessage(bn);
	}

	@Override
	public void send(String data) {
		mySendPollTimer.addMessage(data);
	}

	boolean sendHeartBeat() {

		this.send(new BasicMessage(HEARTBEAT));
		return true;
	}

	private void sendShutdown() {
		sendShutdown(SHUTDOWN);
	}

	private void sendShutdown(String msg) {
		LOGGER.debug("Sending " + myHost + " " + myPort
				+ " a shutdown message.");
		synchronized (mySendPollTimer) {
			mySendPollTimer.dumpAllMessages();
			mySendPollTimer.addMessage(new BasicMessage(SHUTDOWN)
					.toTransportString());
		}
		try {
			LOGGER.debug("Sleeping for 500 ms while the message gets sent.");
			Thread.sleep(500);
		} catch (InterruptedException e) {
			LOGGER.debug("Got interrupted during sleep.");
			LOGGER.debug(ExceptionToString.toString(e));
		}
	}

	public void setBytesExpectingNext(Integer bytesExpectingNext) {
		synchronized (bytesExpectingNext) {
			this.bytesExpectingNext = bytesExpectingNext;
		}

	}

	public void setPollTime(int myPollTime) {
		this.myPollTime = myPollTime;

		boolean restartSend = mySendPollTimer.isPolling(), restartRecv = myRecvPollTimer
				.isPolling();

		myRecvPollTimer.cancel();
		myRecvPollTimer = new ReceivePollTask(this, myPollTime);
		mySendPollTimer.cancel();
		mySendPollTimer = new SendPollTask(this, myPollTime,
				mySendPollTimer.getPendingMsgs());
		if (restartSend)
			mySendPollTimer.start();
		if (restartRecv)
			myRecvPollTimer.start();
	}

	public void setTimeout(int myTimeout) {
		this.myTimeout = myTimeout;
	}

	@Override
	public void setupSocket() throws IOException {
		InetAddress addr = InetAddress.getByName(myHost);
		SocketAddress sockAddr = new InetSocketAddress(addr, myPort);
		mySocket = new Socket();
		mySocket.connect(sockAddr, myTimeout);
		setupSocketChannels();
	}

	protected void setupSocketChannels() throws IOException {
		myOutStream = new BufferedWriter(new OutputStreamWriter(
				mySocket.getOutputStream(), "ISO-8859-1"));
		myInStream = new BufferedReader(new InputStreamReader(
				mySocket.getInputStream(), "ISO-8859-1"));
	}

	@Override
	public void shutdown() {
		// stop polling first, otherwise a deadlock could occur when
		// the polling classes perform the cleanupDisconnectedSocket
		stopRecvPolling();
		sendShutdown();
		stopSendPolling();
		keepPolling = false; // ths may be unnecessary
		try {
			mySocket.close();
		} catch (IOException e) {
			LOGGER.debug("Exception while shutting down the socket");
			LOGGER.debug(ExceptionToString.toString(e));
		}
	}

	@Override
	public void startPolling() {
		LOGGER.debug("starting to poll.");
		keepPolling = true;
		schedulePolling();
	}

	@Override
	public void startWriteDataThread(final String data) {
		if (isConnected()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						String upperData = data;
						myOutStream.write(data);
						myOutStream.flush();
						myParent.dataSent();
						updateLastConnectionTime();
						mySendPollTimer.doneWriting();
					} catch (SocketException e) {
						LOGGER.debug("Experienced an in the thread handling the send to the remote host.");
						LOGGER.debug(ExceptionToString.toString(e));
						mySendPollTimer.doneWriting();
						cleanupDisconnectedSocket();
					} catch (IOException e) {
						LOGGER.debug("Experienced an in the thread handling the send to the remote host.");
						LOGGER.debug(ExceptionToString.toString(e));
						mySendPollTimer.doneWriting();
					}

				}
			}).start();
		}
	}

	@Override
	public void stopPolling() {
		LOGGER.debug("stopping polling.");
		keepPolling = false;

		stopRecvPolling();
		stopSendPolling();

	}

	private void stopRecvPolling() {
		synchronized (myRecvPollTimer) {
			if (myRecvPollTimer != null)
				myRecvPollTimer.cancel();
		}
	}

	private void stopSendPolling() {
		synchronized (mySendPollTimer) {
			if (mySendPollTimer != null)
				mySendPollTimer.cancel();
		}
	}

	protected void updateLastConnectionTime() {
		lastConnection = new Long(new Date().getTime() / 1000);
	}

}
