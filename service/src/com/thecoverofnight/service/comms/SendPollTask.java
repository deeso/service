/*
 * Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *
 */

package com.thecoverofnight.service.comms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.thecoverofnight.service.util.ExceptionToString;

class SendPollTask {
	final Object lock = new Object();
	LinkedList<String> outboundMsgQueue = new LinkedList<String>();

	NodeClientSocket myNT;
	Timer timer = null;

	int pollTime = 0;
	private static Logger LOGGER = Logger.getLogger(SendPollTask.class);

	int state = NOT_POLLING;
	static final int NOT_POLLING = 0;
	static final int POLLING = 1;

	Boolean SOCKET_BEING_WRITTEN = new Boolean(false);
	static final int NOT_WRITING = 0;
	static final int WRITING = 1;

	public void dumpAllMessages() {
		outboundMsgQueue.clear();
	}

	public SendPollTask(NodeClientSocket nt, int timing) {
		myNT = nt;
		pollTime = timing;
		state = NOT_POLLING;

	}

	public SendPollTask(NodeClientSocket nt, int timing, String[] msgs) {
		myNT = nt;
		pollTime = timing;
		state = NOT_POLLING;
		for (String i : msgs)
			outboundMsgQueue.addLast(i);

	}

	public void addMessage(BasicMessage bm) {
		this.addMessage(bm.toTransportString());
	}

	public void addMessage(String msg) {
		synchronized (lock) {
			outboundMsgQueue.addLast(msg);
		}
	}

	public void start() {

		if (state == POLLING)
			return;

		synchronized (lock) {

			if (state == NOT_POLLING) {
				state = POLLING;
				timer = new Timer();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						synchronized (lock) {
							if (!isPolling()) {
								LOGGER.debug("Looks like state == NOT_POLLING");

							} else if (!SOCKET_BEING_WRITTEN
									&& outboundMsgQueue.size() > 0) {
								SOCKET_BEING_WRITTEN = true;
								String data = "";
								// I dont think we should be able to deadlock
								// here,
								// but it may be possible
								synchronized (outboundMsgQueue) {
									data = outboundMsgQueue.removeFirst();
								}
								myNT.startWriteDataThread(data);
								LOGGER.debug("sending data.");
							}
						}

						// FIXME need to desk check this for a race condition
						// not doing it now because of a time crunch
						if (!myNT.isKeepPolling()) {
							cancel();
							LOGGER.debug("cancelling socket.");
						} else if (!myNT.isConnected()) {
							LOGGER.debug("It appears the connection was lost.");
							cancel();
							myNT.cleanupDisconnectedSocket();

						} else if (!isPolling()) {
							cancel();
						}

					}
				}, pollTime, pollTime);
			}
		}
	}

	public void doneWriting() {
		synchronized (lock) {
			SOCKET_BEING_WRITTEN = false;
		}
	}

	public boolean isPolling() {
		return state == POLLING;
	}

	public void reset() {
		cancel();
	}

	public void setPollTime(int time) {
		pollTime = time;
	}

	public String[] getPendingMsgs() {

		if (isPolling()) {
			return new String[0];
		}

		ArrayList<String> msgs = new ArrayList<String>();

		for (String msg : outboundMsgQueue)
			msgs.add(msg);
		return msgs.toArray(new String[0]);

	}

	public void cancel() {

		synchronized (lock) {
			state = NOT_POLLING;
			SOCKET_BEING_WRITTEN = false;

			if (timer == null) {
				return;
			}
			timer.cancel();
		}

	}

}
